import { Component, OnInit } from '@angular/core';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Router, NavigationStart, NavigationCancel, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
declare let $: any;

@Component({
    selector: 'app-standar',
    templateUrl: './standar.component.html',
    styleUrls: ['./standar.component.scss'],
    providers: [
        Location, {
            provide: LocationStrategy,
            useClass: PathLocationStrategy
        }
    ]
})
export class StandarComponent implements OnInit {

    constructor(private router: Router) { }
    location: any;
    routerSubscription: any;
    ngOnInit(): void {
        this.recallJsFuntions();
    }

    recallJsFuntions() {
        this.router.events
            .subscribe((event) => {
                if (event instanceof NavigationStart) {
                    $('.preloader-area').fadeIn('slow');
                }
            });
        this.routerSubscription = this.router.events
            .pipe(filter(event => event instanceof NavigationEnd || event instanceof NavigationCancel))
            .subscribe(event => {
                $.getScript('../assets/js/main.js');
                $('.preloader-area').fadeOut('slow');
                this.location = this.router.url;
                if (!(event instanceof NavigationEnd)) {
                    return;
                }
                window.scrollTo(0, 0);
            });
    }

}
