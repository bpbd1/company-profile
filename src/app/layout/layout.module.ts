import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StandarComponent } from './standar/standar.component';
import { ComponentsModule } from '../components/components.module';
import { RouterModule } from '@angular/router';

const COM = [StandarComponent]


@NgModule({
    declarations: [...COM],
    imports: [
        CommonModule,
        ComponentsModule,
        RouterModule
    ],
    exports: [...COM]
})
export class LayoutModule { }
