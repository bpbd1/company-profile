import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArtikelRoutingModule } from './artikel-routing.module';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { AppPipeModule } from 'src/app/_pipe/app-pipe.module';


@NgModule({
  declarations: [ListComponent, DetailComponent],
  imports: [
    CommonModule,
    ArtikelRoutingModule,
    ComponentsModule,
        
    NgxSkeletonLoaderModule,
    AppPipeModule,
  ]
})
export class ArtikelModule { }
