import { Component, OnInit } from '@angular/core';
import { CmsPostService, CmsPostTypeService } from 'src/app/sdk/admin/services';
import { ActivatedRoute } from '@angular/router';
import { Cms_Post, Cms_Post_type } from 'src/app/sdk/admin/models';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  constructor(
    private cmsPostService: CmsPostService,
    private cmsPostTypeService: CmsPostTypeService,
    private activatedRoute: ActivatedRoute,
  ) { }

  id_post: string = ''
  dataPost: Cms_Post = {}
  is_loading = true
  ngOnInit(): void {
    this.id_post = this.activatedRoute.snapshot.paramMap.get('id')
    this.getData()
  }

  getData() {
    this.is_loading = true
    this.cmsPostService.getCmsPostsId(this.id_post).subscribe(
      data => {
        this.dataPost = data.data;
        this.is_loading = false

        this.getType(data.data.id_post_type)
      },
      err => {
        this.is_loading = false
        alert("server not respon")
      }
    )
  }

  dataType: Cms_Post_type = {}
  getType(id) {
    this.cmsPostTypeService.getCmsPostTypesId(id).subscribe(
      data => {
        this.dataType = data.data
      },
      err => {
        alert("server not respon")
      }
    )
  }

}
