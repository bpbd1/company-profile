import { Component, OnInit } from '@angular/core';
import { CmsPostService, CmsPostTypeService } from 'src/app/sdk/admin/services';
import { ActivatedRoute } from '@angular/router';
import { Cms_Post, Cms_Post_type } from 'src/app/sdk/admin/models';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor(
    private cmsPostService: CmsPostService,
    private cmsPostTypeService: CmsPostTypeService,
    private activeRouter: ActivatedRoute
  ) { }

  id_Type = null;
  Post = null;
  is_loading= true
  ngOnInit(): void {
    this.activeRouter.paramMap.subscribe(
      data => {
        if (data.get('id')) {
          this.id_Type = data.get('id');
          this.getPost()
          this.getData()
        }
      }
    )
  }


  dataType: Cms_Post_type = {}
  getData() {
    this.is_loading= true
    this.cmsPostTypeService.getCmsPostTypesId(this.id_Type).subscribe(
      data => {
        this.dataType = data.data
        this.is_loading= false
      },
      err => {
        this.is_loading= false
        alert("server not respon")
      }
    )
  }

  pageSize = 10
  pageIndex = 1
  total = 0

  dataPost: Cms_Post[] = []
  getPost() {
    this.is_loading= true
      let param = {
          pageSize: this.pageSize,
          pageIndex: this.pageIndex,
          id_post_type: this.id_Type

      }
      this.cmsPostService.getCmsPosts(JSON.stringify(param)).subscribe(
          data => {
              this.dataPost = data.data['data'];
              this.total = data.data['total']
              this.is_loading= false
          },
          err => {
            this.is_loading= false
              alert("server not respon")
          }
      )
  }
}
