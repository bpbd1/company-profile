import { MapComponent } from "./../components/map/map.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PagesRoutingModule } from "./pages-routing.module";
import { HomeComponent } from "./home/home.component";
import { ComponentsModule } from '../components/components.module';
import { NotFountComponent } from './not-fount/not-fount.component';
import { ProfilComponent } from './profil/profil.component';
import { KontakComponent } from './kontak/kontak.component';
import { LaporComponent } from './lapor/lapor.component';
import { FormsModule } from '@angular/forms';


@NgModule({
    declarations: [NotFountComponent, ProfilComponent, KontakComponent, LaporComponent],
    imports: [
        CommonModule,
        PagesRoutingModule,
        ComponentsModule,
        FormsModule
        
    ]
})
export class PagesModule { }
