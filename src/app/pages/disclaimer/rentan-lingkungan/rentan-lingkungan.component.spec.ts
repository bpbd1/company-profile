import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RentanLingkunganComponent } from './rentan-lingkungan.component';

describe('RentanLingkunganComponent', () => {
  let component: RentanLingkunganComponent;
  let fixture: ComponentFixture<RentanLingkunganComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RentanLingkunganComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RentanLingkunganComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
