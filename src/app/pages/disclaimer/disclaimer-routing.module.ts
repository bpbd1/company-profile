import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DisclaimerComponent } from './disclaimer.component';
import { BahayaComponent } from './bahaya/bahaya.component';
import { KapasitasComponent } from './kapasitas/kapasitas.component';
import { KerentananComponent } from './kerentanan/kerentanan.component';
import { RentanLingkunganComponent } from './rentan-lingkungan/rentan-lingkungan.component';
import { ResikoComponent } from './resiko/resiko.component';


const routes: Routes = [
  {
    path: '',
    component: DisclaimerComponent,
    children: [
      {
        path: '',
        redirectTo: 'bahaya'
      },
      {
        path: 'bahaya',
        component: BahayaComponent
      },
      {
        path: 'kapasitas',
        component: KapasitasComponent
      },
      {
        path: 'kerentanan',
        component: KerentananComponent
      },
      {
        path: 'rentan-lingkungan',
        component: RentanLingkunganComponent
      },
      {
        path: 'resiko',
        component: ResikoComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DisclaimerRoutingModule { }
