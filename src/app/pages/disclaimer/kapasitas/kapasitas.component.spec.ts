import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KapasitasComponent } from './kapasitas.component';

describe('KapasitasComponent', () => {
  let component: KapasitasComponent;
  let fixture: ComponentFixture<KapasitasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KapasitasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KapasitasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
