import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DisclaimerRoutingModule } from './disclaimer-routing.module';
import { BahayaComponent } from './bahaya/bahaya.component';
import { KerentananComponent } from './kerentanan/kerentanan.component';
import { RentanLingkunganComponent } from './rentan-lingkungan/rentan-lingkungan.component';
import { KapasitasComponent } from './kapasitas/kapasitas.component';
import { ResikoComponent } from './resiko/resiko.component';
import { DisclaimerComponent } from './disclaimer.component';
import { AppModule } from 'src/app/app.module';
import { ComponentsModule } from 'src/app/components/components.module';


@NgModule({
  declarations: [BahayaComponent, KerentananComponent, RentanLingkunganComponent, KapasitasComponent, ResikoComponent, DisclaimerComponent],
  imports: [
    CommonModule,
    DisclaimerRoutingModule,
    ComponentsModule,
  ]
})
export class DisclaimerModule { }
