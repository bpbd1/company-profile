import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KerentananComponent } from './kerentanan.component';

describe('KerentananComponent', () => {
  let component: KerentananComponent;
  let fixture: ComponentFixture<KerentananComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KerentananComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KerentananComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
