import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BahayaComponent } from './bahaya.component';

describe('BahayaComponent', () => {
  let component: BahayaComponent;
  let fixture: ComponentFixture<BahayaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BahayaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BahayaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
