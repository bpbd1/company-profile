import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResikoComponent } from './resiko.component';

describe('ResikoComponent', () => {
  let component: ResikoComponent;
  let fixture: ComponentFixture<ResikoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResikoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResikoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
