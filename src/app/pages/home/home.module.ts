import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HomeRoutingModule } from "./home-routing.module";
import { SlideComponent } from "./slide/slide.component";
import { HomeComponent } from "./home.component";
import { BeritaComponent } from "./berita/berita.component";
import { UnggulanComponent } from "./unggulan/unggulan.component";
import { StatistikComponent } from "./statistik/statistik.component";
import { RepositoryComponent } from "./repository/repository.component";
import { AboutComponent } from "./about/about.component";
import { NgxSkeletonLoaderModule } from "ngx-skeleton-loader";
import { AppPipeModule } from "src/app/_pipe/app-pipe.module";
import { ComponentsModule } from "src/app/components/components.module";
import * as echarts from 'echarts';
import { NgxEchartsModule } from 'ngx-echarts';
import { OwlModule } from "ngx-owl-carousel";

@NgModule({
    declarations: [
        HomeComponent,
        SlideComponent,
        BeritaComponent,
        UnggulanComponent,
        StatistikComponent,
        RepositoryComponent,
        AboutComponent,
    ],
    imports: [
        CommonModule,
        HomeRoutingModule,
        OwlModule,
        NgxSkeletonLoaderModule,
        AppPipeModule,
        ComponentsModule,
        NgxEchartsModule.forRoot({
            echarts
        })
    ],
})
export class HomeModule { }
