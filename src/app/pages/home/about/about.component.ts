import { Component, OnInit } from '@angular/core';
import { CmsPageService } from 'src/app/sdk/admin/services';
import { Cms_Page } from 'src/app/sdk/admin/models';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(
    private cmsPageService: CmsPageService,
  ) { }



  ngOnInit() {
    this.getData()
  }

  data: Cms_Page = {}
  id_page: string = 'b6a4699b-9bde-448c-af00-b038ad9f59e8'
  is_loading= true

  getData() {
    this.is_loading= true
    this.cmsPageService.getCmsPagesId(this.id_page).subscribe(
      data => {
        this.data = data.data;
        this.is_loading= false
 
      }
    )
  }

 
}
