import { Component, OnInit } from '@angular/core';
import { CmsPostService } from 'src/app/sdk/admin/services';
import { Cms_Post } from 'src/app/sdk/admin/models';


@Component({
  selector: 'app-berita',
  templateUrl: './berita.component.html',
  styleUrls: ['./berita.component.scss']
})
export class BeritaComponent implements OnInit {

  constructor(
    private cmsPostService: CmsPostService,

) { }

ngOnInit() {
    this.getData()
}

is_loading = true
lisData: Cms_Post[] = []
listOfData: Cms_Post[] = []
pageIndex = 1
pageSize = 3
sortValue: string | null = null
sortKey: string | null = null
id_post_type = 'all'
getData() {
    this.is_loading= true
    this.cmsPostService.getCmsPosts(JSON.stringify({
        pageIndex: this.pageIndex,
        pageSize: this.pageSize,
        sortKey: this.sortKey,
        sortValue: this.sortValue,
        id_post_type: this.id_post_type
    })) .subscribe((data: any) => {
        this.listOfData = data.data.data
        console.log(data.data.data)
      })
}



}
