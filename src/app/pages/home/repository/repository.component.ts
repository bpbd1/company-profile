import { Component, OnInit } from '@angular/core';
import { CmsPetaKategoriService, CmsDirektoriKategoriService, CmsDirektoriKategoriLinkService } from 'src/app/sdk/admin/services';
import { Cms_Peta_kategori, Cms_Direktori_kategori } from 'src/app/sdk/admin/models';

@Component({
  selector: 'app-repository',
  templateUrl: './repository.component.html',
  styleUrls: ['./repository.component.scss']
})
export class RepositoryComponent implements OnInit {

  constructor(
    private cmsDirektoriKategoriService: CmsDirektoriKategoriService,
    private cmsDirektoriKategoriLinkService: CmsDirektoriKategoriLinkService,

  ) { }


  ngOnInit() {
    this.getData()
  }

  dataKategori: Cms_Direktori_kategori[] = []


  is_loading = true

  getData() {
    this.is_loading = true

    this.cmsDirektoriKategoriService.getCmsDirektoriKategoris().subscribe(
      data => {
        this.dataKategori = data.data

        this.is_loading = false
        this.dataKategori.forEach((direktori, indek) => {
          this.getTotal(indek, direktori.id_direktori_kategori)
        })
      }
    )
  }

  getTotal(index, id_direktori_kategori) {
    let param = {
      id_direktori_kategori: id_direktori_kategori,
      total: true
    }
    this.cmsDirektoriKategoriLinkService.getCmsDirektoriKategoriLinks(JSON.stringify(param)).subscribe(
      data => {
        this.dataKategori[index]['jumlah'] = data.data[0]['total']
      }
    )
  }

}
