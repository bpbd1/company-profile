import { Component, OnInit } from "@angular/core";
import { CmsSlideService, CmsMediaService, CmsSlideDetailService } from "src/app/sdk/admin/services";
import { Cms_Slide, Cms_Slide_detail } from "src/app/sdk/admin/models";

@Component({
    selector: "app-slide",
    templateUrl: "./slide.component.html",
    styleUrls: ["./slide.component.scss"],
})
export class SlideComponent implements OnInit {
    constructor(
        private cmsSlideService: CmsSlideService,
        private cmsSlideDetailService: CmsSlideDetailService,
        private mediaService: CmsMediaService
    ) { }

    ngOnInit() {
        this.getData();
    }

    id_slide: string = '560a9216-cb8c-4ac7-b356-9637b30ab485'

    dataSlide: Cms_Slide_detail[] = [];
    mySlideImages = [];
    images = [];

    mySlideOptions = { items: 1, dots: false, nav: true };
    myCarouselOptions = { items: 3, dots: false, nav: true };

    getData() {
        this.cmsSlideDetailService.getCmsSlideDetails(JSON.stringify({ id_slide: this.id_slide })).subscribe((data) => {
            this.dataSlide = data.data;
            this.mySlideImages = data.data;
            this.dataSlide.forEach((item, index) => {
                this.images.push(
                    "http://murung-admin-api.bpbd.devetek.com/download?id_media=" + item.id_media
                );
            });
        });
    }
}
