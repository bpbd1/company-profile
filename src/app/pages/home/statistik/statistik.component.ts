import { Component, OnInit } from '@angular/core';
import { CmsBencanaKategoriService, CmsBencanaKategoriLinkService, CmsBencanaService } from 'src/app/sdk/admin/services';
import { Cms_bencana_kategori } from 'src/app/sdk/admin/models';
import { graphic } from 'echarts';

@Component({
    selector: 'app-statistik',
    templateUrl: './statistik.component.html',
    styleUrls: ['./statistik.component.scss']
})
export class StatistikComponent implements OnInit {

    constructor(
        private cmsBencanaKategoriService: CmsBencanaKategoriService,
        private cmsBencanaService: CmsBencanaService,

    ) { }


    ngOnInit() {
        this.getData()


    }

    dataKategori: any[] = []


    is_loading = true
    tahun = 2019// new Date().getFullYear()
    getData() {
        this.is_loading = true

        this.cmsBencanaService
            .getCmsBencanas(
                JSON.stringify({
                    periode: this.tahun,
                    rekap: true
                }),
            )
            .subscribe(
                data => {

                    this.dataKategori = data.data
                },
                err => {
                    this.is_loading = false
                    alert("server not respon")
                }
            )

        this.cmsBencanaService
            .getCmsBencanas(
                JSON.stringify({
                    periode: this.tahun,
                    grafik: true
                }),
            )
            .subscribe(
                data => {

                    this.grafik_data = data.data
                    this.grafik()
                },
                err => {
                    this.is_loading = false
                    alert("server not respon")
                }
            )
    }

    options: any
    grafik_data: any = [];
    grafik() {


        const xAxisData = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'];

        const dataBencana = [];

        const legenda = []
        this.grafik_data.forEach(item => {
            legenda.push(item.bencana_kategori)

            let param = [];
            for (let i = 1; i <= 12; i++) {
                param.push(item['bulan' + i]);
            }
            dataBencana.push(
                {
                    name: item.bencana_kategori,
                    type: 'bar',
                    smooth: true,
                    data: param,
                    animationDelay: (idx) => idx * 10,
                }
            )
        });
        console.log(this.grafik_data)
        this.options = {
            legend: {
                data: legenda,
                align: 'left',
            },
            tooltip: {},
            xAxis: {
                data: xAxisData,
                silent: false,
                splitLine: {
                    show: false,
                },
            },
            yAxis: {},
            series: dataBencana,
            animationEasing: 'elasticOut',
            animationDelayUpdate: (idx) => idx * 5,
        };
        this.resizeChart()

    }
    echartsIntance: any
    onChartInit(ec) {
        this.echartsIntance = ec;
    }

    resizeChart() {
        if (this.echartsIntance) {
            this.echartsIntance.resize();
        }
    }


    // y: any = ''
    // label =[]

    // ngAfterViewInit() {
    //   let chart = new CanvasJS.Chart("chartContainer", {
    //     animationEnabled: true,
    //     exportEnabled: true,
    //     title: {
    //       text: "Basic Column Chart in Angular"
    //     },
    //     data: [{
    //       type: "column",
    //       dataPoints: [
    //         { y: this.y, label: this.label },

    //       ]
    //     }]
    //   });
    //   chart.render();
    // }

   
}

