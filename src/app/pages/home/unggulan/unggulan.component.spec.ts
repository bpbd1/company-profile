import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnggulanComponent } from './unggulan.component';

describe('UnggulanComponent', () => {
  let component: UnggulanComponent;
  let fixture: ComponentFixture<UnggulanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnggulanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnggulanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
