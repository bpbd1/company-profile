import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporComponent } from './lapor.component';

describe('LaporComponent', () => {
  let component: LaporComponent;
  let fixture: ComponentFixture<LaporComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
