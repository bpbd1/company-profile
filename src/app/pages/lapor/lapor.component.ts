import { Component, OnInit } from '@angular/core';
import { Cms_Pengaduan } from 'src/app/sdk/admin/models';
import { CmsPengaduanService } from 'src/app/sdk/admin/services';

@Component({
  selector: 'app-lapor',
  templateUrl: './lapor.component.html',
  styleUrls: ['./lapor.component.scss']
})
export class LaporComponent implements OnInit {

  constructor(
    private cmsPengaduanService: CmsPengaduanService,
  ) { }

  ngOnInit(): void {
    this.getLapor()
  }

  nama_lengkap: string = ''
  subjek: string = ''
  telp: string = ''
  lokasi_kejadian: string = ''
  kronologi: string = ''
  simpan() {
    let param: Cms_Pengaduan = {
      nama_lengkap: this.nama_lengkap,
      subjek: this.subjek,
      lokasi_kejadian: this.lokasi_kejadian,
      kronologi: this.kronologi,
      telp: this.telp
    };


    this.cmsPengaduanService.postCmsPengaduans(param).subscribe(
      data => {
        this.nama_lengkap = ''
        this.subjek = ''
        this.telp = ''
        this.lokasi_kejadian = ''
        this.kronologi = ''
        this.getLapor()
      },
    )
  }

  dataLapor = []

  getLapor() {
    this.cmsPengaduanService.getCmsPengaduans().subscribe(
      data=> {
        this.dataLapor = data.data
      }
    )
  }
}
