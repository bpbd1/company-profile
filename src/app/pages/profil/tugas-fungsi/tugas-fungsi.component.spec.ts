import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TugasFungsiComponent } from './tugas-fungsi.component';

describe('TugasFungsiComponent', () => {
  let component: TugasFungsiComponent;
  let fixture: ComponentFixture<TugasFungsiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TugasFungsiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TugasFungsiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
