import { Component, OnInit } from '@angular/core';
import { CmsPageService } from 'src/app/sdk/admin/services';
import { Cms_Page } from 'src/app/sdk/admin/models';

@Component({
  selector: 'app-tugas-fungsi',
  templateUrl: './tugas-fungsi.component.html',
  styleUrls: ['./tugas-fungsi.component.scss']
})
export class TugasFungsiComponent implements OnInit {

  constructor(
    private cmsPageService: CmsPageService,
  ) { }



  ngOnInit() {
    this.getData()
  }

  dataTugas: Cms_Page = {}
  dataFungsi: Cms_Page = {}
  id_tugas: string = '8c8ec3d8-8e9c-4f0f-af9a-4faeb479bd37'
  id_fungsi: string = '18423f27-b4f9-4504-a6ea-9db3515eb3aa'
  is_loading= true

  getData() {
    this.is_loading= true
    this.cmsPageService.getCmsPagesId(this.id_tugas).subscribe(
      data => {
        this.dataTugas = data.data;
        this.is_loading= false
 
      }
    )

    this.cmsPageService.getCmsPagesId(this.id_fungsi).subscribe(
      data => {
        this.dataFungsi = data.data;
        this.is_loading= false
 
      }
    )
  }

}
