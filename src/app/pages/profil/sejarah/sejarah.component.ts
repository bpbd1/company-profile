import { Component, OnInit } from '@angular/core';
import { CmsPageService } from 'src/app/sdk/admin/services';
import { Cms_Page } from 'src/app/sdk/admin/models';

@Component({
  selector: 'app-sejarah',
  templateUrl: './sejarah.component.html',
  styleUrls: ['./sejarah.component.scss']
})
export class SejarahComponent implements OnInit {

  constructor(
    private cmsPageService: CmsPageService,
  ) { }



  ngOnInit() {
    this.getData()
  }

  data: Cms_Page = {}
  id_page: string = '888c699a-19a1-49b9-9d8b-82ead707cf35'
  is_loading= true

  getData() {
    this.is_loading= true
    this.cmsPageService.getCmsPagesId(this.id_page).subscribe(
      data => {
        this.data = data.data;
        this.is_loading= false
 
      }
    )
  }


}
