import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfilRoutingModule } from './profil-routing.module';
import { SejarahComponent } from './sejarah/sejarah.component';
import { VisiMisiComponent } from './visi-misi/visi-misi.component';
import { TugasFungsiComponent } from './tugas-fungsi/tugas-fungsi.component';
import { PersonilComponent } from './personil/personil.component';
import { StrukturOrganisasiComponent } from './struktur-organisasi/struktur-organisasi.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { AppPipeModule } from 'src/app/_pipe/app-pipe.module';
import { ApiModule } from 'src/app/sdk/admin/api.module';
import { ComponentsModule } from 'src/app/components/components.module';


@NgModule({
    declarations: [SejarahComponent, VisiMisiComponent, TugasFungsiComponent, PersonilComponent, StrukturOrganisasiComponent],
    imports: [
        CommonModule,
        ProfilRoutingModule,
        NgxSkeletonLoaderModule,
        AppPipeModule,
        ComponentsModule,
        ApiModule.forRoot({ rootUrl: 'http://murung-admin-api.bpbd.devetek.com/api' }),
    ]
})
export class ProfilModule { }
