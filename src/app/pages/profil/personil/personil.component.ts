import { Component, OnInit } from '@angular/core';
import { CmsStrukturOrganisasiiService } from 'src/app/sdk/admin/services/cms-struktur-organisasii.service';
import { Cms_Struktur_organisasii } from 'src/app/sdk/admin/models/cms-_struktur-_organisasii';



@Component({
  selector: 'app-personil',
  templateUrl: './personil.component.html',
  styleUrls: ['./personil.component.scss']
})
export class PersonilComponent implements OnInit {

  constructor(
    private cmsStrukturOrganisasiiService: CmsStrukturOrganisasiiService,
  ) { }



  ngOnInit() {
    this.getData()
  }

  data: Cms_Struktur_organisasii [] = []
  is_loading= true

  getData() {
    this.is_loading= true
    this.cmsStrukturOrganisasiiService.getCmsStrukturOrganisasiis().subscribe(
      data => {
        this.data = data.data;
        this.is_loading= false
 
      }
    )
  }

}
