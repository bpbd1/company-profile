import { SejarahComponent } from "./sejarah/sejarah.component";
import { ProfilComponent } from "./profil.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { VisiMisiComponent } from './visi-misi/visi-misi.component';
import { TugasFungsiComponent } from './tugas-fungsi/tugas-fungsi.component';
import { PersonilComponent } from './personil/personil.component';
import { StrukturOrganisasiComponent } from './struktur-organisasi/struktur-organisasi.component';

const routes: Routes = [
  {
    path: "",
    component: ProfilComponent,
    children: [
      {
        path: '',
        redirectTo: 'sejarah'
      },
      {
        path: "sejarah",
        component: SejarahComponent,
      },
      {
        path: "visi-misi",
        component: VisiMisiComponent,
      },
      {
        path: "tugas-fungsi",
        component: TugasFungsiComponent,
      },
      {
        path: "personil",
        component: PersonilComponent,
      },
      {
        path: "struktur-organisasi",
        component: StrukturOrganisasiComponent,
      },
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilRoutingModule {}
