import { Component, OnInit } from '@angular/core';
import { CmsPageService } from 'src/app/sdk/admin/services';
import { Cms_Page } from 'src/app/sdk/admin/models';

@Component({
  selector: 'app-visi-misi',
  templateUrl: './visi-misi.component.html',
  styleUrls: ['./visi-misi.component.scss']
})
export class VisiMisiComponent implements OnInit {

  constructor(
    private cmsPageService: CmsPageService,
  ) { }



  ngOnInit() {
    this.getData()
  }

  dataMisi: Cms_Page = {}
  dataVisi: Cms_Page = {}
  id_visi: string = 'b6bc0f8d-6e9a-4345-8d0f-4b0663553755'
  id_misi: string = '15370898-1e52-4df8-8044-8840fe407029'
  is_loading= true

  getData() {
    this.is_loading= true
    this.cmsPageService.getCmsPagesId(this.id_visi).subscribe(
      data => {
        this.dataVisi = data.data;
        this.is_loading= false
 
      }
    )

    this.cmsPageService.getCmsPagesId(this.id_misi).subscribe(
      data => {
        this.dataMisi = data.data;
        this.is_loading= false
 
      }
    )
  }

}
