import { Component, OnInit } from '@angular/core';
import { Cms_Contact_us } from 'src/app/sdk/admin/models';
import { CmsContactUsService } from 'src/app/sdk/admin/services';

@Component({
  selector: 'app-kontak',
  templateUrl: './kontak.component.html',
  styleUrls: ['./kontak.component.scss']
})
export class KontakComponent implements OnInit {

  constructor(
    private cmsContactUsService: CmsContactUsService
  ) { }

  ngOnInit(): void {
    
  }

  nama: string = ''
  email: string = ''
  subjek: string = ''
  pesan: string = ''
  is_loading = true
  simpan() {
    let param: Cms_Contact_us = {
      nama: this.nama,
      email: this.email,
      subjek: this.subjek,
      pesan: this.pesan
    };

    this.is_loading = true
    this.cmsContactUsService.postCmsContactUses(param).subscribe(
      data => {
        this.nama = ''
        this.email = ''
        this.subjek = ''
        this.pesan = ''
        this.is_loading = false
      },
    )
  }

}
