import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KatalogRoutingModule } from './katalog-routing.module';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ComponentsModule } from 'src/app/components/components.module';
import { FormsModule } from '@angular/forms';
import { AppPipeModule } from 'src/app/_pipe/app-pipe.module';


@NgModule({
    declarations: [ListComponent, DetailComponent],
    imports: [
        CommonModule,
        KatalogRoutingModule,
        NgxSkeletonLoaderModule,
        ComponentsModule,
        AppPipeModule,
        FormsModule
    ]
})
export class KatalogModule { }
