import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CmsDirektoriService, CmsDirektoriKategoriService } from 'src/app/sdk/admin/services';
import { Cms_Direktori, Cms_Direktori_kategori } from 'src/app/sdk/admin/models';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    constructor(
        private cmsDirektoriService: CmsDirektoriService,
        private cmsDirektoriKategoriService: CmsDirektoriKategoriService,
        private activeRouter: ActivatedRoute
    ) { }

    bulan = [
        {
            id:'1',
            bulan:"Januari"
        },
        {
            id:'2',
            bulan:"Februari"
        },
        {
            id:'3',
            bulan:"Maret"
        },
        {
            id:'4',
            bulan:"April"
        },
        {
            id:'5',
            bulan:"Mei"
        },
        {
            id:'6',
            bulan:"Juni"
        },
        {
            id:'7',
            bulan:"Juli"
        },
        {
            id:'8',
            bulan:"Agustus"
        },
        {
            id:'9',
            bulan:"September"
        },
        {
            id:'10',
            bulan:"Oktober"
        },
        {
            id:'11',
            bulan:"November"
        },
        {
            id:'12',
            bulan:"Desember"
        }
    ]

    year_current = new Date().getFullYear();
    year_ago = new Date().getFullYear() - 5;
    year = []


    id_kategori = null;
    direktori = null;
    waktu = null;
    is_loading = true
    ngOnInit(): void {
        this.activeRouter.paramMap.subscribe(
            data => {
                if (data.get('id')) {
                    this.id_kategori = data.get('id');
                    this.searchData()
                    this.getData()
                }
            }
        )

        
        // console.log(this.year)
        for (let i = this.year_ago ; i <= this.year_current ; i++) {
            // console.log ("Block statement execution no." + i);
            this.year.push({'tahun' : i})
        }
    }

    kategori: Cms_Direktori []= []
    getKategori() {
        this.is_loading = true
        this.cmsDirektoriService.getCmsDirektoris(JSON.stringify({id_direktori_kategori: this.id_kategori})).subscribe(
            data => {
                this.kategori = data.data
                this.is_loading = false
            },
            err => {
                this.is_loading = false
                alert("server not respon")
            }
        )
    }

    dataKategori: Cms_Direktori_kategori = {}
    getData() {
        this.is_loading = true
        this.cmsDirektoriKategoriService.getCmsDirektoriKategorisId(this.id_kategori).subscribe(
            data => {
                this.dataKategori = data.data
                this.is_loading = false
            },
            err => {
                this.is_loading = false
                alert("server not respon")
            }
        )
    }

    kata_kunci: string = ''
    tahun_form: string = ''
    bulan_form: string = ''
    searchData(): void {
        this.cmsDirektoriService
            .getCmsDirektoris(
                JSON.stringify({
                    kata_kunci: this.kata_kunci,
                    bulan:this.bulan_form,
                    tahun:this.tahun_form,
                    id_direktori_kategori: this.id_kategori
                }),
            )
            .subscribe(
                data => {
                    this.kategori = data.data
                    // this.bulan_form = null
                    // this.tahun_form = null
                    // this.kata_kunci = null
                    this.is_loading = false
                },
                err => {
                    this.is_loading = false
                    alert("server not respon")
                }
            )
    }

    
    simpan() {
        console.log('test')
    //   let param: Cms_Pengaduan = {
    //     kata_kunci: this.kata_kunci,
    //     tahun_form: this.tahun_form,
    //     bulan_form: this.bulan_form
    //   };
  
  
    //   this.cmsPengaduanService.postCmsPengaduans(param).subscribe(
    //     data => {
    //       console.log('oke')
    //     },
    //   )
    }

    // pageSize = 10
    // pageIndex = 1
    // total = 0

    // listDirektori: [] = []

    // getDirektori() {

    //     let param = {
    //         pageSize: this.pageSize,
    //         pageIndex: this.pageIndex,
    //         // id_kategori: this.id_kategori

    //     }
    //     this.cmsDirektoriService.getCmsDirektoris(JSON.stringify(param)).subscribe(
    //         data => {
    //             this.kategori = data.data['data'];
    //             this.total = data.data['total']
    //         },
    //         err => {
    //             alert("server not respon")
    //         }
    //     )
    // }

}
