import { Component, OnInit } from '@angular/core';
import { DirektoriService } from 'src/app/sdk/end/services';
import { ActivatedRoute } from '@angular/router';
import { Direktori } from 'src/app/sdk/end/models';
import { CmsDirektoriService } from 'src/app/sdk/admin/services';
import { Cms_Direktori } from 'src/app/sdk/admin/models';

@Component({
    selector: 'app-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

    constructor(
        private cmsDirektoriService: CmsDirektoriService,
        private activeRouter: ActivatedRoute
    ) { }

    id_direktori = null
    is_loading = true
    ngOnInit(): void {
        this.activeRouter.paramMap.subscribe(
            data => {
                if (data.get('id')) {
                    this.id_direktori = data.get('id')

                    this.getData()
                }
            }
        )
    }

    direktori: Cms_Direktori = {}
    getData() {
        this.is_loading = true
        this.cmsDirektoriService.getCmsDirektorisId(this.id_direktori).subscribe(
            data => {
                this.direktori = data.data
                this.is_loading = false
            },
            err => {
                this.is_loading = false
                alert("server not respon")
            }
        )
    }


}
