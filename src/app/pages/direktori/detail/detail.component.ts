import { Component, OnInit } from '@angular/core';
import { DirektoriService } from 'src/app/sdk/end/services';
import { ActivatedRoute } from '@angular/router';
import { Direktori } from 'src/app/sdk/end/models';
import { CmsBencanaService, AdministrasiKelurahanService, AdministrasiKecamatanService, AdministrasiKabkotService, AdministrasiProvinsiService } from 'src/app/sdk/admin/services';
import { Cms_bencana, Administrasi_Kelurahan, Administrasi_Kecamatan, Administrasi_Kabkot, Administrasi_Provinsi } from 'src/app/sdk/admin/models';

@Component({
    selector: 'app-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

    constructor(
        private cmsBencanaService: CmsBencanaService,
        private administrasiKelurahanService: AdministrasiKelurahanService,
        private administrasiKecamatanService: AdministrasiKecamatanService,
        private administrasiKabkotService: AdministrasiKabkotService,
        private administrasiProvinsiService: AdministrasiProvinsiService,
        private activeRouter: ActivatedRoute
    ) { }

    id_direktori = null
    ngOnInit(): void {
        this.activeRouter.paramMap.subscribe(
            data => {
                if (data.get('id_direktori')) {
                    this.id_direktori = data.get('id_direktori')

                    this.getData()
                }
            }
        )
    }

    bencana: Cms_bencana = {}
    getData() {
        this.cmsBencanaService.getCmsBencanasId(this.id_direktori).subscribe(
            data=> {
                this.bencana = data.data
                this.getKel(data.data.desa)
                this.getKec(data.data.kecematan)
                this.getKab(data.data.kabkot)
                this.getProv(data.data.provinsi)
            }
        )
    }

    kel: Administrasi_Kelurahan = {}
    kec: Administrasi_Kecamatan = {}
    kab: Administrasi_Kabkot = {}
    prov: Administrasi_Provinsi = {}

    getKel(desa) {
        this.administrasiKelurahanService.getAdministrasiKelurahansId(desa).subscribe(
            data=> {
                this.kel = data.data
            }
        )
    }

    getKec(kecematan) {
        this.administrasiKecamatanService.getAdministrasiKecamatansId(kecematan).subscribe(
            data=> {
                this.kec = data.data
            }
        )
    }

    getKab(kabkot) {
        this.administrasiKabkotService.getAdministrasiKabkotsId(kabkot).subscribe(
            data=> {
                this.kab = data.data
            }
        )
    }

    getProv(provinsi) {
        this.administrasiProvinsiService.getAdministrasiProvinsisId(provinsi).subscribe(
            data=> {
                this.prov = data.data
            }
        )
    }

}
