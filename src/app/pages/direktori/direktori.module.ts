import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DirektoriRoutingModule } from './direktori-routing.module';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ComponentsModule } from 'src/app/components/components.module';
import { FormsModule } from '@angular/forms';

import * as echarts from 'echarts';
import { NgxEchartsModule } from 'ngx-echarts';

@NgModule({
    declarations: [ListComponent, DetailComponent],
    imports: [
        CommonModule,
        DirektoriRoutingModule,
        NgxSkeletonLoaderModule,
        ComponentsModule,
        ComponentsModule,
        FormsModule,
        NgxEchartsModule.forRoot({
            echarts
        })
    ],
    entryComponents: [
        DetailComponent
    ]
})
export class DirektoriModule { } 
