import { Component, OnInit, Input } from '@angular/core';
import { KategoriService, DirektoriService } from 'src/app/sdk/end/services';
import { ActivatedRoute } from '@angular/router';
import { Kategori, Direktori } from 'src/app/sdk/end/models';
import { CmsBencanaKategoriService, CmsBencanaService, AdministrasiKelurahanService, AdministrasiKecamatanService, AdministrasiKabkotService, AdministrasiProvinsiService } from 'src/app/sdk/admin/services';
import { Cms_bencana_kategori, Cms_bencana, Administrasi_Kelurahan, Administrasi_Kecamatan, Administrasi_Kabkot, Administrasi_Provinsi } from 'src/app/sdk/admin/models';
import { DetailComponent } from '../detail/detail.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    @Input() value = '';

    constructor(
        private cmsBencanaKategoriService: CmsBencanaKategoriService,
        private administrasiKelurahanService: AdministrasiKelurahanService,
        private administrasiKecamatanService: AdministrasiKecamatanService,
        private administrasiKabkotService: AdministrasiKabkotService,
        private administrasiProvinsiService: AdministrasiProvinsiService,
        private cmsBencanaService: CmsBencanaService,
        private activeRouter: ActivatedRoute
    ) { }




    waktu = null;
    is_loading = true
    id_kategori = null;
    direktori = null;

    list_tahun = []
    ngOnInit(): void {
        this.activeRouter.paramMap.subscribe(
            data => {
                if (data.get('id')) {
                    this.id_kategori = data.get('id');
                    this.getKategori()
                    this.searchData()
                }
            }
        )
        let a;
        for (a = 0; a <= 3; a++) {
            this.list_tahun.push(this.tahun - a);
        }


    }

    updateOptions: any;
    options: any = null;
    grafik_data: any = [];
    grafik() {


        const xAxisData = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'];

        const dataBencana = [];

        const legenda = []
        this.grafik_data.forEach(item => {
            legenda.push(item.bencana_kategori)

            let param = [];
            for (let i = 1; i <= 12; i++) {
                param.push(item['bulan' + i]);
            }
            dataBencana.push(
                {
                    name: item.bencana_kategori,
                    type: 'bar',
                    smooth: true,
                    data: param,
                    animationDelay: (idx) => idx * 10,
                }
            )
        });
        console.log(this.grafik_data)
        this.options = {
            legend: {
                data: legenda,
                align: 'left',
            },
            tooltip: {},
            xAxis: {
                data: xAxisData,
                silent: false,
                splitLine: {
                    show: false,
                },
            },
            yAxis: {},
            series: dataBencana,
            animationEasing: 'elasticOut',
            animationDelayUpdate: (idx) => idx * 5,
        };
        this.resizeChart()

    }
    echartsIntance: any
    onChartInit(ec) {
        this.echartsIntance = ec;
    }

    resizeChart() {
        if (this.echartsIntance) {
            this.echartsIntance.resize();
        }
    }

    kategori: Cms_bencana_kategori = {}
    getKategori() {
        this.is_loading = true
        this.cmsBencanaKategoriService.getCmsBencanaKategorisId(this.id_kategori).subscribe(
            data => {
                this.kategori = data.data
                this.is_loading = false
            },
            err => {
                this.is_loading = false
                alert("server not respon")
            }
        )
    }



    dataBencana = []

    orderby = 'asc'
    order = 'tanggal'

    tahun = (new Date()).getFullYear()
    searchData(order = null): void {
        this.is_loading = true
        this.cmsBencanaService
            .getCmsBencanas(
                JSON.stringify({
                    periode: this.tahun,
                    id_bencana_kategori: [this.id_kategori],
                    sortValue: this.orderby == 'desc' ? 'asc' : 'desc',
                    sortKey: this.order
                }),
            )
            .subscribe(
                data => {
                    this.orderby = this.orderby == 'desc' ? 'asc' : 'desc';
                    this.dataBencana = data.data;
                    this.is_loading = false;
                },
                err => {
                    this.is_loading = false
                    alert("server not respon")
                }
            )

        this.cmsBencanaService
            .getCmsBencanas(
                JSON.stringify({
                    periode: this.tahun,
                    id_bencana_kategori: [this.id_kategori],
                    sortValue: this.orderby == 'desc' ? 'asc' : 'desc',
                    sortKey: this.order,
                    grafik: true
                }),
            )
            .subscribe(
                data => {

                    this.grafik_data = data.data
                    this.grafik()
                },
                err => {
                    this.is_loading = false
                    alert("server not respon")
                }
            )

    }

    bencana: Cms_bencana = {}
    getDetail(id) {
        this.cmsBencanaService.getCmsBencanasId(id).subscribe(
            data => {
                this.bencana = data.data
                this.getKel(data.data.desa)
                this.getKec(data.data.kecematan)
                this.getKab(data.data.kabkot)
                this.getProv(data.data.provinsi)
            }
        )
    }

    kel: Administrasi_Kelurahan = {}
    kec: Administrasi_Kecamatan = {}
    kab: Administrasi_Kabkot = {}
    prov: Administrasi_Provinsi = {}

    getKel(desa) {
        this.administrasiKelurahanService.getAdministrasiKelurahansId(desa).subscribe(
            data => {
                this.kel = data.data
            }
        )
    }

    getKec(kecematan) {
        this.administrasiKecamatanService.getAdministrasiKecamatansId(kecematan).subscribe(
            data => {
                this.kec = data.data
            }
        )
    }

    getKab(kabkot) {
        this.administrasiKabkotService.getAdministrasiKabkotsId(kabkot).subscribe(
            data => {
                this.kab = data.data
            }
        )
    }

    getProv(provinsi) {
        this.administrasiProvinsiService.getAdministrasiProvinsisId(provinsi).subscribe(
            data => {
                this.prov = data.data
            }
        )
    }

    //    id_model: string = null;
    //    getDetail() {
    //         const drawerRef = this.drawerService.create<DetailComponent, { value: string }, string>({
    //             nzTitle: 'Search Model',
    //             nzContent: SearchItemComponent,
    //             nzWidth: '900px',
    //             nzContentParams: {
    //                 value: 'arifan'
    //             }
    //         });

    //         drawerRef.afterOpen.subscribe(() => {
    //             console.log('Drawer(Component) open');
    //         });

    //         drawerRef.afterClose.subscribe(data => {

    //             if (typeof data === 'string') {
    //                 this.id_model = data;
    //                 this.beforeModel()
    //             }
    //         });
    //     }
}
