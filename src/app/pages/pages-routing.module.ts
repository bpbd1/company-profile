import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { NotFountComponent } from "./not-fount/not-fount.component";
import { KontakComponent } from './kontak/kontak.component';
import { LaporComponent } from './lapor/lapor.component';

const routes: Routes = [
  {
    path: "",
    loadChildren: () => import("./home/home.module").then((m) => m.HomeModule),
  },
  {
    path: "profil",
    loadChildren: () =>
      import("./profil/profil.module").then((m) => m.ProfilModule),
  },
  {
    path: "direktori",
    loadChildren: () =>
      import("./direktori/direktori.module").then((m) => m.DirektoriModule),
  },
  {
    path: "katalog",
    loadChildren: () =>
      import("./katalog/katalog.module").then((m) => m.KatalogModule),
  },
  {
    path: "artikel",
    loadChildren: () =>
      import("./artikel/artikel.module").then((m) => m.ArtikelModule),
  },
  {
    path: "disclaimer",
    loadChildren: () =>
      import("./disclaimer/disclaimer.module").then((m) => m.DisclaimerModule),
  },
  {
    path: "kontak",
    component: KontakComponent,
  },
  {
    path: "lapor",
    component: LaporComponent,
  },
  {
    path: "**",
    component: NotFountComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
