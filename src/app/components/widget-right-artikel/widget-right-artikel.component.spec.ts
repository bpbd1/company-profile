import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetRightArtikelComponent } from './widget-right-artikel.component';

describe('WidgetRightArtikelComponent', () => {
  let component: WidgetRightArtikelComponent;
  let fixture: ComponentFixture<WidgetRightArtikelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetRightArtikelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetRightArtikelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
