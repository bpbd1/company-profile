import { Component, OnInit } from '@angular/core';
import { CmsPostTypeService, CmsPostService } from 'src/app/sdk/admin/services';
import { Cms_Post } from 'src/app/sdk/admin/models';

@Component({
  selector: 'app-widget-right-artikel',
  templateUrl: './widget-right-artikel.component.html',
  styleUrls: ['./widget-right-artikel.component.scss']
})
export class WidgetRightArtikelComponent implements OnInit {

  constructor(
    private cmsPostTypeService: CmsPostTypeService,
    private cmsPostService: CmsPostService,
  ) { }

  ngOnInit(): void {
    this.getData()
    this.getPost()
  }
  dataType = []
  getData() {
    this.cmsPostTypeService.getCmsPostTypes().subscribe(
      data => {
        this.dataType = data.data
      },
      err => {
        alert("server not respon")
      }
    )
  }

  pageSize = 8
  pageIndex = 1
  total = 0

  id_post_type = 'all'
  dataPost: Cms_Post[] = []
  getPost() {

    let param = {
      pageSize: this.pageSize,
      pageIndex: this.pageIndex,
      id_post_type: this.id_post_type

    }
    this.cmsPostService.getCmsPosts(JSON.stringify(param)).subscribe(
      data => {
        this.dataPost = data.data['data'];
        this.total = data.data['total']
      },
      err => {
        alert("server not respon")
      }
    )
  }

}
