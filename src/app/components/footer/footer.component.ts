import { Component, OnInit } from '@angular/core';
import { CmsBencanaService, CmsPageService, CmsDirektoriService, CmsBencanaKategoriService, CmsDirektoriKategoriService, CmsWidgetService } from 'src/app/sdk/admin/services';
import { Cms_Direktori, Cms_Page, Cms_bencana, Cms_bencana_kategori, Cms_Direktori_kategori, Cms_Widget } from 'src/app/sdk/admin/models';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(
    private cmsPageService: CmsPageService,
    private cmsBencanaKategoriService: CmsBencanaKategoriService,
    private cmsDirektoriKategoriService: CmsDirektoriKategoriService,
    private cmsWidgetService: CmsWidgetService,
  ) { }


  ngOnInit() {
    this.getData()
  }

  id_home : string = '0ea3a374-a37e-484f-ac52-50ec659071d9'
  id_kontak: string = '7b12e1bd-994a-4deb-a56e-9adc9ea358b1'

  dataDirektori: Cms_Direktori_kategori[] = []
  dataPage: Cms_Page[] = []
  dataBencana: Cms_bencana_kategori[] = []
  kontak: Cms_Widget =  {}
  home: Cms_Widget =  {}

  is_loading = true
  id_parent: string = '523de87b-5939-4ea4-9cb7-2c5a7449ef4d'

  getData() {
    this.is_loading = true
    this.cmsBencanaKategoriService.getCmsBencanaKategoris().subscribe(
      data => {
        this.dataBencana = data.data
      }
    )

    this.cmsPageService.getCmsPages(JSON.stringify({id_parent: this.id_parent})).subscribe(
      data => {
        this.dataPage = data.data
      }
    )

    this.cmsDirektoriKategoriService.getCmsDirektoriKategoris().subscribe(
      data => {
        this.dataDirektori = data.data
      }
    )

    this.cmsWidgetService.getCmsWidgetsId(this.id_home).subscribe(
      data => {
        this.home = data.data
      }
    )

    this.cmsWidgetService.getCmsWidgetsId(this.id_kontak).subscribe(
      data => {
        this.kontak = data.data
      }
    )
    this.is_loading = false
  }
}
