import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FooterComponent } from "./footer/footer.component";
import { MapComponent } from "./map/map.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { PreloaderComponent } from "./preloader/preloader.component";
import { SubscribeComponent } from "./subscribe/subscribe.component";
import { TopHeaderComponent } from "./top-header/top-header.component";
import { RouterModule } from "@angular/router";
import { WidgetRightComponent } from './widget-right/widget-right.component';
import { AppPipeModule } from '../_pipe/app-pipe.module';
import { FooterKontakComponent } from './footer-kontak/footer-kontak.component';
import { WidgetRightArtikelComponent } from './widget-right-artikel/widget-right-artikel.component';

const COM = [
    FooterComponent,
    MapComponent,
    NavbarComponent,
    PreloaderComponent,
    SubscribeComponent,
    TopHeaderComponent,
    FooterKontakComponent,

    WidgetRightComponent,
    WidgetRightArtikelComponent
];

@NgModule({
    declarations: [...COM ],
    imports: [CommonModule, RouterModule, AppPipeModule,],
    exports: [...COM]
})
export class ComponentsModule { }
