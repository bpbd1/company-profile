import { Component, OnInit } from '@angular/core';
import { Kategori } from 'src/app/sdk/end/models';
import { KategoriService } from 'src/app/sdk/end/services';
import { CmsBencanaKategoriService, CmsPetaKategoriService, CmsDirektoriKategoriService } from 'src/app/sdk/admin/services';

@Component({
    selector: 'app-widget-right',
    templateUrl: './widget-right.component.html',
    styleUrls: ['./widget-right.component.scss']
})
export class WidgetRightComponent implements OnInit {

    constructor(
        private kategoriService: KategoriService,
        private cmsBencanaKategoriService: CmsBencanaKategoriService,
        private cmsDirektoriKategoriService: CmsDirektoriKategoriService,
    ) { }

    bencana = []
    kategori = []

    ngOnInit(): void {
        // this.getKategori('direktori');
        // this.getKategori('katalog');

        this.cmsBencanaKategoriService.getCmsBencanaKategoris().subscribe(
            data => {
                this.bencana = data.data
            }
        )

        this.cmsDirektoriKategoriService.getCmsDirektoriKategoris().subscribe(
            data => {
                this.kategori = data.data
            }
        )

    }

    // listKategori = {};
    // getKategori(jenis) {
    //     this.kategoriService.getEndKategoris(JSON.stringify({ 'for_module': jenis })).subscribe(
    //         data => {
    //             this.listKategori[jenis] = data.data
    //         }
    //     )
    // }

}
