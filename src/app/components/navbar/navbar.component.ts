import { Component, OnInit } from '@angular/core';
import { CmsPageService, CmsBencanaKategoriService, CmsPetaKategoriService, CmsDirektoriKategoriService, CmsPostTypeService } from 'src/app/sdk/admin/services';


@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

    constructor(
        private cmsPageService: CmsPageService,
        private cmsBencanaKategoriService: CmsBencanaKategoriService,
        private cmsDirektoriKategoriService: CmsDirektoriKategoriService,
        private cmsPostTypeService: CmsPostTypeService,

    ) { }

    id_parent: string = '523de87b-5939-4ea4-9cb7-2c5a7449ef4d'
    profile = []
    bencana = []
    kategori = []
    post = []
    ngOnInit() {

        this.cmsPageService.getCmsPages(JSON.stringify({id_parent : this.id_parent})).subscribe(
            data => {
                this.profile = data.data
            }
        )

        this.cmsBencanaKategoriService.getCmsBencanaKategoris().subscribe(
            data => {
                this.bencana = data.data
            }
        )

        this.cmsDirektoriKategoriService.getCmsDirektoriKategoris().subscribe(
            data => {
                this.kategori = data.data
            }
        )

        this.cmsPostTypeService.getCmsPostTypes().subscribe(
            data => {
                this.post = data.data
            }
        )
    }

}
