import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterKontakComponent } from './footer-kontak.component';

describe('FooterKontakComponent', () => {
  let component: FooterKontakComponent;
  let fixture: ComponentFixture<FooterKontakComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterKontakComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterKontakComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
