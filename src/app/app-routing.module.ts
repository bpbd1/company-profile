import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./pages/home/home.component";
import { StandarComponent } from "./layout/standar/standar.component";

const routes: Routes = [
    {
        path: "",
        component: StandarComponent,
        loadChildren: () => import("./pages/pages.module").then(m => m.PagesModule)
    },



];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
