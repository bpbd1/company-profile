import { BrowserModule } from "@angular/platform-browser";
import { NgModule, Sanitizer } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LayoutModule } from "./layout/layout.module";
import { ComponentsModule } from "./components/components.module";
import { LoadingBarHttpClientModule } from "@ngx-loading-bar/http-client";
import { ApiModule } from "./sdk/admin/api.module";
import * as echarts from 'echarts';
import { NgxEchartsModule } from 'ngx-echarts';

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        AppRoutingModule,
        LayoutModule,
        ComponentsModule,
        LoadingBarHttpClientModule,
        // ApiModule.forRoot({ rootUrl: 'http://bpt.devetek.com/api' }),
        ApiModule.forRoot({ rootUrl: "http://murung-api.bpbd.devetek.com/api" }),
        NgxEchartsModule.forRoot({
            echarts
        })
    ],

    bootstrap: [AppComponent],
})
export class AppModule { }
