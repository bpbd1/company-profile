/* tslint:disable */
import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ApiConfiguration, ApiConfigurationInterface } from './api-configuration';

import { AdministrasiKabkotService } from './services/administrasi-kabkot.service';
import { AdministrasiKecamatanService } from './services/administrasi-kecamatan.service';
import { AdministrasiKelurahanService } from './services/administrasi-kelurahan.service';
import { AdministrasiProvinsiService } from './services/administrasi-provinsi.service';
import { AuthAuthJwtService } from './services/auth-auth-jwt.service';
import { CmsContactUsService } from './services/cms-contact-us.service';
import { CmsDirektoriService } from './services/cms-direktori.service';
import { CmsDirektoriKategoriService } from './services/cms-direktori-kategori.service';
import { CmsDirektoriKategoriLinkService } from './services/cms-direktori-kategori-link.service';
import { CmsDocumentService } from './services/cms-document.service';
import { CmsGaleryService } from './services/cms-galery.service';
import { CmsGaleryDetailService } from './services/cms-galery-detail.service';
import { CmsKategoriService } from './services/cms-kategori.service';
import { CmsMediaService } from './services/cms-media.service';
import { CmsPageService } from './services/cms-page.service';
import { CmsPengaduanService } from './services/cms-pengaduan.service';
import { CmsPetaService } from './services/cms-peta.service';
import { CmsPetaDataService } from './services/cms-peta-data.service';
import { CmsPetaKategoriService } from './services/cms-peta-kategori.service';
import { CmsPostService } from './services/cms-post.service';
import { CmsPostKategoriService } from './services/cms-post-kategori.service';
import { CmsPostTypeService } from './services/cms-post-type.service';
import { CmsSlideService } from './services/cms-slide.service';
import { CmsSlideDetailService } from './services/cms-slide-detail.service';
import { CmsStrukturOrganisasiiService } from './services/cms-struktur-organisasii.service';
import { CmsWidgetService } from './services/cms-widget.service';
import { CmsBencanaService } from './services/cms-bencana.service';
import { CmsBencanaKategoriService } from './services/cms-bencana-kategori.service';
import { CmsBencanaKategoriLinkService } from './services/cms-bencana-kategori-link.service';
import { CmsJalurEvakuasiService } from './services/cms-jalur-evakuasi.service';
import { CmsSpotService } from './services/cms-spot.service';
import { CmsSpotKategoriService } from './services/cms-spot-kategori.service';

/**
 * Provider for all Api services, plus ApiConfiguration
 */
@NgModule({
  imports: [
    HttpClientModule
  ],
  exports: [
    HttpClientModule
  ],
  declarations: [],
  providers: [
    ApiConfiguration,
    AdministrasiKabkotService,
    AdministrasiKecamatanService,
    AdministrasiKelurahanService,
    AdministrasiProvinsiService,
    AuthAuthJwtService,
    CmsContactUsService,
    CmsDirektoriService,
    CmsDirektoriKategoriService,
    CmsDirektoriKategoriLinkService,
    CmsDocumentService,
    CmsGaleryService,
    CmsGaleryDetailService,
    CmsKategoriService,
    CmsMediaService,
    CmsPageService,
    CmsPengaduanService,
    CmsPetaService,
    CmsPetaDataService,
    CmsPetaKategoriService,
    CmsPostService,
    CmsPostKategoriService,
    CmsPostTypeService,
    CmsSlideService,
    CmsSlideDetailService,
    CmsStrukturOrganisasiiService,
    CmsWidgetService,
    CmsBencanaService,
    CmsBencanaKategoriService,
    CmsBencanaKategoriLinkService,
    CmsJalurEvakuasiService,
    CmsSpotService,
    CmsSpotKategoriService
  ],
})
export class ApiModule {
  static forRoot(customParams: ApiConfigurationInterface): ModuleWithProviders<ApiModule> {
    return {
      ngModule: ApiModule,
      providers: [
        {
          provide: ApiConfiguration,
          useValue: {rootUrl: customParams.rootUrl}
        }
      ]
    }
  }
}
