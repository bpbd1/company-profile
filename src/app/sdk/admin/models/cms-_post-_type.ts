/* tslint:disable */
export interface Cms_Post_type {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * icon
   */
  icon?: string;

  /**
   * id_post_type
   */
  id_post_type?: string;

  /**
   * post_type
   */
  post_type?: string;

  /**
   * slung
   */
  slung?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
