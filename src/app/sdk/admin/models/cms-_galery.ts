/* tslint:disable */
export interface Cms_Galery {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * id_galery
   */
  id_galery?: string;

  /**
   * keterangan
   */
  keterangan?: string;

  /**
   * nama_galery
   */
  nama_galery?: string;

  /**
   * status
   */
  status?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
