/* tslint:disable */
export interface Cms_spot_kategori {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * icon
   */
  icon?: string;

  /**
   * id_spot_kategori
   */
  id_spot_kategori?: string;

  /**
   * label
   */
  label?: string;

  /**
   * spot_kategori
   */
  spot_kategori?: string;

  /**
   * ya,tidak
   */
  status?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
