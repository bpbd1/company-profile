/* tslint:disable */
export interface Cms_Media {

  /**
   * besar
   */
  besar?: string;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * id_media
   */
  id_media?: string;

  /**
   * kecil
   */
  kecil?: string;

  /**
   * nama_file
   */
  nama_file?: string;

  /**
   * original
   */
  original?: string;

  /**
   * sedang
   */
  sedang?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
