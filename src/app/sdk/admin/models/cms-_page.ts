/* tslint:disable */
export interface Cms_Page {

  /**
   * content
   */
  content?: string;

  /**
   * editor,html,video
   */
  content_type?: string;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * id_media
   */
  id_media?: string;

  /**
   * id_page
   */
  id_page?: string;

  /**
   * id_parent
   */
  id_parent?: string;

  /**
   * slug
   */
  slug?: string;

  /**
   * status
   */
  status?: string;

  /**
   * title
   */
  title?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
