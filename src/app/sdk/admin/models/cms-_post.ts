/* tslint:disable */
export interface Cms_Post {

  /**
   * content
   */
  content?: string;

  /**
   * html,editor,video
   */
  content_type?: string;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * expert
   */
  expert?: string;

  /**
   * id_media
   */
  id_media?: string;

  /**
   * id_post
   */
  id_post?: string;

  /**
   * id_post_type
   */
  id_post_type?: string;

  /**
   * id_user
   */
  id_user?: number;

  /**
   * slug
   */
  slug?: string;

  /**
   * status
   */
  status?: string;

  /**
   * title
   */
  title?: string;

  /**
   * updated_at
   */
  updated_at?: string;

  /**
   * validasi
   */
  validasi?: number;
}
