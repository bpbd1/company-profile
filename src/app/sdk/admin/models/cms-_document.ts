/* tslint:disable */
export interface Cms_Document {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * file_name
   */
  file_name?: string;

  /**
   * for_module
   */
  for_module?: string;

  /**
   * for_module_id
   */
  for_module_id?: string;

  /**
   * for_table
   */
  for_table?: string;

  /**
   * id_document
   */
  id_document?: string;

  /**
   * id_user
   */
  id_user?: number;

  /**
   * name
   */
  name?: string;

  /**
   * nomor
   */
  nomor?: string;

  /**
   * revisi
   */
  revisi?: string;

  /**
   * tanggal
   */
  tanggal?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
