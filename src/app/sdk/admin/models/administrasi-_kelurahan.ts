/* tslint:disable */
export interface Administrasi_Kelurahan {

  /**
   * id_kec
   */
  id_kec?: string;

  /**
   * id_kel
   */
  id_kel?: string;

  /**
   * nama_kel
   */
  nama_kel?: string;
}
