/* tslint:disable */
export interface Cms_Kategori {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * id_kategori
   */
  id_kategori?: string;

  /**
   * id_media
   */
  id_media?: string;

  /**
   * id_post_type
   */
  id_post_type?: string;

  /**
   * kategori
   */
  kategori?: string;

  /**
   * keterangan
   */
  keterangan?: string;

  /**
   * slung
   */
  slung?: string;

  /**
   * aktif/inaktif
   */
  status?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
