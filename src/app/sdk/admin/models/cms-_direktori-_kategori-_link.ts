/* tslint:disable */
export interface Cms_Direktori_kategori_link {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * id_direktori
   */
  id_direktori?: string;

  /**
   * id_direktori_kategori
   */
  id_direktori_kategori?: string;

  /**
   * id_direktori_kategori_link
   */
  id_direktori_kategori_link?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
