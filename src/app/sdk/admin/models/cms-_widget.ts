/* tslint:disable */
export interface Cms_Widget {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * id_media
   */
  id_media?: string;

  /**
   * id_widget
   */
  id_widget?: string;

  /**
   * judul
   */
  judul?: string;

  /**
   * text
   */
  text?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
