/* tslint:disable */
export interface Cms_bencana_kategori {

  /**
   * bencana_kategori
   */
  bencana_kategori?: string;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * icon
   */
  icon?: string;

  /**
   * id_bencana_kategori
   */
  id_bencana_kategori?: string;

  /**
   * image
   */
  image?: string;

  /**
   * slug
   */
  slug?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
