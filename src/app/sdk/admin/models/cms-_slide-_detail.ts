/* tslint:disable */
export interface Cms_Slide_detail {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * id_media
   */
  id_media?: string;

  /**
   * id_slide
   */
  id_slide?: string;

  /**
   * id_slide_detail
   */
  id_slide_detail?: string;

  /**
   * link
   */
  link?: string;

  /**
   * text
   */
  text?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
