/* tslint:disable */
export interface Cms_Peta_kategori {

  /**
   * icon
   */
  icon?: string;

  /**
   * id_peta_kategori
   */
  id_peta_kategori?: string;

  /**
   * kategori
   */
  kategori?: string;
}
