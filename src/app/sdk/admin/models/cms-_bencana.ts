/* tslint:disable */
export interface Cms_bencana {

  /**
   * coordinate
   */
  coordinate?: string;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * desa
   */
  desa?: string;

  /**
   * dusun
   */
  dusun?: string;

  /**
   * fasilitas_ibadah
   */
  fasilitas_ibadah?: number;

  /**
   * fasilitas_kesehatan
   */
  fasilitas_kesehatan?: number;

  /**
   * fasilitas_pendidikan
   */
  fasilitas_pendidikan?: number;

  /**
   * fasilitas_umum
   */
  fasilitas_umum?: number;

  /**
   * foto
   */
  foto?: string;

  /**
   * gallery
   */
  gallery?: string;

  /**
   * id_bencana
   */
  id_bencana?: string;

  /**
   * id_user
   */
  id_user?: number;

  /**
   * jumlah_kk
   */
  jumlah_kk?: number;

  /**
   * jumlah_korban_hilang
   */
  jumlah_korban_hilang?: number;

  /**
   * jumlah_korban_luka_berat
   */
  jumlah_korban_luka_berat?: number;

  /**
   * jumlah_korban_luka_ringan
   */
  jumlah_korban_luka_ringan?: number;

  /**
   * jumlah_korban_meninggal
   */
  jumlah_korban_meninggal?: number;

  /**
   * kabkot
   */
  kabkot?: string;

  /**
   * kecematan
   */
  kecematan?: string;

  /**
   * kerusakan
   */
  kerusakan?: string;

  /**
   * keterangan
   */
  keterangan?: string;

  /**
   * kios_terdampak
   */
  kios_terdampak?: number;

  /**
   * kronologi
   */
  kronologi?: string;

  /**
   * lahan_hutan
   */
  lahan_hutan?: number;

  /**
   * lahan_kebun
   */
  lahan_kebun?: number;

  /**
   * lat
   */
  lat?: any;

  /**
   * ringan,sedang,besar
   */
  level_bencana?: string;

  /**
   * lon
   */
  lon?: any;

  /**
   * luas
   */
  luas?: any;

  /**
   * masa_darurat
   */
  masa_darurat?: string;

  /**
   * penanganan
   */
  penanganan?: string;

  /**
   * penyebab
   */
  penyebab?: string;

  /**
   * provinsi
   */
  provinsi?: string;

  /**
   * rumah_terdampak
   */
  rumah_terdampak?: number;

  /**
   * tanggal
   */
  tanggal?: string;

  /**
   * tindakan
   */
  tindakan?: string;

  /**
   * updated_at
   */
  updated_at?: string;

  /**
   * validasi
   */
  validasi?: number;
}
