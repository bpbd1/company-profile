/* tslint:disable */
export interface Cms_Peta {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * id_media
   */
  id_media?: string;

  /**
   * id_peta
   */
  id_peta?: string;

  /**
   * id_peta_kategori
   */
  id_peta_kategori?: string;

  /**
   * id_user
   */
  id_user?: number;

  /**
   * kode
   */
  kode?: string;

  /**
   * name
   */
  name?: string;

  /**
   * opacity
   */
  opacity?: any;

  /**
   * ya,tidak
   */
  status?: string;

  /**
   * pbf,geojson,png
   */
  sumber_data?: string;

  /**
   * updated_at
   */
  updated_at?: string;

  /**
   * url_data
   */
  url_data?: string;

  /**
   * validasi
   */
  validasi?: number;

  /**
   * true, false
   */
  visible?: string;

  /**
   * zoom_lat
   */
  zoom_lat?: any;

  /**
   * max 15
   */
  zoom_level?: number;

  /**
   * zoom_lon
   */
  zoom_lon?: any;
}
