/* tslint:disable */
export interface Cms_Struktur_organisasii {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * deskripsi
   */
  deskripsi?: string;

  /**
   * golongan
   */
  golongan?: string;

  /**
   * id_media
   */
  id_media?: string;

  /**
   * id_struktur_organisasi
   */
  id_struktur_organisasi?: string;

  /**
   * nama_lengkap
   */
  nama_lengkap?: string;

  /**
   * nip
   */
  nip?: string;

  /**
   * pangkat
   */
  pangkat?: string;

  /**
   * pendidikan
   */
  pendidikan?: string;

  /**
   * tanggal_lahir
   */
  tanggal_lahir?: string;

  /**
   * tempat_lahir
   */
  tempat_lahir?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
