/* tslint:disable */
export interface Administrasi_Provinsi {

  /**
   * id_prov
   */
  id_prov?: string;

  /**
   * nama_prov
   */
  nama_prov?: string;
}
