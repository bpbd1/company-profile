/* tslint:disable */
export interface Cms_Contact_us {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * email
   */
  email?: string;

  /**
   * id_contact_us
   */
  id_contact_us?: string;

  /**
   * nama
   */
  nama?: string;

  /**
   * pesan
   */
  pesan?: string;

  /**
   * subjek
   */
  subjek?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
