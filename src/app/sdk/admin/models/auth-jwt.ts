/* tslint:disable */
export interface AuthJwt {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * email
   */
  email?: string;

  /**
   * email_verified_at
   */
  email_verified_at?: string;

  /**
   * id
   */
  id?: number;

  /**
   * name
   */
  name?: string;

  /**
   * password
   */
  password?: string;

  /**
   * remember_token
   */
  remember_token?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
