/* tslint:disable */
export interface Cms_bencana_kategori_link {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * id_bencana
   */
  id_bencana?: string;

  /**
   * id_bencana_kategori
   */
  id_bencana_kategori?: string;

  /**
   * id_bencana_kategori_link
   */
  id_bencana_kategori_link?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
