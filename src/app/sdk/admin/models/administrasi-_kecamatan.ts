/* tslint:disable */
export interface Administrasi_Kecamatan {

  /**
   * id_kabkot
   */
  id_kabkot?: string;

  /**
   * id_kec
   */
  id_kec?: string;

  /**
   * nama_kec
   */
  nama_kec?: string;
}
