/* tslint:disable */
export interface Administrasi_Kabkot {

  /**
   * id_kabkot
   */
  id_kabkot?: string;

  /**
   * id_prov
   */
  id_prov?: string;

  /**
   * nama_kabkot
   */
  nama_kabkot?: string;
}
