/* tslint:disable */
export interface Cms_jalur_evakuasi {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * dari
   */
  dari?: string;

  /**
   * geojson
   */
  geojson?: string;

  /**
   * id_jalur_evakuasi
   */
  id_jalur_evakuasi?: string;

  /**
   * ke
   */
  ke?: string;

  /**
   * lebar
   */
  lebar?: any;

  /**
   * line
   */
  line?: string;

  /**
   * panjang
   */
  panjang?: any;

  /**
   * petunjuk
   */
  petunjuk?: string;

  /**
   * updated_at
   */
  updated_at?: string;

  /**
   * visble
   */
  visble?: string;

  /**
   * warna
   */
  warna?: string;
}
