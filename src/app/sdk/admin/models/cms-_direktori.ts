/* tslint:disable */
export interface Cms_Direktori {

  /**
   * file
   */
   file?: string;

  /**
   * Administrator
   */
  Administrator?: string;

  /**
   * abstark
   */
  abstark?: string;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * feature
   */
  feature?: string;

  /**
   * id_direktori
   */
  id_direktori?: string;

  /**
   * id_media
   */
  id_media?: string;

  /**
   * id_user
   */
  id_user?: number;

  /**
   * judul
   */
  judul?: string;

  /**
   * kategori
   */
  kategori?: string;

  /**
   * keyword
   */
  keyword?: string;

  /**
   * kode
   */
  kode?: string;

  /**
   * kugi
   */
  kugi?: string;

  /**
   * link_id_peta
   */
  link_id_peta?: string;

  /**
   * nama_peta_wilayah
   */
  nama_peta_wilayah?: string;

  /**
   * name_file
   */
  name_file?: string;

  /**
   * namna_peta
   */
  namna_peta?: string;

  /**
   * organisasi_pengelola
   */
  organisasi_pengelola?: string;

  /**
   * shp
   */
  shp?: string;

  /**
   * size
   */
  size?: string;

  /**
   * slug
   */
  slug?: string;

  /**
   * tags
   */
  tags?: string;

  /**
   * tahun
   */
  tahun?: number;

  /**
   * tanggal_publikasi
   */
  tanggal_publikasi?: string;

  /**
   * png,mbtile,geojson,shp,dbf,pdf,jpg
   */
  tipe?: string;

  /**
   * updated_at
   */
  updated_at?: string;

  /**
   * validasi
   */
  validasi?: number;

  /**
   * walidata
   */
  walidata?: string;

  /**
   * wilayah
   */
  wilayah?: string;
}
