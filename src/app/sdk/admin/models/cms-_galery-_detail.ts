/* tslint:disable */
export interface Cms_Galery_detail {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * id_galery
   */
  id_galery?: string;

  /**
   * id_galery_detail
   */
  id_galery_detail?: string;

  /**
   * id_media
   */
  id_media?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
