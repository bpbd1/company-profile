/* tslint:disable */
export interface Cms_Pengaduan {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * id_media
   */
  id_media?: string;

  /**
   * id_pengaduan
   */
  id_pengaduan?: string;

  /**
   * kronologi
   */
  kronologi?: string;

  /**
   * lokasi_kejadian
   */
  lokasi_kejadian?: string;

  /**
   * nama_lengkap
   */
  nama_lengkap?: string;

  /**
   * subjek
   */
  subjek?: string;

  /**
   * tanggal_kejadian
   */
  tanggal_kejadian?: string;

  /**
   * telp
   */
  telp?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
