/* tslint:disable */
export interface Cms_spot {

  /**
   * coordinate
   */
  coordinate?: string;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * desa
   */
  desa?: string;

  /**
   * foto
   */
  foto?: string;

  /**
   * id_spot
   */
  id_spot?: string;

  /**
   * id_spot_kategori
   */
  id_spot_kategori?: string;

  /**
   * id_user
   */
  id_user?: number;

  /**
   * kabkot
   */
  kabkot?: string;

  /**
   * kecematan
   */
  kecematan?: string;

  /**
   * kelurahan
   */
  kelurahan?: string;

  /**
   * keterangan
   */
  keterangan?: string;

  /**
   * label
   */
  label?: string;

  /**
   * lat
   */
  lat?: any;

  /**
   * lon
   */
  lon?: any;

  /**
   * provinsi
   */
  provinsi?: string;

  /**
   * ya,tidak
   */
  status_spot?: string;

  /**
   * tanggal_take
   */
  tanggal_take?: string;

  /**
   * updated_at
   */
  updated_at?: string;

  /**
   * validasi
   */
  validasi?: number;

  /**
   * visible
   */
  visible?: string;
}
