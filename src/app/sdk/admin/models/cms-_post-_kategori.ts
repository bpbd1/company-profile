/* tslint:disable */
export interface Cms_Post_kategori {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * id_kategori
   */
  id_kategori?: string;

  /**
   * id_post
   */
  id_post?: string;

  /**
   * id_post_kategori
   */
  id_post_kategori?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
