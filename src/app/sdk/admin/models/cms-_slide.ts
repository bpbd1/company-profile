/* tslint:disable */
export interface Cms_Slide {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * id_media
   */
  id_media?: string;

  /**
   * id_slide
   */
  id_slide?: string;

  /**
   * id_user
   */
  id_user?: number;

  /**
   * judul
   */
  judul?: string;

  /**
   * link
   */
  link?: string;

  /**
   * status
   */
  status?: string;

  /**
   * text_1
   */
  text_1?: string;

  /**
   * text_2
   */
  text_2?: string;

  /**
   * updated_at
   */
  updated_at?: string;

  /**
   * validasi
   */
  validasi?: number;
}
