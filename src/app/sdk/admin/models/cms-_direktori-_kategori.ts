/* tslint:disable */
export interface Cms_Direktori_kategori {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * icon
   */
  icon?: string;

  /**
   * id_direktori_kategori
   */
  id_direktori_kategori?: string;

  /**
   * image
   */
  image?: string;

  /**
   * kategori
   */
  kategori?: string;

  /**
   * slug
   */
  slug?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
