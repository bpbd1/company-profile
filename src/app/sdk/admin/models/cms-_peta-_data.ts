/* tslint:disable */
export interface Cms_Peta_data {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * id_peta
   */
  id_peta?: string;

  /**
   * id_peta_data
   */
  id_peta_data?: string;

  /**
   * key_unik
   */
  key_unik?: string;

  /**
   * klasifikasi
   */
  klasifikasi?: string;

  /**
   * luas
   */
  luas?: any;

  /**
   * nama
   */
  nama?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
