/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Galery_detail } from '../models/cms-_galery-_detail';
@Injectable({
  providedIn: 'root',
})
class CmsGaleryDetailService extends __BaseService {
  static readonly getCmsGaleryDetailsPath = '/cms/galery_details';
  static readonly postCmsGaleryDetailsPath = '/cms/galery_details';
  static readonly getCmsGaleryDetailsIdPath = '/cms/galery_details/{id}';
  static readonly putCmsGaleryDetailsIdPath = '/cms/galery_details/{id}';
  static readonly deleteCmsGaleryDetailsIdPath = '/cms/galery_details/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Galery_details.
   *
   * Get all Galery_details
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsGaleryDetailsResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Galery_detail>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/galery_details`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Galery_detail>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Galery_details.
   *
   * Get all Galery_details
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsGaleryDetails(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Galery_detail>, message?: string}> {
    return this.getCmsGaleryDetailsResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Galery_detail>, message?: string})
    );
  }

  /**
   * Store a newly created Galery_detail in storage
   *
   * Store Galery_detail
   * @param body Galery_detail that should be stored
   * @return successful operation
   */
  postCmsGaleryDetailsResponse(body?: Cms_Galery_detail): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Galery_detail, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/galery_details`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Galery_detail, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Galery_detail in storage
   *
   * Store Galery_detail
   * @param body Galery_detail that should be stored
   * @return successful operation
   */
  postCmsGaleryDetails(body?: Cms_Galery_detail): __Observable<{success?: boolean, data?: Cms_Galery_detail, message?: string}> {
    return this.postCmsGaleryDetailsResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Galery_detail, message?: string})
    );
  }

  /**
   * Display the specified Galery_detail
   *
   * Get Galery_detail
   * @param id id of Galery_detail
   * @return successful operation
   */
  getCmsGaleryDetailsIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Galery_detail, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/galery_details/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Galery_detail, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Galery_detail
   *
   * Get Galery_detail
   * @param id id of Galery_detail
   * @return successful operation
   */
  getCmsGaleryDetailsId(id: string): __Observable<{success?: boolean, data?: Cms_Galery_detail, message?: string}> {
    return this.getCmsGaleryDetailsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Galery_detail, message?: string})
    );
  }

  /**
   * Update the specified Galery_detail in storage
   *
   * Update Galery_detail
   * @param params The `CmsGaleryDetailService.PutCmsGaleryDetailsIdParams` containing the following parameters:
   *
   * - `id`: id of Galery_detail
   *
   * - `body`: Galery_detail that should be updated
   *
   * @return successful operation
   */
  putCmsGaleryDetailsIdResponse(params: CmsGaleryDetailService.PutCmsGaleryDetailsIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Galery_detail, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/galery_details/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Galery_detail, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Galery_detail in storage
   *
   * Update Galery_detail
   * @param params The `CmsGaleryDetailService.PutCmsGaleryDetailsIdParams` containing the following parameters:
   *
   * - `id`: id of Galery_detail
   *
   * - `body`: Galery_detail that should be updated
   *
   * @return successful operation
   */
  putCmsGaleryDetailsId(params: CmsGaleryDetailService.PutCmsGaleryDetailsIdParams): __Observable<{success?: boolean, data?: Cms_Galery_detail, message?: string}> {
    return this.putCmsGaleryDetailsIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Galery_detail, message?: string})
    );
  }

  /**
   * Remove the specified Galery_detail from storage
   *
   * Delete Galery_detail
   * @param id id of Galery_detail
   * @return successful operation
   */
  deleteCmsGaleryDetailsIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/galery_details/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Galery_detail from storage
   *
   * Delete Galery_detail
   * @param id id of Galery_detail
   * @return successful operation
   */
  deleteCmsGaleryDetailsId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsGaleryDetailsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsGaleryDetailService {

  /**
   * Parameters for putCmsGaleryDetailsId
   */
  export interface PutCmsGaleryDetailsIdParams {

    /**
     * id of Galery_detail
     */
    id: string;

    /**
     * Galery_detail that should be updated
     */
    body?: Cms_Galery_detail;
  }
}

export { CmsGaleryDetailService }
