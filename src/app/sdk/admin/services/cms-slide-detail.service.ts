/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Slide_detail } from '../models/cms-_slide-_detail';
@Injectable({
  providedIn: 'root',
})
class CmsSlideDetailService extends __BaseService {
  static readonly getCmsSlideDetailsPath = '/cms/slide_details';
  static readonly postCmsSlideDetailsPath = '/cms/slide_details';
  static readonly getCmsSlideDetailsIdPath = '/cms/slide_details/{id}';
  static readonly putCmsSlideDetailsIdPath = '/cms/slide_details/{id}';
  static readonly deleteCmsSlideDetailsIdPath = '/cms/slide_details/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Slide_details.
   *
   * Get all Slide_details
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsSlideDetailsResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Slide_detail>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/slide_details`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Slide_detail>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Slide_details.
   *
   * Get all Slide_details
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsSlideDetails(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Slide_detail>, message?: string}> {
    return this.getCmsSlideDetailsResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Slide_detail>, message?: string})
    );
  }

  /**
   * Store a newly created Slide_detail in storage
   *
   * Store Slide_detail
   * @param body Slide_detail that should be stored
   * @return successful operation
   */
  postCmsSlideDetailsResponse(body?: Cms_Slide_detail): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Slide_detail, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/slide_details`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Slide_detail, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Slide_detail in storage
   *
   * Store Slide_detail
   * @param body Slide_detail that should be stored
   * @return successful operation
   */
  postCmsSlideDetails(body?: Cms_Slide_detail): __Observable<{success?: boolean, data?: Cms_Slide_detail, message?: string}> {
    return this.postCmsSlideDetailsResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Slide_detail, message?: string})
    );
  }

  /**
   * Display the specified Slide_detail
   *
   * Get Slide_detail
   * @param id id of Slide_detail
   * @return successful operation
   */
  getCmsSlideDetailsIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Slide_detail, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/slide_details/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Slide_detail, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Slide_detail
   *
   * Get Slide_detail
   * @param id id of Slide_detail
   * @return successful operation
   */
  getCmsSlideDetailsId(id: string): __Observable<{success?: boolean, data?: Cms_Slide_detail, message?: string}> {
    return this.getCmsSlideDetailsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Slide_detail, message?: string})
    );
  }

  /**
   * Update the specified Slide_detail in storage
   *
   * Update Slide_detail
   * @param params The `CmsSlideDetailService.PutCmsSlideDetailsIdParams` containing the following parameters:
   *
   * - `id`: id of Slide_detail
   *
   * - `body`: Slide_detail that should be updated
   *
   * @return successful operation
   */
  putCmsSlideDetailsIdResponse(params: CmsSlideDetailService.PutCmsSlideDetailsIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Slide_detail, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/slide_details/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Slide_detail, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Slide_detail in storage
   *
   * Update Slide_detail
   * @param params The `CmsSlideDetailService.PutCmsSlideDetailsIdParams` containing the following parameters:
   *
   * - `id`: id of Slide_detail
   *
   * - `body`: Slide_detail that should be updated
   *
   * @return successful operation
   */
  putCmsSlideDetailsId(params: CmsSlideDetailService.PutCmsSlideDetailsIdParams): __Observable<{success?: boolean, data?: Cms_Slide_detail, message?: string}> {
    return this.putCmsSlideDetailsIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Slide_detail, message?: string})
    );
  }

  /**
   * Remove the specified Slide_detail from storage
   *
   * Delete Slide_detail
   * @param id id of Slide_detail
   * @return successful operation
   */
  deleteCmsSlideDetailsIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/slide_details/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Slide_detail from storage
   *
   * Delete Slide_detail
   * @param id id of Slide_detail
   * @return successful operation
   */
  deleteCmsSlideDetailsId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsSlideDetailsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsSlideDetailService {

  /**
   * Parameters for putCmsSlideDetailsId
   */
  export interface PutCmsSlideDetailsIdParams {

    /**
     * id of Slide_detail
     */
    id: string;

    /**
     * Slide_detail that should be updated
     */
    body?: Cms_Slide_detail;
  }
}

export { CmsSlideDetailService }
