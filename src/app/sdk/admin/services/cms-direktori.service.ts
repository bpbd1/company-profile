/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Direktori } from '../models/cms-_direktori';
@Injectable({
  providedIn: 'root',
})
class CmsDirektoriService extends __BaseService {
  static readonly getCmsDirektorisPath = '/cms/direktoris';
  static readonly postCmsDirektorisPath = '/cms/direktoris';
  static readonly getCmsDirektorisIdPath = '/cms/direktoris/{id}';
  static readonly putCmsDirektorisIdPath = '/cms/direktoris/{id}';
  static readonly deleteCmsDirektorisIdPath = '/cms/direktoris/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Direktoris.
   *
   * Get all Direktoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsDirektorisResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Direktori>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/direktoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Direktori>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Direktoris.
   *
   * Get all Direktoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsDirektoris(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Direktori>, message?: string}> {
    return this.getCmsDirektorisResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Direktori>, message?: string})
    );
  }

  /**
   * Store a newly created Direktori in storage
   *
   * Store Direktori
   * @param body Direktori that should be stored
   * @return successful operation
   */
  postCmsDirektorisResponse(body?: Cms_Direktori): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Direktori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/direktoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Direktori, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Direktori in storage
   *
   * Store Direktori
   * @param body Direktori that should be stored
   * @return successful operation
   */
  postCmsDirektoris(body?: Cms_Direktori): __Observable<{success?: boolean, data?: Cms_Direktori, message?: string}> {
    return this.postCmsDirektorisResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Direktori, message?: string})
    );
  }

  /**
   * Display the specified Direktori
   *
   * Get Direktori
   * @param id id of Direktori
   * @return successful operation
   */
  getCmsDirektorisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Direktori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/direktoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Direktori, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Direktori
   *
   * Get Direktori
   * @param id id of Direktori
   * @return successful operation
   */
  getCmsDirektorisId(id: string): __Observable<{success?: boolean, data?: Cms_Direktori, message?: string}> {
    return this.getCmsDirektorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Direktori, message?: string})
    );
  }

  /**
   * Update the specified Direktori in storage
   *
   * Update Direktori
   * @param params The `CmsDirektoriService.PutCmsDirektorisIdParams` containing the following parameters:
   *
   * - `id`: id of Direktori
   *
   * - `body`: Direktori that should be updated
   *
   * @return successful operation
   */
  putCmsDirektorisIdResponse(params: CmsDirektoriService.PutCmsDirektorisIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Direktori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/direktoris/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Direktori, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Direktori in storage
   *
   * Update Direktori
   * @param params The `CmsDirektoriService.PutCmsDirektorisIdParams` containing the following parameters:
   *
   * - `id`: id of Direktori
   *
   * - `body`: Direktori that should be updated
   *
   * @return successful operation
   */
  putCmsDirektorisId(params: CmsDirektoriService.PutCmsDirektorisIdParams): __Observable<{success?: boolean, data?: Cms_Direktori, message?: string}> {
    return this.putCmsDirektorisIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Direktori, message?: string})
    );
  }

  /**
   * Remove the specified Direktori from storage
   *
   * Delete Direktori
   * @param id id of Direktori
   * @return successful operation
   */
  deleteCmsDirektorisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/direktoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Direktori from storage
   *
   * Delete Direktori
   * @param id id of Direktori
   * @return successful operation
   */
  deleteCmsDirektorisId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsDirektorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsDirektoriService {

  /**
   * Parameters for putCmsDirektorisId
   */
  export interface PutCmsDirektorisIdParams {

    /**
     * id of Direktori
     */
    id: string;

    /**
     * Direktori that should be updated
     */
    body?: Cms_Direktori;
  }
}

export { CmsDirektoriService }
