/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Galery } from '../models/cms-_galery';
@Injectable({
  providedIn: 'root',
})
class CmsGaleryService extends __BaseService {
  static readonly getCmsGaleriesPath = '/cms/galeries';
  static readonly postCmsGaleriesPath = '/cms/galeries';
  static readonly getCmsGaleriesIdPath = '/cms/galeries/{id}';
  static readonly putCmsGaleriesIdPath = '/cms/galeries/{id}';
  static readonly deleteCmsGaleriesIdPath = '/cms/galeries/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Galeries.
   *
   * Get all Galeries
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsGaleriesResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Galery>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/galeries`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Galery>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Galeries.
   *
   * Get all Galeries
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsGaleries(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Galery>, message?: string}> {
    return this.getCmsGaleriesResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Galery>, message?: string})
    );
  }

  /**
   * Store a newly created Galery in storage
   *
   * Store Galery
   * @param body Galery that should be stored
   * @return successful operation
   */
  postCmsGaleriesResponse(body?: Cms_Galery): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Galery, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/galeries`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Galery, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Galery in storage
   *
   * Store Galery
   * @param body Galery that should be stored
   * @return successful operation
   */
  postCmsGaleries(body?: Cms_Galery): __Observable<{success?: boolean, data?: Cms_Galery, message?: string}> {
    return this.postCmsGaleriesResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Galery, message?: string})
    );
  }

  /**
   * Display the specified Galery
   *
   * Get Galery
   * @param id id of Galery
   * @return successful operation
   */
  getCmsGaleriesIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Galery, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/galeries/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Galery, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Galery
   *
   * Get Galery
   * @param id id of Galery
   * @return successful operation
   */
  getCmsGaleriesId(id: string): __Observable<{success?: boolean, data?: Cms_Galery, message?: string}> {
    return this.getCmsGaleriesIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Galery, message?: string})
    );
  }

  /**
   * Update the specified Galery in storage
   *
   * Update Galery
   * @param params The `CmsGaleryService.PutCmsGaleriesIdParams` containing the following parameters:
   *
   * - `id`: id of Galery
   *
   * - `body`: Galery that should be updated
   *
   * @return successful operation
   */
  putCmsGaleriesIdResponse(params: CmsGaleryService.PutCmsGaleriesIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Galery, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/galeries/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Galery, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Galery in storage
   *
   * Update Galery
   * @param params The `CmsGaleryService.PutCmsGaleriesIdParams` containing the following parameters:
   *
   * - `id`: id of Galery
   *
   * - `body`: Galery that should be updated
   *
   * @return successful operation
   */
  putCmsGaleriesId(params: CmsGaleryService.PutCmsGaleriesIdParams): __Observable<{success?: boolean, data?: Cms_Galery, message?: string}> {
    return this.putCmsGaleriesIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Galery, message?: string})
    );
  }

  /**
   * Remove the specified Galery from storage
   *
   * Delete Galery
   * @param id id of Galery
   * @return successful operation
   */
  deleteCmsGaleriesIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/galeries/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Galery from storage
   *
   * Delete Galery
   * @param id id of Galery
   * @return successful operation
   */
  deleteCmsGaleriesId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsGaleriesIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsGaleryService {

  /**
   * Parameters for putCmsGaleriesId
   */
  export interface PutCmsGaleriesIdParams {

    /**
     * id of Galery
     */
    id: string;

    /**
     * Galery that should be updated
     */
    body?: Cms_Galery;
  }
}

export { CmsGaleryService }
