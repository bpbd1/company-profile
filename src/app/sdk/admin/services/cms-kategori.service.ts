/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Kategori } from '../models/cms-_kategori';
@Injectable({
  providedIn: 'root',
})
class CmsKategoriService extends __BaseService {
  static readonly getCmsKategorisPath = '/cms/kategoris';
  static readonly postCmsKategorisPath = '/cms/kategoris';
  static readonly getCmsKategorisIdPath = '/cms/kategoris/{id}';
  static readonly putCmsKategorisIdPath = '/cms/kategoris/{id}';
  static readonly deleteCmsKategorisIdPath = '/cms/kategoris/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Kategoris.
   *
   * Get all Kategoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsKategorisResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Kategori>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/kategoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Kategori>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Kategoris.
   *
   * Get all Kategoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsKategoris(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Kategori>, message?: string}> {
    return this.getCmsKategorisResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Kategori>, message?: string})
    );
  }

  /**
   * Store a newly created Kategori in storage
   *
   * Store Kategori
   * @param body Kategori that should be stored
   * @return successful operation
   */
  postCmsKategorisResponse(body?: Cms_Kategori): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/kategoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Kategori, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Kategori in storage
   *
   * Store Kategori
   * @param body Kategori that should be stored
   * @return successful operation
   */
  postCmsKategoris(body?: Cms_Kategori): __Observable<{success?: boolean, data?: Cms_Kategori, message?: string}> {
    return this.postCmsKategorisResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Kategori, message?: string})
    );
  }

  /**
   * Display the specified Kategori
   *
   * Get Kategori
   * @param id id of Kategori
   * @return successful operation
   */
  getCmsKategorisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/kategoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Kategori, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Kategori
   *
   * Get Kategori
   * @param id id of Kategori
   * @return successful operation
   */
  getCmsKategorisId(id: string): __Observable<{success?: boolean, data?: Cms_Kategori, message?: string}> {
    return this.getCmsKategorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Kategori, message?: string})
    );
  }

  /**
   * Update the specified Kategori in storage
   *
   * Update Kategori
   * @param params The `CmsKategoriService.PutCmsKategorisIdParams` containing the following parameters:
   *
   * - `id`: id of Kategori
   *
   * - `body`: Kategori that should be updated
   *
   * @return successful operation
   */
  putCmsKategorisIdResponse(params: CmsKategoriService.PutCmsKategorisIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/kategoris/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Kategori, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Kategori in storage
   *
   * Update Kategori
   * @param params The `CmsKategoriService.PutCmsKategorisIdParams` containing the following parameters:
   *
   * - `id`: id of Kategori
   *
   * - `body`: Kategori that should be updated
   *
   * @return successful operation
   */
  putCmsKategorisId(params: CmsKategoriService.PutCmsKategorisIdParams): __Observable<{success?: boolean, data?: Cms_Kategori, message?: string}> {
    return this.putCmsKategorisIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Kategori, message?: string})
    );
  }

  /**
   * Remove the specified Kategori from storage
   *
   * Delete Kategori
   * @param id id of Kategori
   * @return successful operation
   */
  deleteCmsKategorisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/kategoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Kategori from storage
   *
   * Delete Kategori
   * @param id id of Kategori
   * @return successful operation
   */
  deleteCmsKategorisId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsKategorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsKategoriService {

  /**
   * Parameters for putCmsKategorisId
   */
  export interface PutCmsKategorisIdParams {

    /**
     * id of Kategori
     */
    id: string;

    /**
     * Kategori that should be updated
     */
    body?: Cms_Kategori;
  }
}

export { CmsKategoriService }
