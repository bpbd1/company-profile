/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Peta } from '../models/cms-_peta';
@Injectable({
  providedIn: 'root',
})
class CmsPetaService extends __BaseService {
  static readonly getCmsPetasPath = '/cms/petas';
  static readonly postCmsPetasPath = '/cms/petas';
  static readonly getCmsPetasIdPath = '/cms/petas/{id}';
  static readonly putCmsPetasIdPath = '/cms/petas/{id}';
  static readonly deleteCmsPetasIdPath = '/cms/petas/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Petas.
   *
   * Get all Petas
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsPetasResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Peta>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/petas`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Peta>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Petas.
   *
   * Get all Petas
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsPetas(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Peta>, message?: string}> {
    return this.getCmsPetasResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Peta>, message?: string})
    );
  }

  /**
   * Store a newly created Peta in storage
   *
   * Store Peta
   * @param body Peta that should be stored
   * @return successful operation
   */
  postCmsPetasResponse(body?: Cms_Peta): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Peta, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/petas`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Peta, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Peta in storage
   *
   * Store Peta
   * @param body Peta that should be stored
   * @return successful operation
   */
  postCmsPetas(body?: Cms_Peta): __Observable<{success?: boolean, data?: Cms_Peta, message?: string}> {
    return this.postCmsPetasResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Peta, message?: string})
    );
  }

  /**
   * Display the specified Peta
   *
   * Get Peta
   * @param id id of Peta
   * @return successful operation
   */
  getCmsPetasIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Peta, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/petas/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Peta, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Peta
   *
   * Get Peta
   * @param id id of Peta
   * @return successful operation
   */
  getCmsPetasId(id: string): __Observable<{success?: boolean, data?: Cms_Peta, message?: string}> {
    return this.getCmsPetasIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Peta, message?: string})
    );
  }

  /**
   * Update the specified Peta in storage
   *
   * Update Peta
   * @param params The `CmsPetaService.PutCmsPetasIdParams` containing the following parameters:
   *
   * - `id`: id of Peta
   *
   * - `body`: Peta that should be updated
   *
   * @return successful operation
   */
  putCmsPetasIdResponse(params: CmsPetaService.PutCmsPetasIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Peta, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/petas/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Peta, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Peta in storage
   *
   * Update Peta
   * @param params The `CmsPetaService.PutCmsPetasIdParams` containing the following parameters:
   *
   * - `id`: id of Peta
   *
   * - `body`: Peta that should be updated
   *
   * @return successful operation
   */
  putCmsPetasId(params: CmsPetaService.PutCmsPetasIdParams): __Observable<{success?: boolean, data?: Cms_Peta, message?: string}> {
    return this.putCmsPetasIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Peta, message?: string})
    );
  }

  /**
   * Remove the specified Peta from storage
   *
   * Delete Peta
   * @param id id of Peta
   * @return successful operation
   */
  deleteCmsPetasIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/petas/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Peta from storage
   *
   * Delete Peta
   * @param id id of Peta
   * @return successful operation
   */
  deleteCmsPetasId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsPetasIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsPetaService {

  /**
   * Parameters for putCmsPetasId
   */
  export interface PutCmsPetasIdParams {

    /**
     * id of Peta
     */
    id: string;

    /**
     * Peta that should be updated
     */
    body?: Cms_Peta;
  }
}

export { CmsPetaService }
