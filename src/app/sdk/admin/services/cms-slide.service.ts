/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Slide } from '../models/cms-_slide';
@Injectable({
  providedIn: 'root',
})
class CmsSlideService extends __BaseService {
  static readonly getCmsSlidesPath = '/cms/slides';
  static readonly postCmsSlidesPath = '/cms/slides';
  static readonly getCmsSlidesIdPath = '/cms/slides/{id}';
  static readonly putCmsSlidesIdPath = '/cms/slides/{id}';
  static readonly deleteCmsSlidesIdPath = '/cms/slides/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Slides.
   *
   * Get all Slides
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsSlidesResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Slide>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/slides`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Slide>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Slides.
   *
   * Get all Slides
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsSlides(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Slide>, message?: string}> {
    return this.getCmsSlidesResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Slide>, message?: string})
    );
  }

  /**
   * Store a newly created Slide in storage
   *
   * Store Slide
   * @param body Slide that should be stored
   * @return successful operation
   */
  postCmsSlidesResponse(body?: Cms_Slide): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Slide, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/slides`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Slide, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Slide in storage
   *
   * Store Slide
   * @param body Slide that should be stored
   * @return successful operation
   */
  postCmsSlides(body?: Cms_Slide): __Observable<{success?: boolean, data?: Cms_Slide, message?: string}> {
    return this.postCmsSlidesResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Slide, message?: string})
    );
  }

  /**
   * Display the specified Slide
   *
   * Get Slide
   * @param id id of Slide
   * @return successful operation
   */
  getCmsSlidesIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Slide, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/slides/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Slide, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Slide
   *
   * Get Slide
   * @param id id of Slide
   * @return successful operation
   */
  getCmsSlidesId(id: string): __Observable<{success?: boolean, data?: Cms_Slide, message?: string}> {
    return this.getCmsSlidesIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Slide, message?: string})
    );
  }

  /**
   * Update the specified Slide in storage
   *
   * Update Slide
   * @param params The `CmsSlideService.PutCmsSlidesIdParams` containing the following parameters:
   *
   * - `id`: id of Slide
   *
   * - `body`: Slide that should be updated
   *
   * @return successful operation
   */
  putCmsSlidesIdResponse(params: CmsSlideService.PutCmsSlidesIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Slide, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/slides/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Slide, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Slide in storage
   *
   * Update Slide
   * @param params The `CmsSlideService.PutCmsSlidesIdParams` containing the following parameters:
   *
   * - `id`: id of Slide
   *
   * - `body`: Slide that should be updated
   *
   * @return successful operation
   */
  putCmsSlidesId(params: CmsSlideService.PutCmsSlidesIdParams): __Observable<{success?: boolean, data?: Cms_Slide, message?: string}> {
    return this.putCmsSlidesIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Slide, message?: string})
    );
  }

  /**
   * Remove the specified Slide from storage
   *
   * Delete Slide
   * @param id id of Slide
   * @return successful operation
   */
  deleteCmsSlidesIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/slides/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Slide from storage
   *
   * Delete Slide
   * @param id id of Slide
   * @return successful operation
   */
  deleteCmsSlidesId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsSlidesIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsSlideService {

  /**
   * Parameters for putCmsSlidesId
   */
  export interface PutCmsSlidesIdParams {

    /**
     * id of Slide
     */
    id: string;

    /**
     * Slide that should be updated
     */
    body?: Cms_Slide;
  }
}

export { CmsSlideService }
