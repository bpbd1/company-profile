/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Page } from '../models/cms-_page';
@Injectable({
  providedIn: 'root',
})
class CmsPageService extends __BaseService {
  static readonly getCmsPagesPath = '/cms/pages';
  static readonly postCmsPagesPath = '/cms/pages';
  static readonly getCmsPagesIdPath = '/cms/pages/{id}';
  static readonly putCmsPagesIdPath = '/cms/pages/{id}';
  static readonly deleteCmsPagesIdPath = '/cms/pages/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Pages.
   *
   * Get all Pages
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsPagesResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Page>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/pages`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Page>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Pages.
   *
   * Get all Pages
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsPages(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Page>, message?: string}> {
    return this.getCmsPagesResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Page>, message?: string})
    );
  }

  /**
   * Store a newly created Page in storage
   *
   * Store Page
   * @param body Page that should be stored
   * @return successful operation
   */
  postCmsPagesResponse(body?: Cms_Page): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Page, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/pages`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Page, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Page in storage
   *
   * Store Page
   * @param body Page that should be stored
   * @return successful operation
   */
  postCmsPages(body?: Cms_Page): __Observable<{success?: boolean, data?: Cms_Page, message?: string}> {
    return this.postCmsPagesResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Page, message?: string})
    );
  }

  /**
   * Display the specified Page
   *
   * Get Page
   * @param id id of Page
   * @return successful operation
   */
  getCmsPagesIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Page, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/pages/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Page, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Page
   *
   * Get Page
   * @param id id of Page
   * @return successful operation
   */
  getCmsPagesId(id: string): __Observable<{success?: boolean, data?: Cms_Page, message?: string}> {
    return this.getCmsPagesIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Page, message?: string})
    );
  }

  /**
   * Update the specified Page in storage
   *
   * Update Page
   * @param params The `CmsPageService.PutCmsPagesIdParams` containing the following parameters:
   *
   * - `id`: id of Page
   *
   * - `body`: Page that should be updated
   *
   * @return successful operation
   */
  putCmsPagesIdResponse(params: CmsPageService.PutCmsPagesIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Page, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/pages/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Page, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Page in storage
   *
   * Update Page
   * @param params The `CmsPageService.PutCmsPagesIdParams` containing the following parameters:
   *
   * - `id`: id of Page
   *
   * - `body`: Page that should be updated
   *
   * @return successful operation
   */
  putCmsPagesId(params: CmsPageService.PutCmsPagesIdParams): __Observable<{success?: boolean, data?: Cms_Page, message?: string}> {
    return this.putCmsPagesIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Page, message?: string})
    );
  }

  /**
   * Remove the specified Page from storage
   *
   * Delete Page
   * @param id id of Page
   * @return successful operation
   */
  deleteCmsPagesIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/pages/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Page from storage
   *
   * Delete Page
   * @param id id of Page
   * @return successful operation
   */
  deleteCmsPagesId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsPagesIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsPageService {

  /**
   * Parameters for putCmsPagesId
   */
  export interface PutCmsPagesIdParams {

    /**
     * id of Page
     */
    id: string;

    /**
     * Page that should be updated
     */
    body?: Cms_Page;
  }
}

export { CmsPageService }
