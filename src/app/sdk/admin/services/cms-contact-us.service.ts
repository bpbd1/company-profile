/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Contact_us } from '../models/cms-_contact-_us';
@Injectable({
  providedIn: 'root',
})
class CmsContactUsService extends __BaseService {
  static readonly getCmsContactUsesPath = '/cms/contact_uses';
  static readonly postCmsContactUsesPath = '/cms/contact_uses';
  static readonly getCmsContactUsesIdPath = '/cms/contact_uses/{id}';
  static readonly putCmsContactUsesIdPath = '/cms/contact_uses/{id}';
  static readonly deleteCmsContactUsesIdPath = '/cms/contact_uses/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Contact_uses.
   *
   * Get all Contact_uses
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsContactUsesResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Contact_us>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/contact_uses`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Contact_us>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Contact_uses.
   *
   * Get all Contact_uses
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsContactUses(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Contact_us>, message?: string}> {
    return this.getCmsContactUsesResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Contact_us>, message?: string})
    );
  }

  /**
   * Store a newly created Contact_us in storage
   *
   * Store Contact_us
   * @param body Contact_us that should be stored
   * @return successful operation
   */
  postCmsContactUsesResponse(body?: Cms_Contact_us): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Contact_us, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/contact_uses`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Contact_us, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Contact_us in storage
   *
   * Store Contact_us
   * @param body Contact_us that should be stored
   * @return successful operation
   */
  postCmsContactUses(body?: Cms_Contact_us): __Observable<{success?: boolean, data?: Cms_Contact_us, message?: string}> {
    return this.postCmsContactUsesResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Contact_us, message?: string})
    );
  }

  /**
   * Display the specified Contact_us
   *
   * Get Contact_us
   * @param id id of Contact_us
   * @return successful operation
   */
  getCmsContactUsesIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Contact_us, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/contact_uses/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Contact_us, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Contact_us
   *
   * Get Contact_us
   * @param id id of Contact_us
   * @return successful operation
   */
  getCmsContactUsesId(id: string): __Observable<{success?: boolean, data?: Cms_Contact_us, message?: string}> {
    return this.getCmsContactUsesIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Contact_us, message?: string})
    );
  }

  /**
   * Update the specified Contact_us in storage
   *
   * Update Contact_us
   * @param params The `CmsContactUsService.PutCmsContactUsesIdParams` containing the following parameters:
   *
   * - `id`: id of Contact_us
   *
   * - `body`: Contact_us that should be updated
   *
   * @return successful operation
   */
  putCmsContactUsesIdResponse(params: CmsContactUsService.PutCmsContactUsesIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Contact_us, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/contact_uses/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Contact_us, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Contact_us in storage
   *
   * Update Contact_us
   * @param params The `CmsContactUsService.PutCmsContactUsesIdParams` containing the following parameters:
   *
   * - `id`: id of Contact_us
   *
   * - `body`: Contact_us that should be updated
   *
   * @return successful operation
   */
  putCmsContactUsesId(params: CmsContactUsService.PutCmsContactUsesIdParams): __Observable<{success?: boolean, data?: Cms_Contact_us, message?: string}> {
    return this.putCmsContactUsesIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Contact_us, message?: string})
    );
  }

  /**
   * Remove the specified Contact_us from storage
   *
   * Delete Contact_us
   * @param id id of Contact_us
   * @return successful operation
   */
  deleteCmsContactUsesIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/contact_uses/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Contact_us from storage
   *
   * Delete Contact_us
   * @param id id of Contact_us
   * @return successful operation
   */
  deleteCmsContactUsesId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsContactUsesIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsContactUsService {

  /**
   * Parameters for putCmsContactUsesId
   */
  export interface PutCmsContactUsesIdParams {

    /**
     * id of Contact_us
     */
    id: string;

    /**
     * Contact_us that should be updated
     */
    body?: Cms_Contact_us;
  }
}

export { CmsContactUsService }
