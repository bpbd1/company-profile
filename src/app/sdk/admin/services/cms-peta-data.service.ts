/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Peta_data } from '../models/cms-_peta-_data';
@Injectable({
  providedIn: 'root',
})
class CmsPetaDataService extends __BaseService {
  static readonly getCmsPetaDatasPath = '/cms/peta_datas';
  static readonly postCmsPetaDatasPath = '/cms/peta_datas';
  static readonly getCmsPetaDatasIdPath = '/cms/peta_datas/{id}';
  static readonly putCmsPetaDatasIdPath = '/cms/peta_datas/{id}';
  static readonly deleteCmsPetaDatasIdPath = '/cms/peta_datas/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Peta_datas.
   *
   * Get all Peta_datas
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsPetaDatasResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Peta_data>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/peta_datas`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Peta_data>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Peta_datas.
   *
   * Get all Peta_datas
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsPetaDatas(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Peta_data>, message?: string}> {
    return this.getCmsPetaDatasResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Peta_data>, message?: string})
    );
  }

  /**
   * Store a newly created Peta_data in storage
   *
   * Store Peta_data
   * @param body Peta_data that should be stored
   * @return successful operation
   */
  postCmsPetaDatasResponse(body?: Cms_Peta_data): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Peta_data, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/peta_datas`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Peta_data, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Peta_data in storage
   *
   * Store Peta_data
   * @param body Peta_data that should be stored
   * @return successful operation
   */
  postCmsPetaDatas(body?: Cms_Peta_data): __Observable<{success?: boolean, data?: Cms_Peta_data, message?: string}> {
    return this.postCmsPetaDatasResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Peta_data, message?: string})
    );
  }

  /**
   * Display the specified Peta_data
   *
   * Get Peta_data
   * @param id id of Peta_data
   * @return successful operation
   */
  getCmsPetaDatasIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Peta_data, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/peta_datas/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Peta_data, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Peta_data
   *
   * Get Peta_data
   * @param id id of Peta_data
   * @return successful operation
   */
  getCmsPetaDatasId(id: string): __Observable<{success?: boolean, data?: Cms_Peta_data, message?: string}> {
    return this.getCmsPetaDatasIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Peta_data, message?: string})
    );
  }

  /**
   * Update the specified Peta_data in storage
   *
   * Update Peta_data
   * @param params The `CmsPetaDataService.PutCmsPetaDatasIdParams` containing the following parameters:
   *
   * - `id`: id of Peta_data
   *
   * - `body`: Peta_data that should be updated
   *
   * @return successful operation
   */
  putCmsPetaDatasIdResponse(params: CmsPetaDataService.PutCmsPetaDatasIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Peta_data, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/peta_datas/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Peta_data, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Peta_data in storage
   *
   * Update Peta_data
   * @param params The `CmsPetaDataService.PutCmsPetaDatasIdParams` containing the following parameters:
   *
   * - `id`: id of Peta_data
   *
   * - `body`: Peta_data that should be updated
   *
   * @return successful operation
   */
  putCmsPetaDatasId(params: CmsPetaDataService.PutCmsPetaDatasIdParams): __Observable<{success?: boolean, data?: Cms_Peta_data, message?: string}> {
    return this.putCmsPetaDatasIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Peta_data, message?: string})
    );
  }

  /**
   * Remove the specified Peta_data from storage
   *
   * Delete Peta_data
   * @param id id of Peta_data
   * @return successful operation
   */
  deleteCmsPetaDatasIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/peta_datas/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Peta_data from storage
   *
   * Delete Peta_data
   * @param id id of Peta_data
   * @return successful operation
   */
  deleteCmsPetaDatasId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsPetaDatasIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsPetaDataService {

  /**
   * Parameters for putCmsPetaDatasId
   */
  export interface PutCmsPetaDatasIdParams {

    /**
     * id of Peta_data
     */
    id: string;

    /**
     * Peta_data that should be updated
     */
    body?: Cms_Peta_data;
  }
}

export { CmsPetaDataService }
