/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Peta_kategori } from '../models/cms-_peta-_kategori';
@Injectable({
  providedIn: 'root',
})
class CmsPetaKategoriService extends __BaseService {
  static readonly getCmsPetaKategorisPath = '/cms/peta_kategoris';
  static readonly postCmsPetaKategorisPath = '/cms/peta_kategoris';
  static readonly getCmsPetaKategorisIdPath = '/cms/peta_kategoris/{id}';
  static readonly putCmsPetaKategorisIdPath = '/cms/peta_kategoris/{id}';
  static readonly deleteCmsPetaKategorisIdPath = '/cms/peta_kategoris/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Peta_kategoris.
   *
   * Get all Peta_kategoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsPetaKategorisResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Peta_kategori>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/peta_kategoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Peta_kategori>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Peta_kategoris.
   *
   * Get all Peta_kategoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsPetaKategoris(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Peta_kategori>, message?: string}> {
    return this.getCmsPetaKategorisResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Peta_kategori>, message?: string})
    );
  }

  /**
   * Store a newly created Peta_kategori in storage
   *
   * Store Peta_kategori
   * @param body Peta_kategori that should be stored
   * @return successful operation
   */
  postCmsPetaKategorisResponse(body?: Cms_Peta_kategori): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Peta_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/peta_kategoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Peta_kategori, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Peta_kategori in storage
   *
   * Store Peta_kategori
   * @param body Peta_kategori that should be stored
   * @return successful operation
   */
  postCmsPetaKategoris(body?: Cms_Peta_kategori): __Observable<{success?: boolean, data?: Cms_Peta_kategori, message?: string}> {
    return this.postCmsPetaKategorisResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Peta_kategori, message?: string})
    );
  }

  /**
   * Display the specified Peta_kategori
   *
   * Get Peta_kategori
   * @param id id of Peta_kategori
   * @return successful operation
   */
  getCmsPetaKategorisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Peta_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/peta_kategoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Peta_kategori, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Peta_kategori
   *
   * Get Peta_kategori
   * @param id id of Peta_kategori
   * @return successful operation
   */
  getCmsPetaKategorisId(id: string): __Observable<{success?: boolean, data?: Cms_Peta_kategori, message?: string}> {
    return this.getCmsPetaKategorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Peta_kategori, message?: string})
    );
  }

  /**
   * Update the specified Peta_kategori in storage
   *
   * Update Peta_kategori
   * @param params The `CmsPetaKategoriService.PutCmsPetaKategorisIdParams` containing the following parameters:
   *
   * - `id`: id of Peta_kategori
   *
   * - `body`: Peta_kategori that should be updated
   *
   * @return successful operation
   */
  putCmsPetaKategorisIdResponse(params: CmsPetaKategoriService.PutCmsPetaKategorisIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Peta_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/peta_kategoris/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Peta_kategori, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Peta_kategori in storage
   *
   * Update Peta_kategori
   * @param params The `CmsPetaKategoriService.PutCmsPetaKategorisIdParams` containing the following parameters:
   *
   * - `id`: id of Peta_kategori
   *
   * - `body`: Peta_kategori that should be updated
   *
   * @return successful operation
   */
  putCmsPetaKategorisId(params: CmsPetaKategoriService.PutCmsPetaKategorisIdParams): __Observable<{success?: boolean, data?: Cms_Peta_kategori, message?: string}> {
    return this.putCmsPetaKategorisIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Peta_kategori, message?: string})
    );
  }

  /**
   * Remove the specified Peta_kategori from storage
   *
   * Delete Peta_kategori
   * @param id id of Peta_kategori
   * @return successful operation
   */
  deleteCmsPetaKategorisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/peta_kategoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Peta_kategori from storage
   *
   * Delete Peta_kategori
   * @param id id of Peta_kategori
   * @return successful operation
   */
  deleteCmsPetaKategorisId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsPetaKategorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsPetaKategoriService {

  /**
   * Parameters for putCmsPetaKategorisId
   */
  export interface PutCmsPetaKategorisIdParams {

    /**
     * id of Peta_kategori
     */
    id: string;

    /**
     * Peta_kategori that should be updated
     */
    body?: Cms_Peta_kategori;
  }
}

export { CmsPetaKategoriService }
