/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_spot_kategori } from '../models/cms-_spot-_kategori';
@Injectable({
  providedIn: 'root',
})
class CmsSpotKategoriService extends __BaseService {
  static readonly getCmsSpotKategorisPath = '/cms/spot_kategoris';
  static readonly postCmsSpotKategorisPath = '/cms/spot_kategoris';
  static readonly getCmsSpotKategorisIdPath = '/cms/spot_kategoris/{id}';
  static readonly putCmsSpotKategorisIdPath = '/cms/spot_kategoris/{id}';
  static readonly deleteCmsSpotKategorisIdPath = '/cms/spot_kategoris/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the spot_kategoris.
   *
   * Get all spot_kategoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsSpotKategorisResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_spot_kategori>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/spot_kategoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_spot_kategori>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the spot_kategoris.
   *
   * Get all spot_kategoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsSpotKategoris(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_spot_kategori>, message?: string}> {
    return this.getCmsSpotKategorisResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_spot_kategori>, message?: string})
    );
  }

  /**
   * Store a newly created spot_kategori in storage
   *
   * Store spot_kategori
   * @param body spot_kategori that should be stored
   * @return successful operation
   */
  postCmsSpotKategorisResponse(body?: Cms_spot_kategori): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_spot_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/spot_kategoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_spot_kategori, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created spot_kategori in storage
   *
   * Store spot_kategori
   * @param body spot_kategori that should be stored
   * @return successful operation
   */
  postCmsSpotKategoris(body?: Cms_spot_kategori): __Observable<{success?: boolean, data?: Cms_spot_kategori, message?: string}> {
    return this.postCmsSpotKategorisResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_spot_kategori, message?: string})
    );
  }

  /**
   * Display the specified spot_kategori
   *
   * Get spot_kategori
   * @param id id of spot_kategori
   * @return successful operation
   */
  getCmsSpotKategorisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_spot_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/spot_kategoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_spot_kategori, message?: string}>;
      })
    );
  }
  /**
   * Display the specified spot_kategori
   *
   * Get spot_kategori
   * @param id id of spot_kategori
   * @return successful operation
   */
  getCmsSpotKategorisId(id: string): __Observable<{success?: boolean, data?: Cms_spot_kategori, message?: string}> {
    return this.getCmsSpotKategorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_spot_kategori, message?: string})
    );
  }

  /**
   * Update the specified spot_kategori in storage
   *
   * Update spot_kategori
   * @param params The `CmsSpotKategoriService.PutCmsSpotKategorisIdParams` containing the following parameters:
   *
   * - `id`: id of spot_kategori
   *
   * - `body`: spot_kategori that should be updated
   *
   * @return successful operation
   */
  putCmsSpotKategorisIdResponse(params: CmsSpotKategoriService.PutCmsSpotKategorisIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_spot_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/spot_kategoris/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_spot_kategori, message?: string}>;
      })
    );
  }
  /**
   * Update the specified spot_kategori in storage
   *
   * Update spot_kategori
   * @param params The `CmsSpotKategoriService.PutCmsSpotKategorisIdParams` containing the following parameters:
   *
   * - `id`: id of spot_kategori
   *
   * - `body`: spot_kategori that should be updated
   *
   * @return successful operation
   */
  putCmsSpotKategorisId(params: CmsSpotKategoriService.PutCmsSpotKategorisIdParams): __Observable<{success?: boolean, data?: Cms_spot_kategori, message?: string}> {
    return this.putCmsSpotKategorisIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_spot_kategori, message?: string})
    );
  }

  /**
   * Remove the specified spot_kategori from storage
   *
   * Delete spot_kategori
   * @param id id of spot_kategori
   * @return successful operation
   */
  deleteCmsSpotKategorisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/spot_kategoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified spot_kategori from storage
   *
   * Delete spot_kategori
   * @param id id of spot_kategori
   * @return successful operation
   */
  deleteCmsSpotKategorisId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsSpotKategorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsSpotKategoriService {

  /**
   * Parameters for putCmsSpotKategorisId
   */
  export interface PutCmsSpotKategorisIdParams {

    /**
     * id of spot_kategori
     */
    id: string;

    /**
     * spot_kategori that should be updated
     */
    body?: Cms_spot_kategori;
  }
}

export { CmsSpotKategoriService }
