/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Media } from '../models/cms-_media';
@Injectable({
  providedIn: 'root',
})
class CmsMediaService extends __BaseService {
  static readonly getCmsMediaPath = '/cms/media';
  static readonly postCmsMediaPath = '/cms/media';
  static readonly getCmsMediaIdPath = '/cms/media/{id}';
  static readonly putCmsMediaIdPath = '/cms/media/{id}';
  static readonly deleteCmsMediaIdPath = '/cms/media/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Media.
   *
   * Get all Media
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsMediaResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Media>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/media`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Media>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Media.
   *
   * Get all Media
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsMedia(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Media>, message?: string}> {
    return this.getCmsMediaResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Media>, message?: string})
    );
  }

  /**
   * Store a newly created Media in storage
   *
   * Store Media
   * @param body Media that should be stored
   * @return successful operation
   */
  postCmsMediaResponse(body?: Cms_Media): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Media, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/media`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Media, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Media in storage
   *
   * Store Media
   * @param body Media that should be stored
   * @return successful operation
   */
  postCmsMedia(body?: Cms_Media): __Observable<{success?: boolean, data?: Cms_Media, message?: string}> {
    return this.postCmsMediaResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Media, message?: string})
    );
  }

  /**
   * Display the specified Media
   *
   * Get Media
   * @param id id of Media
   * @return successful operation
   */
  getCmsMediaIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Media, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/media/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Media, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Media
   *
   * Get Media
   * @param id id of Media
   * @return successful operation
   */
  getCmsMediaId(id: string): __Observable<{success?: boolean, data?: Cms_Media, message?: string}> {
    return this.getCmsMediaIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Media, message?: string})
    );
  }

  /**
   * Update the specified Media in storage
   *
   * Update Media
   * @param params The `CmsMediaService.PutCmsMediaIdParams` containing the following parameters:
   *
   * - `id`: id of Media
   *
   * - `body`: Media that should be updated
   *
   * @return successful operation
   */
  putCmsMediaIdResponse(params: CmsMediaService.PutCmsMediaIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Media, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/media/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Media, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Media in storage
   *
   * Update Media
   * @param params The `CmsMediaService.PutCmsMediaIdParams` containing the following parameters:
   *
   * - `id`: id of Media
   *
   * - `body`: Media that should be updated
   *
   * @return successful operation
   */
  putCmsMediaId(params: CmsMediaService.PutCmsMediaIdParams): __Observable<{success?: boolean, data?: Cms_Media, message?: string}> {
    return this.putCmsMediaIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Media, message?: string})
    );
  }

  /**
   * Remove the specified Media from storage
   *
   * Delete Media
   * @param id id of Media
   * @return successful operation
   */
  deleteCmsMediaIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/media/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Media from storage
   *
   * Delete Media
   * @param id id of Media
   * @return successful operation
   */
  deleteCmsMediaId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsMediaIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsMediaService {

  /**
   * Parameters for putCmsMediaId
   */
  export interface PutCmsMediaIdParams {

    /**
     * id of Media
     */
    id: string;

    /**
     * Media that should be updated
     */
    body?: Cms_Media;
  }
}

export { CmsMediaService }
