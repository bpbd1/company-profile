/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_bencana_kategori } from '../models/cms-_bencana-_kategori';
@Injectable({
  providedIn: 'root',
})
class CmsBencanaKategoriService extends __BaseService {
  static readonly getCmsBencanaKategorisPath = '/cms/bencana_kategoris';
  static readonly postCmsBencanaKategorisPath = '/cms/bencana_kategoris';
  static readonly getCmsBencanaKategorisIdPath = '/cms/bencana_kategoris/{id}';
  static readonly putCmsBencanaKategorisIdPath = '/cms/bencana_kategoris/{id}';
  static readonly deleteCmsBencanaKategorisIdPath = '/cms/bencana_kategoris/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the bencana_kategoris.
   *
   * Get all bencana_kategoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsBencanaKategorisResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_bencana_kategori>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/bencana_kategoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_bencana_kategori>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the bencana_kategoris.
   *
   * Get all bencana_kategoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsBencanaKategoris(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_bencana_kategori>, message?: string}> {
    return this.getCmsBencanaKategorisResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_bencana_kategori>, message?: string})
    );
  }

  /**
   * Store a newly created bencana_kategori in storage
   *
   * Store bencana_kategori
   * @param body bencana_kategori that should be stored
   * @return successful operation
   */
  postCmsBencanaKategorisResponse(body?: Cms_bencana_kategori): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_bencana_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/bencana_kategoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_bencana_kategori, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created bencana_kategori in storage
   *
   * Store bencana_kategori
   * @param body bencana_kategori that should be stored
   * @return successful operation
   */
  postCmsBencanaKategoris(body?: Cms_bencana_kategori): __Observable<{success?: boolean, data?: Cms_bencana_kategori, message?: string}> {
    return this.postCmsBencanaKategorisResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_bencana_kategori, message?: string})
    );
  }

  /**
   * Display the specified bencana_kategori
   *
   * Get bencana_kategori
   * @param id id of bencana_kategori
   * @return successful operation
   */
  getCmsBencanaKategorisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_bencana_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/bencana_kategoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_bencana_kategori, message?: string}>;
      })
    );
  }
  /**
   * Display the specified bencana_kategori
   *
   * Get bencana_kategori
   * @param id id of bencana_kategori
   * @return successful operation
   */
  getCmsBencanaKategorisId(id: string): __Observable<{success?: boolean, data?: Cms_bencana_kategori, message?: string}> {
    return this.getCmsBencanaKategorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_bencana_kategori, message?: string})
    );
  }

  /**
   * Update the specified bencana_kategori in storage
   *
   * Update bencana_kategori
   * @param params The `CmsBencanaKategoriService.PutCmsBencanaKategorisIdParams` containing the following parameters:
   *
   * - `id`: id of bencana_kategori
   *
   * - `body`: bencana_kategori that should be updated
   *
   * @return successful operation
   */
  putCmsBencanaKategorisIdResponse(params: CmsBencanaKategoriService.PutCmsBencanaKategorisIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_bencana_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/bencana_kategoris/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_bencana_kategori, message?: string}>;
      })
    );
  }
  /**
   * Update the specified bencana_kategori in storage
   *
   * Update bencana_kategori
   * @param params The `CmsBencanaKategoriService.PutCmsBencanaKategorisIdParams` containing the following parameters:
   *
   * - `id`: id of bencana_kategori
   *
   * - `body`: bencana_kategori that should be updated
   *
   * @return successful operation
   */
  putCmsBencanaKategorisId(params: CmsBencanaKategoriService.PutCmsBencanaKategorisIdParams): __Observable<{success?: boolean, data?: Cms_bencana_kategori, message?: string}> {
    return this.putCmsBencanaKategorisIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_bencana_kategori, message?: string})
    );
  }

  /**
   * Remove the specified bencana_kategori from storage
   *
   * Delete bencana_kategori
   * @param id id of bencana_kategori
   * @return successful operation
   */
  deleteCmsBencanaKategorisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/bencana_kategoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified bencana_kategori from storage
   *
   * Delete bencana_kategori
   * @param id id of bencana_kategori
   * @return successful operation
   */
  deleteCmsBencanaKategorisId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsBencanaKategorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsBencanaKategoriService {

  /**
   * Parameters for putCmsBencanaKategorisId
   */
  export interface PutCmsBencanaKategorisIdParams {

    /**
     * id of bencana_kategori
     */
    id: string;

    /**
     * bencana_kategori that should be updated
     */
    body?: Cms_bencana_kategori;
  }
}

export { CmsBencanaKategoriService }
