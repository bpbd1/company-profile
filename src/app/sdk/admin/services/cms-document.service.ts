/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Document } from '../models/cms-_document';
@Injectable({
  providedIn: 'root',
})
class CmsDocumentService extends __BaseService {
  static readonly getCmsDocumentsPath = '/cms/documents';
  static readonly postCmsDocumentsPath = '/cms/documents';
  static readonly getCmsDocumentsIdPath = '/cms/documents/{id}';
  static readonly putCmsDocumentsIdPath = '/cms/documents/{id}';
  static readonly deleteCmsDocumentsIdPath = '/cms/documents/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Documents.
   *
   * Get all Documents
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsDocumentsResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Document>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/documents`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Document>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Documents.
   *
   * Get all Documents
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsDocuments(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Document>, message?: string}> {
    return this.getCmsDocumentsResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Document>, message?: string})
    );
  }

  /**
   * Store a newly created Document in storage
   *
   * Store Document
   * @param body Document that should be stored
   * @return successful operation
   */
  postCmsDocumentsResponse(body?: Cms_Document): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Document, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/documents`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Document, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Document in storage
   *
   * Store Document
   * @param body Document that should be stored
   * @return successful operation
   */
  postCmsDocuments(body?: Cms_Document): __Observable<{success?: boolean, data?: Cms_Document, message?: string}> {
    return this.postCmsDocumentsResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Document, message?: string})
    );
  }

  /**
   * Display the specified Document
   *
   * Get Document
   * @param id id of Document
   * @return successful operation
   */
  getCmsDocumentsIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Document, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/documents/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Document, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Document
   *
   * Get Document
   * @param id id of Document
   * @return successful operation
   */
  getCmsDocumentsId(id: string): __Observable<{success?: boolean, data?: Cms_Document, message?: string}> {
    return this.getCmsDocumentsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Document, message?: string})
    );
  }

  /**
   * Update the specified Document in storage
   *
   * Update Document
   * @param params The `CmsDocumentService.PutCmsDocumentsIdParams` containing the following parameters:
   *
   * - `id`: id of Document
   *
   * - `body`: Document that should be updated
   *
   * @return successful operation
   */
  putCmsDocumentsIdResponse(params: CmsDocumentService.PutCmsDocumentsIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Document, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/documents/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Document, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Document in storage
   *
   * Update Document
   * @param params The `CmsDocumentService.PutCmsDocumentsIdParams` containing the following parameters:
   *
   * - `id`: id of Document
   *
   * - `body`: Document that should be updated
   *
   * @return successful operation
   */
  putCmsDocumentsId(params: CmsDocumentService.PutCmsDocumentsIdParams): __Observable<{success?: boolean, data?: Cms_Document, message?: string}> {
    return this.putCmsDocumentsIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Document, message?: string})
    );
  }

  /**
   * Remove the specified Document from storage
   *
   * Delete Document
   * @param id id of Document
   * @return successful operation
   */
  deleteCmsDocumentsIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/documents/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Document from storage
   *
   * Delete Document
   * @param id id of Document
   * @return successful operation
   */
  deleteCmsDocumentsId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsDocumentsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsDocumentService {

  /**
   * Parameters for putCmsDocumentsId
   */
  export interface PutCmsDocumentsIdParams {

    /**
     * id of Document
     */
    id: string;

    /**
     * Document that should be updated
     */
    body?: Cms_Document;
  }
}

export { CmsDocumentService }
