/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Administrasi_Kecamatan } from '../models/administrasi-_kecamatan';
@Injectable({
  providedIn: 'root',
})
class AdministrasiKecamatanService extends __BaseService {
  static readonly getAdministrasiKecamatansPath = '/administrasi/kecamatans';
  static readonly postAdministrasiKecamatansPath = '/administrasi/kecamatans';
  static readonly getAdministrasiKecamatansIdPath = '/administrasi/kecamatans/{id}';
  static readonly putAdministrasiKecamatansIdPath = '/administrasi/kecamatans/{id}';
  static readonly deleteAdministrasiKecamatansIdPath = '/administrasi/kecamatans/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Kecamatans.
   *
   * Get all Kecamatans
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getAdministrasiKecamatansResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Administrasi_Kecamatan>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/administrasi/kecamatans`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Administrasi_Kecamatan>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Kecamatans.
   *
   * Get all Kecamatans
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getAdministrasiKecamatans(filter?: string): __Observable<{success?: boolean, data?: Array<Administrasi_Kecamatan>, message?: string}> {
    return this.getAdministrasiKecamatansResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Administrasi_Kecamatan>, message?: string})
    );
  }

  /**
   * Store a newly created Kecamatan in storage
   *
   * Store Kecamatan
   * @param body Kecamatan that should be stored
   * @return successful operation
   */
  postAdministrasiKecamatansResponse(body?: Administrasi_Kecamatan): __Observable<__StrictHttpResponse<{success?: boolean, data?: Administrasi_Kecamatan, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/administrasi/kecamatans`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Administrasi_Kecamatan, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Kecamatan in storage
   *
   * Store Kecamatan
   * @param body Kecamatan that should be stored
   * @return successful operation
   */
  postAdministrasiKecamatans(body?: Administrasi_Kecamatan): __Observable<{success?: boolean, data?: Administrasi_Kecamatan, message?: string}> {
    return this.postAdministrasiKecamatansResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Administrasi_Kecamatan, message?: string})
    );
  }

  /**
   * Display the specified Kecamatan
   *
   * Get Kecamatan
   * @param id id of Kecamatan
   * @return successful operation
   */
  getAdministrasiKecamatansIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Administrasi_Kecamatan, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/administrasi/kecamatans/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Administrasi_Kecamatan, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Kecamatan
   *
   * Get Kecamatan
   * @param id id of Kecamatan
   * @return successful operation
   */
  getAdministrasiKecamatansId(id: string): __Observable<{success?: boolean, data?: Administrasi_Kecamatan, message?: string}> {
    return this.getAdministrasiKecamatansIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Administrasi_Kecamatan, message?: string})
    );
  }

  /**
   * Update the specified Kecamatan in storage
   *
   * Update Kecamatan
   * @param params The `AdministrasiKecamatanService.PutAdministrasiKecamatansIdParams` containing the following parameters:
   *
   * - `id`: id of Kecamatan
   *
   * - `body`: Kecamatan that should be updated
   *
   * @return successful operation
   */
  putAdministrasiKecamatansIdResponse(params: AdministrasiKecamatanService.PutAdministrasiKecamatansIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Administrasi_Kecamatan, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/administrasi/kecamatans/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Administrasi_Kecamatan, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Kecamatan in storage
   *
   * Update Kecamatan
   * @param params The `AdministrasiKecamatanService.PutAdministrasiKecamatansIdParams` containing the following parameters:
   *
   * - `id`: id of Kecamatan
   *
   * - `body`: Kecamatan that should be updated
   *
   * @return successful operation
   */
  putAdministrasiKecamatansId(params: AdministrasiKecamatanService.PutAdministrasiKecamatansIdParams): __Observable<{success?: boolean, data?: Administrasi_Kecamatan, message?: string}> {
    return this.putAdministrasiKecamatansIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Administrasi_Kecamatan, message?: string})
    );
  }

  /**
   * Remove the specified Kecamatan from storage
   *
   * Delete Kecamatan
   * @param id id of Kecamatan
   * @return successful operation
   */
  deleteAdministrasiKecamatansIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/administrasi/kecamatans/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Kecamatan from storage
   *
   * Delete Kecamatan
   * @param id id of Kecamatan
   * @return successful operation
   */
  deleteAdministrasiKecamatansId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteAdministrasiKecamatansIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module AdministrasiKecamatanService {

  /**
   * Parameters for putAdministrasiKecamatansId
   */
  export interface PutAdministrasiKecamatansIdParams {

    /**
     * id of Kecamatan
     */
    id: string;

    /**
     * Kecamatan that should be updated
     */
    body?: Administrasi_Kecamatan;
  }
}

export { AdministrasiKecamatanService }
