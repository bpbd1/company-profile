/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Struktur_organisasii } from '../models/cms-_struktur-_organisasii';
@Injectable({
  providedIn: 'root',
})
class CmsStrukturOrganisasiiService extends __BaseService {
  static readonly getCmsStrukturOrganisasiisPath = '/cms/struktur_organisasiis';
  static readonly postCmsStrukturOrganisasiisPath = '/cms/struktur_organisasiis';
  static readonly getCmsStrukturOrganisasiisIdPath = '/cms/struktur_organisasiis/{id}';
  static readonly putCmsStrukturOrganisasiisIdPath = '/cms/struktur_organisasiis/{id}';
  static readonly deleteCmsStrukturOrganisasiisIdPath = '/cms/struktur_organisasiis/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Struktur_organisasiis.
   *
   * Get all Struktur_organisasiis
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsStrukturOrganisasiisResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Struktur_organisasii>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/struktur_organisasiis`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Struktur_organisasii>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Struktur_organisasiis.
   *
   * Get all Struktur_organisasiis
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsStrukturOrganisasiis(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Struktur_organisasii>, message?: string}> {
    return this.getCmsStrukturOrganisasiisResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Struktur_organisasii>, message?: string})
    );
  }

  /**
   * Store a newly created Struktur_organisasii in storage
   *
   * Store Struktur_organisasii
   * @param body Struktur_organisasii that should be stored
   * @return successful operation
   */
  postCmsStrukturOrganisasiisResponse(body?: Cms_Struktur_organisasii): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Struktur_organisasii, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/struktur_organisasiis`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Struktur_organisasii, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Struktur_organisasii in storage
   *
   * Store Struktur_organisasii
   * @param body Struktur_organisasii that should be stored
   * @return successful operation
   */
  postCmsStrukturOrganisasiis(body?: Cms_Struktur_organisasii): __Observable<{success?: boolean, data?: Cms_Struktur_organisasii, message?: string}> {
    return this.postCmsStrukturOrganisasiisResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Struktur_organisasii, message?: string})
    );
  }

  /**
   * Display the specified Struktur_organisasii
   *
   * Get Struktur_organisasii
   * @param id id of Struktur_organisasii
   * @return successful operation
   */
  getCmsStrukturOrganisasiisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Struktur_organisasii, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/struktur_organisasiis/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Struktur_organisasii, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Struktur_organisasii
   *
   * Get Struktur_organisasii
   * @param id id of Struktur_organisasii
   * @return successful operation
   */
  getCmsStrukturOrganisasiisId(id: string): __Observable<{success?: boolean, data?: Cms_Struktur_organisasii, message?: string}> {
    return this.getCmsStrukturOrganisasiisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Struktur_organisasii, message?: string})
    );
  }

  /**
   * Update the specified Struktur_organisasii in storage
   *
   * Update Struktur_organisasii
   * @param params The `CmsStrukturOrganisasiiService.PutCmsStrukturOrganisasiisIdParams` containing the following parameters:
   *
   * - `id`: id of Struktur_organisasii
   *
   * - `body`: Struktur_organisasii that should be updated
   *
   * @return successful operation
   */
  putCmsStrukturOrganisasiisIdResponse(params: CmsStrukturOrganisasiiService.PutCmsStrukturOrganisasiisIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Struktur_organisasii, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/struktur_organisasiis/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Struktur_organisasii, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Struktur_organisasii in storage
   *
   * Update Struktur_organisasii
   * @param params The `CmsStrukturOrganisasiiService.PutCmsStrukturOrganisasiisIdParams` containing the following parameters:
   *
   * - `id`: id of Struktur_organisasii
   *
   * - `body`: Struktur_organisasii that should be updated
   *
   * @return successful operation
   */
  putCmsStrukturOrganisasiisId(params: CmsStrukturOrganisasiiService.PutCmsStrukturOrganisasiisIdParams): __Observable<{success?: boolean, data?: Cms_Struktur_organisasii, message?: string}> {
    return this.putCmsStrukturOrganisasiisIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Struktur_organisasii, message?: string})
    );
  }

  /**
   * Remove the specified Struktur_organisasii from storage
   *
   * Delete Struktur_organisasii
   * @param id id of Struktur_organisasii
   * @return successful operation
   */
  deleteCmsStrukturOrganisasiisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/struktur_organisasiis/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Struktur_organisasii from storage
   *
   * Delete Struktur_organisasii
   * @param id id of Struktur_organisasii
   * @return successful operation
   */
  deleteCmsStrukturOrganisasiisId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsStrukturOrganisasiisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsStrukturOrganisasiiService {

  /**
   * Parameters for putCmsStrukturOrganisasiisId
   */
  export interface PutCmsStrukturOrganisasiisIdParams {

    /**
     * id of Struktur_organisasii
     */
    id: string;

    /**
     * Struktur_organisasii that should be updated
     */
    body?: Cms_Struktur_organisasii;
  }
}

export { CmsStrukturOrganisasiiService }
