/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Post_type } from '../models/cms-_post-_type';
@Injectable({
  providedIn: 'root',
})
class CmsPostTypeService extends __BaseService {
  static readonly getCmsPostTypesPath = '/cms/post_types';
  static readonly postCmsPostTypesPath = '/cms/post_types';
  static readonly getCmsPostTypesIdPath = '/cms/post_types/{id}';
  static readonly putCmsPostTypesIdPath = '/cms/post_types/{id}';
  static readonly deleteCmsPostTypesIdPath = '/cms/post_types/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Post_types.
   *
   * Get all Post_types
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsPostTypesResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Post_type>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/post_types`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Post_type>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Post_types.
   *
   * Get all Post_types
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsPostTypes(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Post_type>, message?: string}> {
    return this.getCmsPostTypesResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Post_type>, message?: string})
    );
  }

  /**
   * Store a newly created Post_type in storage
   *
   * Store Post_type
   * @param body Post_type that should be stored
   * @return successful operation
   */
  postCmsPostTypesResponse(body?: Cms_Post_type): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Post_type, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/post_types`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Post_type, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Post_type in storage
   *
   * Store Post_type
   * @param body Post_type that should be stored
   * @return successful operation
   */
  postCmsPostTypes(body?: Cms_Post_type): __Observable<{success?: boolean, data?: Cms_Post_type, message?: string}> {
    return this.postCmsPostTypesResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Post_type, message?: string})
    );
  }

  /**
   * Display the specified Post_type
   *
   * Get Post_type
   * @param id id of Post_type
   * @return successful operation
   */
  getCmsPostTypesIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Post_type, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/post_types/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Post_type, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Post_type
   *
   * Get Post_type
   * @param id id of Post_type
   * @return successful operation
   */
  getCmsPostTypesId(id: string): __Observable<{success?: boolean, data?: Cms_Post_type, message?: string}> {
    return this.getCmsPostTypesIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Post_type, message?: string})
    );
  }

  /**
   * Update the specified Post_type in storage
   *
   * Update Post_type
   * @param params The `CmsPostTypeService.PutCmsPostTypesIdParams` containing the following parameters:
   *
   * - `id`: id of Post_type
   *
   * - `body`: Post_type that should be updated
   *
   * @return successful operation
   */
  putCmsPostTypesIdResponse(params: CmsPostTypeService.PutCmsPostTypesIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Post_type, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/post_types/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Post_type, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Post_type in storage
   *
   * Update Post_type
   * @param params The `CmsPostTypeService.PutCmsPostTypesIdParams` containing the following parameters:
   *
   * - `id`: id of Post_type
   *
   * - `body`: Post_type that should be updated
   *
   * @return successful operation
   */
  putCmsPostTypesId(params: CmsPostTypeService.PutCmsPostTypesIdParams): __Observable<{success?: boolean, data?: Cms_Post_type, message?: string}> {
    return this.putCmsPostTypesIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Post_type, message?: string})
    );
  }

  /**
   * Remove the specified Post_type from storage
   *
   * Delete Post_type
   * @param id id of Post_type
   * @return successful operation
   */
  deleteCmsPostTypesIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/post_types/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Post_type from storage
   *
   * Delete Post_type
   * @param id id of Post_type
   * @return successful operation
   */
  deleteCmsPostTypesId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsPostTypesIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsPostTypeService {

  /**
   * Parameters for putCmsPostTypesId
   */
  export interface PutCmsPostTypesIdParams {

    /**
     * id of Post_type
     */
    id: string;

    /**
     * Post_type that should be updated
     */
    body?: Cms_Post_type;
  }
}

export { CmsPostTypeService }
