/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Administrasi_Provinsi } from '../models/administrasi-_provinsi';
@Injectable({
  providedIn: 'root',
})
class AdministrasiProvinsiService extends __BaseService {
  static readonly getAdministrasiProvinsisPath = '/administrasi/provinsis';
  static readonly postAdministrasiProvinsisPath = '/administrasi/provinsis';
  static readonly getAdministrasiProvinsisIdPath = '/administrasi/provinsis/{id}';
  static readonly putAdministrasiProvinsisIdPath = '/administrasi/provinsis/{id}';
  static readonly deleteAdministrasiProvinsisIdPath = '/administrasi/provinsis/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Provinsis.
   *
   * Get all Provinsis
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getAdministrasiProvinsisResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Administrasi_Provinsi>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/administrasi/provinsis`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Administrasi_Provinsi>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Provinsis.
   *
   * Get all Provinsis
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getAdministrasiProvinsis(filter?: string): __Observable<{success?: boolean, data?: Array<Administrasi_Provinsi>, message?: string}> {
    return this.getAdministrasiProvinsisResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Administrasi_Provinsi>, message?: string})
    );
  }

  /**
   * Store a newly created Provinsi in storage
   *
   * Store Provinsi
   * @param body Provinsi that should be stored
   * @return successful operation
   */
  postAdministrasiProvinsisResponse(body?: Administrasi_Provinsi): __Observable<__StrictHttpResponse<{success?: boolean, data?: Administrasi_Provinsi, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/administrasi/provinsis`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Administrasi_Provinsi, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Provinsi in storage
   *
   * Store Provinsi
   * @param body Provinsi that should be stored
   * @return successful operation
   */
  postAdministrasiProvinsis(body?: Administrasi_Provinsi): __Observable<{success?: boolean, data?: Administrasi_Provinsi, message?: string}> {
    return this.postAdministrasiProvinsisResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Administrasi_Provinsi, message?: string})
    );
  }

  /**
   * Display the specified Provinsi
   *
   * Get Provinsi
   * @param id id of Provinsi
   * @return successful operation
   */
  getAdministrasiProvinsisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Administrasi_Provinsi, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/administrasi/provinsis/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Administrasi_Provinsi, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Provinsi
   *
   * Get Provinsi
   * @param id id of Provinsi
   * @return successful operation
   */
  getAdministrasiProvinsisId(id: string): __Observable<{success?: boolean, data?: Administrasi_Provinsi, message?: string}> {
    return this.getAdministrasiProvinsisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Administrasi_Provinsi, message?: string})
    );
  }

  /**
   * Update the specified Provinsi in storage
   *
   * Update Provinsi
   * @param params The `AdministrasiProvinsiService.PutAdministrasiProvinsisIdParams` containing the following parameters:
   *
   * - `id`: id of Provinsi
   *
   * - `body`: Provinsi that should be updated
   *
   * @return successful operation
   */
  putAdministrasiProvinsisIdResponse(params: AdministrasiProvinsiService.PutAdministrasiProvinsisIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Administrasi_Provinsi, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/administrasi/provinsis/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Administrasi_Provinsi, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Provinsi in storage
   *
   * Update Provinsi
   * @param params The `AdministrasiProvinsiService.PutAdministrasiProvinsisIdParams` containing the following parameters:
   *
   * - `id`: id of Provinsi
   *
   * - `body`: Provinsi that should be updated
   *
   * @return successful operation
   */
  putAdministrasiProvinsisId(params: AdministrasiProvinsiService.PutAdministrasiProvinsisIdParams): __Observable<{success?: boolean, data?: Administrasi_Provinsi, message?: string}> {
    return this.putAdministrasiProvinsisIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Administrasi_Provinsi, message?: string})
    );
  }

  /**
   * Remove the specified Provinsi from storage
   *
   * Delete Provinsi
   * @param id id of Provinsi
   * @return successful operation
   */
  deleteAdministrasiProvinsisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/administrasi/provinsis/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Provinsi from storage
   *
   * Delete Provinsi
   * @param id id of Provinsi
   * @return successful operation
   */
  deleteAdministrasiProvinsisId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteAdministrasiProvinsisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module AdministrasiProvinsiService {

  /**
   * Parameters for putAdministrasiProvinsisId
   */
  export interface PutAdministrasiProvinsisIdParams {

    /**
     * id of Provinsi
     */
    id: string;

    /**
     * Provinsi that should be updated
     */
    body?: Administrasi_Provinsi;
  }
}

export { AdministrasiProvinsiService }
