/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Post } from '../models/cms-_post';
@Injectable({
  providedIn: 'root',
})
class CmsPostService extends __BaseService {
  static readonly getCmsPostsPath = '/cms/posts';
  static readonly postCmsPostsPath = '/cms/posts';
  static readonly getCmsPostsIdPath = '/cms/posts/{id}';
  static readonly putCmsPostsIdPath = '/cms/posts/{id}';
  static readonly deleteCmsPostsIdPath = '/cms/posts/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Posts.
   *
   * Get all Posts
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsPostsResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Post>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/posts`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Post>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Posts.
   *
   * Get all Posts
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsPosts(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Post>, message?: string}> {
    return this.getCmsPostsResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Post>, message?: string})
    );
  }

  /**
   * Store a newly created Post in storage
   *
   * Store Post
   * @param body Post that should be stored
   * @return successful operation
   */
  postCmsPostsResponse(body?: Cms_Post): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Post, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/posts`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Post, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Post in storage
   *
   * Store Post
   * @param body Post that should be stored
   * @return successful operation
   */
  postCmsPosts(body?: Cms_Post): __Observable<{success?: boolean, data?: Cms_Post, message?: string}> {
    return this.postCmsPostsResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Post, message?: string})
    );
  }

  /**
   * Display the specified Post
   *
   * Get Post
   * @param id id of Post
   * @return successful operation
   */
  getCmsPostsIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Post, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/posts/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Post, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Post
   *
   * Get Post
   * @param id id of Post
   * @return successful operation
   */
  getCmsPostsId(id: string): __Observable<{success?: boolean, data?: Cms_Post, message?: string}> {
    return this.getCmsPostsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Post, message?: string})
    );
  }

  /**
   * Update the specified Post in storage
   *
   * Update Post
   * @param params The `CmsPostService.PutCmsPostsIdParams` containing the following parameters:
   *
   * - `id`: id of Post
   *
   * - `body`: Post that should be updated
   *
   * @return successful operation
   */
  putCmsPostsIdResponse(params: CmsPostService.PutCmsPostsIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Post, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/posts/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Post, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Post in storage
   *
   * Update Post
   * @param params The `CmsPostService.PutCmsPostsIdParams` containing the following parameters:
   *
   * - `id`: id of Post
   *
   * - `body`: Post that should be updated
   *
   * @return successful operation
   */
  putCmsPostsId(params: CmsPostService.PutCmsPostsIdParams): __Observable<{success?: boolean, data?: Cms_Post, message?: string}> {
    return this.putCmsPostsIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Post, message?: string})
    );
  }

  /**
   * Remove the specified Post from storage
   *
   * Delete Post
   * @param id id of Post
   * @return successful operation
   */
  deleteCmsPostsIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/posts/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Post from storage
   *
   * Delete Post
   * @param id id of Post
   * @return successful operation
   */
  deleteCmsPostsId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsPostsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsPostService {

  /**
   * Parameters for putCmsPostsId
   */
  export interface PutCmsPostsIdParams {

    /**
     * id of Post
     */
    id: string;

    /**
     * Post that should be updated
     */
    body?: Cms_Post;
  }
}

export { CmsPostService }
