/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Widget } from '../models/cms-_widget';
@Injectable({
  providedIn: 'root',
})
class CmsWidgetService extends __BaseService {
  static readonly getCmsWidgetsPath = '/cms/widgets';
  static readonly postCmsWidgetsPath = '/cms/widgets';
  static readonly getCmsWidgetsIdPath = '/cms/widgets/{id}';
  static readonly putCmsWidgetsIdPath = '/cms/widgets/{id}';
  static readonly deleteCmsWidgetsIdPath = '/cms/widgets/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Widgets.
   *
   * Get all Widgets
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsWidgetsResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Widget>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/widgets`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Widget>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Widgets.
   *
   * Get all Widgets
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsWidgets(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Widget>, message?: string}> {
    return this.getCmsWidgetsResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Widget>, message?: string})
    );
  }

  /**
   * Store a newly created Widget in storage
   *
   * Store Widget
   * @param body Widget that should be stored
   * @return successful operation
   */
  postCmsWidgetsResponse(body?: Cms_Widget): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Widget, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/widgets`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Widget, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Widget in storage
   *
   * Store Widget
   * @param body Widget that should be stored
   * @return successful operation
   */
  postCmsWidgets(body?: Cms_Widget): __Observable<{success?: boolean, data?: Cms_Widget, message?: string}> {
    return this.postCmsWidgetsResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Widget, message?: string})
    );
  }

  /**
   * Display the specified Widget
   *
   * Get Widget
   * @param id id of Widget
   * @return successful operation
   */
  getCmsWidgetsIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Widget, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/widgets/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Widget, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Widget
   *
   * Get Widget
   * @param id id of Widget
   * @return successful operation
   */
  getCmsWidgetsId(id: string): __Observable<{success?: boolean, data?: Cms_Widget, message?: string}> {
    return this.getCmsWidgetsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Widget, message?: string})
    );
  }

  /**
   * Update the specified Widget in storage
   *
   * Update Widget
   * @param params The `CmsWidgetService.PutCmsWidgetsIdParams` containing the following parameters:
   *
   * - `id`: id of Widget
   *
   * - `body`: Widget that should be updated
   *
   * @return successful operation
   */
  putCmsWidgetsIdResponse(params: CmsWidgetService.PutCmsWidgetsIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Widget, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/widgets/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Widget, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Widget in storage
   *
   * Update Widget
   * @param params The `CmsWidgetService.PutCmsWidgetsIdParams` containing the following parameters:
   *
   * - `id`: id of Widget
   *
   * - `body`: Widget that should be updated
   *
   * @return successful operation
   */
  putCmsWidgetsId(params: CmsWidgetService.PutCmsWidgetsIdParams): __Observable<{success?: boolean, data?: Cms_Widget, message?: string}> {
    return this.putCmsWidgetsIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Widget, message?: string})
    );
  }

  /**
   * Remove the specified Widget from storage
   *
   * Delete Widget
   * @param id id of Widget
   * @return successful operation
   */
  deleteCmsWidgetsIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/widgets/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Widget from storage
   *
   * Delete Widget
   * @param id id of Widget
   * @return successful operation
   */
  deleteCmsWidgetsId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsWidgetsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsWidgetService {

  /**
   * Parameters for putCmsWidgetsId
   */
  export interface PutCmsWidgetsIdParams {

    /**
     * id of Widget
     */
    id: string;

    /**
     * Widget that should be updated
     */
    body?: Cms_Widget;
  }
}

export { CmsWidgetService }
