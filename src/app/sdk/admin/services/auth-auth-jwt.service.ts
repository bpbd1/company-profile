/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { AuthJwt } from '../models/auth-jwt';
@Injectable({
  providedIn: 'root',
})
class AuthAuthJwtService extends __BaseService {
  static readonly getAuthAuthJwtsPath = '/auth/auth_jwts';
  static readonly postAuthAuthJwtsPath = '/auth/auth_jwts';
  static readonly getAuthAuthJwtsIdPath = '/auth/auth_jwts/{id}';
  static readonly putAuthAuthJwtsIdPath = '/auth/auth_jwts/{id}';
  static readonly deleteAuthAuthJwtsIdPath = '/auth/auth_jwts/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the authJwts.
   *
   * Get all authJwts
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getAuthAuthJwtsResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<AuthJwt>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/auth/auth_jwts`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<AuthJwt>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the authJwts.
   *
   * Get all authJwts
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getAuthAuthJwts(filter?: string): __Observable<{success?: boolean, data?: Array<AuthJwt>, message?: string}> {
    return this.getAuthAuthJwtsResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<AuthJwt>, message?: string})
    );
  }

  /**
   * Store a newly created authJwt in storage
   *
   * Store authJwt
   * @param body authJwt that should be stored
   * @return successful operation
   */
  postAuthAuthJwtsResponse(body?: AuthJwt): __Observable<__StrictHttpResponse<{success?: boolean, data?: AuthJwt, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/auth/auth_jwts`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: AuthJwt, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created authJwt in storage
   *
   * Store authJwt
   * @param body authJwt that should be stored
   * @return successful operation
   */
  postAuthAuthJwts(body?: AuthJwt): __Observable<{success?: boolean, data?: AuthJwt, message?: string}> {
    return this.postAuthAuthJwtsResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: AuthJwt, message?: string})
    );
  }

  /**
   * Display the specified authJwt
   *
   * Get authJwt
   * @param id id of authJwt
   * @return successful operation
   */
  getAuthAuthJwtsIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: AuthJwt, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/auth/auth_jwts/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: AuthJwt, message?: string}>;
      })
    );
  }
  /**
   * Display the specified authJwt
   *
   * Get authJwt
   * @param id id of authJwt
   * @return successful operation
   */
  getAuthAuthJwtsId(id: string): __Observable<{success?: boolean, data?: AuthJwt, message?: string}> {
    return this.getAuthAuthJwtsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: AuthJwt, message?: string})
    );
  }

  /**
   * Update the specified authJwt in storage
   *
   * Update authJwt
   * @param params The `AuthAuthJwtService.PutAuthAuthJwtsIdParams` containing the following parameters:
   *
   * - `id`: id of authJwt
   *
   * - `body`: authJwt that should be updated
   *
   * @return successful operation
   */
  putAuthAuthJwtsIdResponse(params: AuthAuthJwtService.PutAuthAuthJwtsIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: AuthJwt, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/auth/auth_jwts/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: AuthJwt, message?: string}>;
      })
    );
  }
  /**
   * Update the specified authJwt in storage
   *
   * Update authJwt
   * @param params The `AuthAuthJwtService.PutAuthAuthJwtsIdParams` containing the following parameters:
   *
   * - `id`: id of authJwt
   *
   * - `body`: authJwt that should be updated
   *
   * @return successful operation
   */
  putAuthAuthJwtsId(params: AuthAuthJwtService.PutAuthAuthJwtsIdParams): __Observable<{success?: boolean, data?: AuthJwt, message?: string}> {
    return this.putAuthAuthJwtsIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: AuthJwt, message?: string})
    );
  }

  /**
   * Remove the specified authJwt from storage
   *
   * Delete authJwt
   * @param id id of authJwt
   * @return successful operation
   */
  deleteAuthAuthJwtsIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/auth/auth_jwts/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified authJwt from storage
   *
   * Delete authJwt
   * @param id id of authJwt
   * @return successful operation
   */
  deleteAuthAuthJwtsId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteAuthAuthJwtsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module AuthAuthJwtService {

  /**
   * Parameters for putAuthAuthJwtsId
   */
  export interface PutAuthAuthJwtsIdParams {

    /**
     * id of authJwt
     */
    id: string;

    /**
     * authJwt that should be updated
     */
    body?: AuthJwt;
  }
}

export { AuthAuthJwtService }
