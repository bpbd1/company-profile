/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_jalur_evakuasi } from '../models/cms-_jalur-_evakuasi';
@Injectable({
  providedIn: 'root',
})
class CmsJalurEvakuasiService extends __BaseService {
  static readonly getCmsJalurEvakuasisPath = '/cms/jalur_evakuasis';
  static readonly postCmsJalurEvakuasisPath = '/cms/jalur_evakuasis';
  static readonly getCmsJalurEvakuasisIdPath = '/cms/jalur_evakuasis/{id}';
  static readonly putCmsJalurEvakuasisIdPath = '/cms/jalur_evakuasis/{id}';
  static readonly deleteCmsJalurEvakuasisIdPath = '/cms/jalur_evakuasis/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the jalur_evakuasis.
   *
   * Get all jalur_evakuasis
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsJalurEvakuasisResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_jalur_evakuasi>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/jalur_evakuasis`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_jalur_evakuasi>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the jalur_evakuasis.
   *
   * Get all jalur_evakuasis
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsJalurEvakuasis(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_jalur_evakuasi>, message?: string}> {
    return this.getCmsJalurEvakuasisResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_jalur_evakuasi>, message?: string})
    );
  }

  /**
   * Store a newly created jalur_evakuasi in storage
   *
   * Store jalur_evakuasi
   * @param body jalur_evakuasi that should be stored
   * @return successful operation
   */
  postCmsJalurEvakuasisResponse(body?: Cms_jalur_evakuasi): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_jalur_evakuasi, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/jalur_evakuasis`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_jalur_evakuasi, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created jalur_evakuasi in storage
   *
   * Store jalur_evakuasi
   * @param body jalur_evakuasi that should be stored
   * @return successful operation
   */
  postCmsJalurEvakuasis(body?: Cms_jalur_evakuasi): __Observable<{success?: boolean, data?: Cms_jalur_evakuasi, message?: string}> {
    return this.postCmsJalurEvakuasisResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_jalur_evakuasi, message?: string})
    );
  }

  /**
   * Display the specified jalur_evakuasi
   *
   * Get jalur_evakuasi
   * @param id id of jalur_evakuasi
   * @return successful operation
   */
  getCmsJalurEvakuasisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_jalur_evakuasi, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/jalur_evakuasis/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_jalur_evakuasi, message?: string}>;
      })
    );
  }
  /**
   * Display the specified jalur_evakuasi
   *
   * Get jalur_evakuasi
   * @param id id of jalur_evakuasi
   * @return successful operation
   */
  getCmsJalurEvakuasisId(id: string): __Observable<{success?: boolean, data?: Cms_jalur_evakuasi, message?: string}> {
    return this.getCmsJalurEvakuasisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_jalur_evakuasi, message?: string})
    );
  }

  /**
   * Update the specified jalur_evakuasi in storage
   *
   * Update jalur_evakuasi
   * @param params The `CmsJalurEvakuasiService.PutCmsJalurEvakuasisIdParams` containing the following parameters:
   *
   * - `id`: id of jalur_evakuasi
   *
   * - `body`: jalur_evakuasi that should be updated
   *
   * @return successful operation
   */
  putCmsJalurEvakuasisIdResponse(params: CmsJalurEvakuasiService.PutCmsJalurEvakuasisIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_jalur_evakuasi, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/jalur_evakuasis/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_jalur_evakuasi, message?: string}>;
      })
    );
  }
  /**
   * Update the specified jalur_evakuasi in storage
   *
   * Update jalur_evakuasi
   * @param params The `CmsJalurEvakuasiService.PutCmsJalurEvakuasisIdParams` containing the following parameters:
   *
   * - `id`: id of jalur_evakuasi
   *
   * - `body`: jalur_evakuasi that should be updated
   *
   * @return successful operation
   */
  putCmsJalurEvakuasisId(params: CmsJalurEvakuasiService.PutCmsJalurEvakuasisIdParams): __Observable<{success?: boolean, data?: Cms_jalur_evakuasi, message?: string}> {
    return this.putCmsJalurEvakuasisIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_jalur_evakuasi, message?: string})
    );
  }

  /**
   * Remove the specified jalur_evakuasi from storage
   *
   * Delete jalur_evakuasi
   * @param id id of jalur_evakuasi
   * @return successful operation
   */
  deleteCmsJalurEvakuasisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/jalur_evakuasis/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified jalur_evakuasi from storage
   *
   * Delete jalur_evakuasi
   * @param id id of jalur_evakuasi
   * @return successful operation
   */
  deleteCmsJalurEvakuasisId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsJalurEvakuasisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsJalurEvakuasiService {

  /**
   * Parameters for putCmsJalurEvakuasisId
   */
  export interface PutCmsJalurEvakuasisIdParams {

    /**
     * id of jalur_evakuasi
     */
    id: string;

    /**
     * jalur_evakuasi that should be updated
     */
    body?: Cms_jalur_evakuasi;
  }
}

export { CmsJalurEvakuasiService }
