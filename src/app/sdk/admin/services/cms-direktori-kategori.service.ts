/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Direktori_kategori } from '../models/cms-_direktori-_kategori';
@Injectable({
  providedIn: 'root',
})
class CmsDirektoriKategoriService extends __BaseService {
  static readonly getCmsDirektoriKategorisPath = '/cms/direktori_kategoris';
  static readonly postCmsDirektoriKategorisPath = '/cms/direktori_kategoris';
  static readonly getCmsDirektoriKategorisIdPath = '/cms/direktori_kategoris/{id}';
  static readonly putCmsDirektoriKategorisIdPath = '/cms/direktori_kategoris/{id}';
  static readonly deleteCmsDirektoriKategorisIdPath = '/cms/direktori_kategoris/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Direktori_kategoris.
   *
   * Get all Direktori_kategoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsDirektoriKategorisResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Direktori_kategori>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/direktori_kategoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Direktori_kategori>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Direktori_kategoris.
   *
   * Get all Direktori_kategoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsDirektoriKategoris(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Direktori_kategori>, message?: string}> {
    return this.getCmsDirektoriKategorisResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Direktori_kategori>, message?: string})
    );
  }

  /**
   * Store a newly created Direktori_kategori in storage
   *
   * Store Direktori_kategori
   * @param body Direktori_kategori that should be stored
   * @return successful operation
   */
  postCmsDirektoriKategorisResponse(body?: Cms_Direktori_kategori): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Direktori_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/direktori_kategoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Direktori_kategori, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Direktori_kategori in storage
   *
   * Store Direktori_kategori
   * @param body Direktori_kategori that should be stored
   * @return successful operation
   */
  postCmsDirektoriKategoris(body?: Cms_Direktori_kategori): __Observable<{success?: boolean, data?: Cms_Direktori_kategori, message?: string}> {
    return this.postCmsDirektoriKategorisResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Direktori_kategori, message?: string})
    );
  }

  /**
   * Display the specified Direktori_kategori
   *
   * Get Direktori_kategori
   * @param id id of Direktori_kategori
   * @return successful operation
   */
  getCmsDirektoriKategorisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Direktori_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/direktori_kategoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Direktori_kategori, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Direktori_kategori
   *
   * Get Direktori_kategori
   * @param id id of Direktori_kategori
   * @return successful operation
   */
  getCmsDirektoriKategorisId(id: string): __Observable<{success?: boolean, data?: Cms_Direktori_kategori, message?: string}> {
    return this.getCmsDirektoriKategorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Direktori_kategori, message?: string})
    );
  }

  /**
   * Update the specified Direktori_kategori in storage
   *
   * Update Direktori_kategori
   * @param params The `CmsDirektoriKategoriService.PutCmsDirektoriKategorisIdParams` containing the following parameters:
   *
   * - `id`: id of Direktori_kategori
   *
   * - `body`: Direktori_kategori that should be updated
   *
   * @return successful operation
   */
  putCmsDirektoriKategorisIdResponse(params: CmsDirektoriKategoriService.PutCmsDirektoriKategorisIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Direktori_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/direktori_kategoris/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Direktori_kategori, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Direktori_kategori in storage
   *
   * Update Direktori_kategori
   * @param params The `CmsDirektoriKategoriService.PutCmsDirektoriKategorisIdParams` containing the following parameters:
   *
   * - `id`: id of Direktori_kategori
   *
   * - `body`: Direktori_kategori that should be updated
   *
   * @return successful operation
   */
  putCmsDirektoriKategorisId(params: CmsDirektoriKategoriService.PutCmsDirektoriKategorisIdParams): __Observable<{success?: boolean, data?: Cms_Direktori_kategori, message?: string}> {
    return this.putCmsDirektoriKategorisIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Direktori_kategori, message?: string})
    );
  }

  /**
   * Remove the specified Direktori_kategori from storage
   *
   * Delete Direktori_kategori
   * @param id id of Direktori_kategori
   * @return successful operation
   */
  deleteCmsDirektoriKategorisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/direktori_kategoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Direktori_kategori from storage
   *
   * Delete Direktori_kategori
   * @param id id of Direktori_kategori
   * @return successful operation
   */
  deleteCmsDirektoriKategorisId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsDirektoriKategorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsDirektoriKategoriService {

  /**
   * Parameters for putCmsDirektoriKategorisId
   */
  export interface PutCmsDirektoriKategorisIdParams {

    /**
     * id of Direktori_kategori
     */
    id: string;

    /**
     * Direktori_kategori that should be updated
     */
    body?: Cms_Direktori_kategori;
  }
}

export { CmsDirektoriKategoriService }
