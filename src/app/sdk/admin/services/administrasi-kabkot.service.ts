/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Administrasi_Kabkot } from '../models/administrasi-_kabkot';
@Injectable({
  providedIn: 'root',
})
class AdministrasiKabkotService extends __BaseService {
  static readonly getAdministrasiKabkotsPath = '/administrasi/kabkots';
  static readonly postAdministrasiKabkotsPath = '/administrasi/kabkots';
  static readonly getAdministrasiKabkotsIdPath = '/administrasi/kabkots/{id}';
  static readonly putAdministrasiKabkotsIdPath = '/administrasi/kabkots/{id}';
  static readonly deleteAdministrasiKabkotsIdPath = '/administrasi/kabkots/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Kabkots.
   *
   * Get all Kabkots
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getAdministrasiKabkotsResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Administrasi_Kabkot>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/administrasi/kabkots`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Administrasi_Kabkot>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Kabkots.
   *
   * Get all Kabkots
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getAdministrasiKabkots(filter?: string): __Observable<{success?: boolean, data?: Array<Administrasi_Kabkot>, message?: string}> {
    return this.getAdministrasiKabkotsResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Administrasi_Kabkot>, message?: string})
    );
  }

  /**
   * Store a newly created Kabkot in storage
   *
   * Store Kabkot
   * @param body Kabkot that should be stored
   * @return successful operation
   */
  postAdministrasiKabkotsResponse(body?: Administrasi_Kabkot): __Observable<__StrictHttpResponse<{success?: boolean, data?: Administrasi_Kabkot, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/administrasi/kabkots`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Administrasi_Kabkot, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Kabkot in storage
   *
   * Store Kabkot
   * @param body Kabkot that should be stored
   * @return successful operation
   */
  postAdministrasiKabkots(body?: Administrasi_Kabkot): __Observable<{success?: boolean, data?: Administrasi_Kabkot, message?: string}> {
    return this.postAdministrasiKabkotsResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Administrasi_Kabkot, message?: string})
    );
  }

  /**
   * Display the specified Kabkot
   *
   * Get Kabkot
   * @param id id of Kabkot
   * @return successful operation
   */
  getAdministrasiKabkotsIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Administrasi_Kabkot, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/administrasi/kabkots/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Administrasi_Kabkot, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Kabkot
   *
   * Get Kabkot
   * @param id id of Kabkot
   * @return successful operation
   */
  getAdministrasiKabkotsId(id: string): __Observable<{success?: boolean, data?: Administrasi_Kabkot, message?: string}> {
    return this.getAdministrasiKabkotsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Administrasi_Kabkot, message?: string})
    );
  }

  /**
   * Update the specified Kabkot in storage
   *
   * Update Kabkot
   * @param params The `AdministrasiKabkotService.PutAdministrasiKabkotsIdParams` containing the following parameters:
   *
   * - `id`: id of Kabkot
   *
   * - `body`: Kabkot that should be updated
   *
   * @return successful operation
   */
  putAdministrasiKabkotsIdResponse(params: AdministrasiKabkotService.PutAdministrasiKabkotsIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Administrasi_Kabkot, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/administrasi/kabkots/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Administrasi_Kabkot, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Kabkot in storage
   *
   * Update Kabkot
   * @param params The `AdministrasiKabkotService.PutAdministrasiKabkotsIdParams` containing the following parameters:
   *
   * - `id`: id of Kabkot
   *
   * - `body`: Kabkot that should be updated
   *
   * @return successful operation
   */
  putAdministrasiKabkotsId(params: AdministrasiKabkotService.PutAdministrasiKabkotsIdParams): __Observable<{success?: boolean, data?: Administrasi_Kabkot, message?: string}> {
    return this.putAdministrasiKabkotsIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Administrasi_Kabkot, message?: string})
    );
  }

  /**
   * Remove the specified Kabkot from storage
   *
   * Delete Kabkot
   * @param id id of Kabkot
   * @return successful operation
   */
  deleteAdministrasiKabkotsIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/administrasi/kabkots/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Kabkot from storage
   *
   * Delete Kabkot
   * @param id id of Kabkot
   * @return successful operation
   */
  deleteAdministrasiKabkotsId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteAdministrasiKabkotsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module AdministrasiKabkotService {

  /**
   * Parameters for putAdministrasiKabkotsId
   */
  export interface PutAdministrasiKabkotsIdParams {

    /**
     * id of Kabkot
     */
    id: string;

    /**
     * Kabkot that should be updated
     */
    body?: Administrasi_Kabkot;
  }
}

export { AdministrasiKabkotService }
