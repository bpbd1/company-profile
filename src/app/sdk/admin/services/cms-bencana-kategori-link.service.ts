/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_bencana_kategori_link } from '../models/cms-_bencana-_kategori-_link';
@Injectable({
  providedIn: 'root',
})
class CmsBencanaKategoriLinkService extends __BaseService {
  static readonly getCmsBencanaKategoriLinksPath = '/cms/bencana_kategori_links';
  static readonly postCmsBencanaKategoriLinksPath = '/cms/bencana_kategori_links';
  static readonly getCmsBencanaKategoriLinksIdPath = '/cms/bencana_kategori_links/{id}';
  static readonly putCmsBencanaKategoriLinksIdPath = '/cms/bencana_kategori_links/{id}';
  static readonly deleteCmsBencanaKategoriLinksIdPath = '/cms/bencana_kategori_links/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the bencana_kategori_links.
   *
   * Get all bencana_kategori_links
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsBencanaKategoriLinksResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_bencana_kategori_link>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/bencana_kategori_links`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_bencana_kategori_link>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the bencana_kategori_links.
   *
   * Get all bencana_kategori_links
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsBencanaKategoriLinks(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_bencana_kategori_link>, message?: string}> {
    return this.getCmsBencanaKategoriLinksResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_bencana_kategori_link>, message?: string})
    );
  }

  /**
   * Store a newly created bencana_kategori_link in storage
   *
   * Store bencana_kategori_link
   * @param body bencana_kategori_link that should be stored
   * @return successful operation
   */
  postCmsBencanaKategoriLinksResponse(body?: Cms_bencana_kategori_link): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_bencana_kategori_link, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/bencana_kategori_links`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_bencana_kategori_link, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created bencana_kategori_link in storage
   *
   * Store bencana_kategori_link
   * @param body bencana_kategori_link that should be stored
   * @return successful operation
   */
  postCmsBencanaKategoriLinks(body?: Cms_bencana_kategori_link): __Observable<{success?: boolean, data?: Cms_bencana_kategori_link, message?: string}> {
    return this.postCmsBencanaKategoriLinksResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_bencana_kategori_link, message?: string})
    );
  }

  /**
   * Display the specified bencana_kategori_link
   *
   * Get bencana_kategori_link
   * @param id id of bencana_kategori_link
   * @return successful operation
   */
  getCmsBencanaKategoriLinksIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_bencana_kategori_link, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/bencana_kategori_links/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_bencana_kategori_link, message?: string}>;
      })
    );
  }
  /**
   * Display the specified bencana_kategori_link
   *
   * Get bencana_kategori_link
   * @param id id of bencana_kategori_link
   * @return successful operation
   */
  getCmsBencanaKategoriLinksId(id: string): __Observable<{success?: boolean, data?: Cms_bencana_kategori_link, message?: string}> {
    return this.getCmsBencanaKategoriLinksIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_bencana_kategori_link, message?: string})
    );
  }

  /**
   * Update the specified bencana_kategori_link in storage
   *
   * Update bencana_kategori_link
   * @param params The `CmsBencanaKategoriLinkService.PutCmsBencanaKategoriLinksIdParams` containing the following parameters:
   *
   * - `id`: id of bencana_kategori_link
   *
   * - `body`: bencana_kategori_link that should be updated
   *
   * @return successful operation
   */
  putCmsBencanaKategoriLinksIdResponse(params: CmsBencanaKategoriLinkService.PutCmsBencanaKategoriLinksIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_bencana_kategori_link, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/bencana_kategori_links/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_bencana_kategori_link, message?: string}>;
      })
    );
  }
  /**
   * Update the specified bencana_kategori_link in storage
   *
   * Update bencana_kategori_link
   * @param params The `CmsBencanaKategoriLinkService.PutCmsBencanaKategoriLinksIdParams` containing the following parameters:
   *
   * - `id`: id of bencana_kategori_link
   *
   * - `body`: bencana_kategori_link that should be updated
   *
   * @return successful operation
   */
  putCmsBencanaKategoriLinksId(params: CmsBencanaKategoriLinkService.PutCmsBencanaKategoriLinksIdParams): __Observable<{success?: boolean, data?: Cms_bencana_kategori_link, message?: string}> {
    return this.putCmsBencanaKategoriLinksIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_bencana_kategori_link, message?: string})
    );
  }

  /**
   * Remove the specified bencana_kategori_link from storage
   *
   * Delete bencana_kategori_link
   * @param id id of bencana_kategori_link
   * @return successful operation
   */
  deleteCmsBencanaKategoriLinksIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/bencana_kategori_links/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified bencana_kategori_link from storage
   *
   * Delete bencana_kategori_link
   * @param id id of bencana_kategori_link
   * @return successful operation
   */
  deleteCmsBencanaKategoriLinksId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsBencanaKategoriLinksIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsBencanaKategoriLinkService {

  /**
   * Parameters for putCmsBencanaKategoriLinksId
   */
  export interface PutCmsBencanaKategoriLinksIdParams {

    /**
     * id of bencana_kategori_link
     */
    id: string;

    /**
     * bencana_kategori_link that should be updated
     */
    body?: Cms_bencana_kategori_link;
  }
}

export { CmsBencanaKategoriLinkService }
