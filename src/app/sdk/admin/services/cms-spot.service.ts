/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_spot } from '../models/cms-_spot';
@Injectable({
  providedIn: 'root',
})
class CmsSpotService extends __BaseService {
  static readonly getCmsSpotsPath = '/cms/spots';
  static readonly postCmsSpotsPath = '/cms/spots';
  static readonly getCmsSpotsIdPath = '/cms/spots/{id}';
  static readonly putCmsSpotsIdPath = '/cms/spots/{id}';
  static readonly deleteCmsSpotsIdPath = '/cms/spots/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the spots.
   *
   * Get all spots
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsSpotsResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_spot>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/spots`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_spot>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the spots.
   *
   * Get all spots
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsSpots(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_spot>, message?: string}> {
    return this.getCmsSpotsResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_spot>, message?: string})
    );
  }

  /**
   * Store a newly created spot in storage
   *
   * Store spot
   * @param body spot that should be stored
   * @return successful operation
   */
  postCmsSpotsResponse(body?: Cms_spot): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_spot, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/spots`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_spot, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created spot in storage
   *
   * Store spot
   * @param body spot that should be stored
   * @return successful operation
   */
  postCmsSpots(body?: Cms_spot): __Observable<{success?: boolean, data?: Cms_spot, message?: string}> {
    return this.postCmsSpotsResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_spot, message?: string})
    );
  }

  /**
   * Display the specified spot
   *
   * Get spot
   * @param id id of spot
   * @return successful operation
   */
  getCmsSpotsIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_spot, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/spots/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_spot, message?: string}>;
      })
    );
  }
  /**
   * Display the specified spot
   *
   * Get spot
   * @param id id of spot
   * @return successful operation
   */
  getCmsSpotsId(id: string): __Observable<{success?: boolean, data?: Cms_spot, message?: string}> {
    return this.getCmsSpotsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_spot, message?: string})
    );
  }

  /**
   * Update the specified spot in storage
   *
   * Update spot
   * @param params The `CmsSpotService.PutCmsSpotsIdParams` containing the following parameters:
   *
   * - `id`: id of spot
   *
   * - `body`: spot that should be updated
   *
   * @return successful operation
   */
  putCmsSpotsIdResponse(params: CmsSpotService.PutCmsSpotsIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_spot, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/spots/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_spot, message?: string}>;
      })
    );
  }
  /**
   * Update the specified spot in storage
   *
   * Update spot
   * @param params The `CmsSpotService.PutCmsSpotsIdParams` containing the following parameters:
   *
   * - `id`: id of spot
   *
   * - `body`: spot that should be updated
   *
   * @return successful operation
   */
  putCmsSpotsId(params: CmsSpotService.PutCmsSpotsIdParams): __Observable<{success?: boolean, data?: Cms_spot, message?: string}> {
    return this.putCmsSpotsIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_spot, message?: string})
    );
  }

  /**
   * Remove the specified spot from storage
   *
   * Delete spot
   * @param id id of spot
   * @return successful operation
   */
  deleteCmsSpotsIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/spots/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified spot from storage
   *
   * Delete spot
   * @param id id of spot
   * @return successful operation
   */
  deleteCmsSpotsId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsSpotsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsSpotService {

  /**
   * Parameters for putCmsSpotsId
   */
  export interface PutCmsSpotsIdParams {

    /**
     * id of spot
     */
    id: string;

    /**
     * spot that should be updated
     */
    body?: Cms_spot;
  }
}

export { CmsSpotService }
