/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Post_kategori } from '../models/cms-_post-_kategori';
@Injectable({
  providedIn: 'root',
})
class CmsPostKategoriService extends __BaseService {
  static readonly getCmsPostKategorisPath = '/cms/post_kategoris';
  static readonly postCmsPostKategorisPath = '/cms/post_kategoris';
  static readonly getCmsPostKategorisIdPath = '/cms/post_kategoris/{id}';
  static readonly putCmsPostKategorisIdPath = '/cms/post_kategoris/{id}';
  static readonly deleteCmsPostKategorisIdPath = '/cms/post_kategoris/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Post_kategoris.
   *
   * Get all Post_kategoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsPostKategorisResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Post_kategori>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/post_kategoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Post_kategori>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Post_kategoris.
   *
   * Get all Post_kategoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsPostKategoris(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Post_kategori>, message?: string}> {
    return this.getCmsPostKategorisResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Post_kategori>, message?: string})
    );
  }

  /**
   * Store a newly created Post_kategori in storage
   *
   * Store Post_kategori
   * @param body Post_kategori that should be stored
   * @return successful operation
   */
  postCmsPostKategorisResponse(body?: Cms_Post_kategori): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Post_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/post_kategoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Post_kategori, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Post_kategori in storage
   *
   * Store Post_kategori
   * @param body Post_kategori that should be stored
   * @return successful operation
   */
  postCmsPostKategoris(body?: Cms_Post_kategori): __Observable<{success?: boolean, data?: Cms_Post_kategori, message?: string}> {
    return this.postCmsPostKategorisResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Post_kategori, message?: string})
    );
  }

  /**
   * Display the specified Post_kategori
   *
   * Get Post_kategori
   * @param id id of Post_kategori
   * @return successful operation
   */
  getCmsPostKategorisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Post_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/post_kategoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Post_kategori, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Post_kategori
   *
   * Get Post_kategori
   * @param id id of Post_kategori
   * @return successful operation
   */
  getCmsPostKategorisId(id: string): __Observable<{success?: boolean, data?: Cms_Post_kategori, message?: string}> {
    return this.getCmsPostKategorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Post_kategori, message?: string})
    );
  }

  /**
   * Update the specified Post_kategori in storage
   *
   * Update Post_kategori
   * @param params The `CmsPostKategoriService.PutCmsPostKategorisIdParams` containing the following parameters:
   *
   * - `id`: id of Post_kategori
   *
   * - `body`: Post_kategori that should be updated
   *
   * @return successful operation
   */
  putCmsPostKategorisIdResponse(params: CmsPostKategoriService.PutCmsPostKategorisIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Post_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/post_kategoris/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Post_kategori, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Post_kategori in storage
   *
   * Update Post_kategori
   * @param params The `CmsPostKategoriService.PutCmsPostKategorisIdParams` containing the following parameters:
   *
   * - `id`: id of Post_kategori
   *
   * - `body`: Post_kategori that should be updated
   *
   * @return successful operation
   */
  putCmsPostKategorisId(params: CmsPostKategoriService.PutCmsPostKategorisIdParams): __Observable<{success?: boolean, data?: Cms_Post_kategori, message?: string}> {
    return this.putCmsPostKategorisIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Post_kategori, message?: string})
    );
  }

  /**
   * Remove the specified Post_kategori from storage
   *
   * Delete Post_kategori
   * @param id id of Post_kategori
   * @return successful operation
   */
  deleteCmsPostKategorisIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/post_kategoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Post_kategori from storage
   *
   * Delete Post_kategori
   * @param id id of Post_kategori
   * @return successful operation
   */
  deleteCmsPostKategorisId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsPostKategorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsPostKategoriService {

  /**
   * Parameters for putCmsPostKategorisId
   */
  export interface PutCmsPostKategorisIdParams {

    /**
     * id of Post_kategori
     */
    id: string;

    /**
     * Post_kategori that should be updated
     */
    body?: Cms_Post_kategori;
  }
}

export { CmsPostKategoriService }
