/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_bencana } from '../models/cms-_bencana';
@Injectable({
  providedIn: 'root',
})
class CmsBencanaService extends __BaseService {
  static readonly getCmsBencanasPath = '/cms/bencanas';
  static readonly postCmsBencanasPath = '/cms/bencanas';
  static readonly getCmsBencanasIdPath = '/cms/bencanas/{id}';
  static readonly putCmsBencanasIdPath = '/cms/bencanas/{id}';
  static readonly deleteCmsBencanasIdPath = '/cms/bencanas/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the bencanas.
   *
   * Get all bencanas
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsBencanasResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_bencana>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/bencanas`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_bencana>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the bencanas.
   *
   * Get all bencanas
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsBencanas(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_bencana>, message?: string}> {
    return this.getCmsBencanasResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_bencana>, message?: string})
    );
  }

  /**
   * Store a newly created bencana in storage
   *
   * Store bencana
   * @param body bencana that should be stored
   * @return successful operation
   */
  postCmsBencanasResponse(body?: Cms_bencana): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_bencana, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/bencanas`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_bencana, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created bencana in storage
   *
   * Store bencana
   * @param body bencana that should be stored
   * @return successful operation
   */
  postCmsBencanas(body?: Cms_bencana): __Observable<{success?: boolean, data?: Cms_bencana, message?: string}> {
    return this.postCmsBencanasResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_bencana, message?: string})
    );
  }

  /**
   * Display the specified bencana
   *
   * Get bencana
   * @param id id of bencana
   * @return successful operation
   */
  getCmsBencanasIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_bencana, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/bencanas/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_bencana, message?: string}>;
      })
    );
  }
  /**
   * Display the specified bencana
   *
   * Get bencana
   * @param id id of bencana
   * @return successful operation
   */
  getCmsBencanasId(id: string): __Observable<{success?: boolean, data?: Cms_bencana, message?: string}> {
    return this.getCmsBencanasIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_bencana, message?: string})
    );
  }

  /**
   * Update the specified bencana in storage
   *
   * Update bencana
   * @param params The `CmsBencanaService.PutCmsBencanasIdParams` containing the following parameters:
   *
   * - `id`: id of bencana
   *
   * - `body`: bencana that should be updated
   *
   * @return successful operation
   */
  putCmsBencanasIdResponse(params: CmsBencanaService.PutCmsBencanasIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_bencana, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/bencanas/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_bencana, message?: string}>;
      })
    );
  }
  /**
   * Update the specified bencana in storage
   *
   * Update bencana
   * @param params The `CmsBencanaService.PutCmsBencanasIdParams` containing the following parameters:
   *
   * - `id`: id of bencana
   *
   * - `body`: bencana that should be updated
   *
   * @return successful operation
   */
  putCmsBencanasId(params: CmsBencanaService.PutCmsBencanasIdParams): __Observable<{success?: boolean, data?: Cms_bencana, message?: string}> {
    return this.putCmsBencanasIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_bencana, message?: string})
    );
  }

  /**
   * Remove the specified bencana from storage
   *
   * Delete bencana
   * @param id id of bencana
   * @return successful operation
   */
  deleteCmsBencanasIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/bencanas/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified bencana from storage
   *
   * Delete bencana
   * @param id id of bencana
   * @return successful operation
   */
  deleteCmsBencanasId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsBencanasIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsBencanaService {

  /**
   * Parameters for putCmsBencanasId
   */
  export interface PutCmsBencanasIdParams {

    /**
     * id of bencana
     */
    id: string;

    /**
     * bencana that should be updated
     */
    body?: Cms_bencana;
  }
}

export { CmsBencanaService }
