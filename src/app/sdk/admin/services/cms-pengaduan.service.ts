/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Pengaduan } from '../models/cms-_pengaduan';
@Injectable({
  providedIn: 'root',
})
class CmsPengaduanService extends __BaseService {
  static readonly getCmsPengaduansPath = '/cms/pengaduans';
  static readonly postCmsPengaduansPath = '/cms/pengaduans';
  static readonly getCmsPengaduansIdPath = '/cms/pengaduans/{id}';
  static readonly putCmsPengaduansIdPath = '/cms/pengaduans/{id}';
  static readonly deleteCmsPengaduansIdPath = '/cms/pengaduans/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Pengaduans.
   *
   * Get all Pengaduans
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsPengaduansResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Pengaduan>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/pengaduans`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Pengaduan>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Pengaduans.
   *
   * Get all Pengaduans
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsPengaduans(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Pengaduan>, message?: string}> {
    return this.getCmsPengaduansResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Pengaduan>, message?: string})
    );
  }

  /**
   * Store a newly created Pengaduan in storage
   *
   * Store Pengaduan
   * @param body Pengaduan that should be stored
   * @return successful operation
   */
  postCmsPengaduansResponse(body?: Cms_Pengaduan): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Pengaduan, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/pengaduans`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Pengaduan, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Pengaduan in storage
   *
   * Store Pengaduan
   * @param body Pengaduan that should be stored
   * @return successful operation
   */
  postCmsPengaduans(body?: Cms_Pengaduan): __Observable<{success?: boolean, data?: Cms_Pengaduan, message?: string}> {
    return this.postCmsPengaduansResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Pengaduan, message?: string})
    );
  }

  /**
   * Display the specified Pengaduan
   *
   * Get Pengaduan
   * @param id id of Pengaduan
   * @return successful operation
   */
  getCmsPengaduansIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Pengaduan, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/pengaduans/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Pengaduan, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Pengaduan
   *
   * Get Pengaduan
   * @param id id of Pengaduan
   * @return successful operation
   */
  getCmsPengaduansId(id: string): __Observable<{success?: boolean, data?: Cms_Pengaduan, message?: string}> {
    return this.getCmsPengaduansIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Pengaduan, message?: string})
    );
  }

  /**
   * Update the specified Pengaduan in storage
   *
   * Update Pengaduan
   * @param params The `CmsPengaduanService.PutCmsPengaduansIdParams` containing the following parameters:
   *
   * - `id`: id of Pengaduan
   *
   * - `body`: Pengaduan that should be updated
   *
   * @return successful operation
   */
  putCmsPengaduansIdResponse(params: CmsPengaduanService.PutCmsPengaduansIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Pengaduan, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/pengaduans/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Pengaduan, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Pengaduan in storage
   *
   * Update Pengaduan
   * @param params The `CmsPengaduanService.PutCmsPengaduansIdParams` containing the following parameters:
   *
   * - `id`: id of Pengaduan
   *
   * - `body`: Pengaduan that should be updated
   *
   * @return successful operation
   */
  putCmsPengaduansId(params: CmsPengaduanService.PutCmsPengaduansIdParams): __Observable<{success?: boolean, data?: Cms_Pengaduan, message?: string}> {
    return this.putCmsPengaduansIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Pengaduan, message?: string})
    );
  }

  /**
   * Remove the specified Pengaduan from storage
   *
   * Delete Pengaduan
   * @param id id of Pengaduan
   * @return successful operation
   */
  deleteCmsPengaduansIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/pengaduans/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Pengaduan from storage
   *
   * Delete Pengaduan
   * @param id id of Pengaduan
   * @return successful operation
   */
  deleteCmsPengaduansId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsPengaduansIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsPengaduanService {

  /**
   * Parameters for putCmsPengaduansId
   */
  export interface PutCmsPengaduansIdParams {

    /**
     * id of Pengaduan
     */
    id: string;

    /**
     * Pengaduan that should be updated
     */
    body?: Cms_Pengaduan;
  }
}

export { CmsPengaduanService }
