/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Cms_Direktori_kategori_link } from '../models/cms-_direktori-_kategori-_link';
@Injectable({
  providedIn: 'root',
})
class CmsDirektoriKategoriLinkService extends __BaseService {
  static readonly getCmsDirektoriKategoriLinksPath = '/cms/direktori_kategori_links';
  static readonly postCmsDirektoriKategoriLinksPath = '/cms/direktori_kategori_links';
  static readonly getCmsDirektoriKategoriLinksIdPath = '/cms/direktori_kategori_links/{id}';
  static readonly putCmsDirektoriKategoriLinksIdPath = '/cms/direktori_kategori_links/{id}';
  static readonly deleteCmsDirektoriKategoriLinksIdPath = '/cms/direktori_kategori_links/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Direktori_kategori_links.
   *
   * Get all Direktori_kategori_links
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsDirektoriKategoriLinksResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Cms_Direktori_kategori_link>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/direktori_kategori_links`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Cms_Direktori_kategori_link>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Direktori_kategori_links.
   *
   * Get all Direktori_kategori_links
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getCmsDirektoriKategoriLinks(filter?: string): __Observable<{success?: boolean, data?: Array<Cms_Direktori_kategori_link>, message?: string}> {
    return this.getCmsDirektoriKategoriLinksResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Cms_Direktori_kategori_link>, message?: string})
    );
  }

  /**
   * Store a newly created Direktori_kategori_link in storage
   *
   * Store Direktori_kategori_link
   * @param body Direktori_kategori_link that should be stored
   * @return successful operation
   */
  postCmsDirektoriKategoriLinksResponse(body?: Cms_Direktori_kategori_link): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Direktori_kategori_link, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/cms/direktori_kategori_links`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Direktori_kategori_link, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Direktori_kategori_link in storage
   *
   * Store Direktori_kategori_link
   * @param body Direktori_kategori_link that should be stored
   * @return successful operation
   */
  postCmsDirektoriKategoriLinks(body?: Cms_Direktori_kategori_link): __Observable<{success?: boolean, data?: Cms_Direktori_kategori_link, message?: string}> {
    return this.postCmsDirektoriKategoriLinksResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Direktori_kategori_link, message?: string})
    );
  }

  /**
   * Display the specified Direktori_kategori_link
   *
   * Get Direktori_kategori_link
   * @param id id of Direktori_kategori_link
   * @return successful operation
   */
  getCmsDirektoriKategoriLinksIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Direktori_kategori_link, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/cms/direktori_kategori_links/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Direktori_kategori_link, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Direktori_kategori_link
   *
   * Get Direktori_kategori_link
   * @param id id of Direktori_kategori_link
   * @return successful operation
   */
  getCmsDirektoriKategoriLinksId(id: string): __Observable<{success?: boolean, data?: Cms_Direktori_kategori_link, message?: string}> {
    return this.getCmsDirektoriKategoriLinksIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Direktori_kategori_link, message?: string})
    );
  }

  /**
   * Update the specified Direktori_kategori_link in storage
   *
   * Update Direktori_kategori_link
   * @param params The `CmsDirektoriKategoriLinkService.PutCmsDirektoriKategoriLinksIdParams` containing the following parameters:
   *
   * - `id`: id of Direktori_kategori_link
   *
   * - `body`: Direktori_kategori_link that should be updated
   *
   * @return successful operation
   */
  putCmsDirektoriKategoriLinksIdResponse(params: CmsDirektoriKategoriLinkService.PutCmsDirektoriKategoriLinksIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Cms_Direktori_kategori_link, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/cms/direktori_kategori_links/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Cms_Direktori_kategori_link, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Direktori_kategori_link in storage
   *
   * Update Direktori_kategori_link
   * @param params The `CmsDirektoriKategoriLinkService.PutCmsDirektoriKategoriLinksIdParams` containing the following parameters:
   *
   * - `id`: id of Direktori_kategori_link
   *
   * - `body`: Direktori_kategori_link that should be updated
   *
   * @return successful operation
   */
  putCmsDirektoriKategoriLinksId(params: CmsDirektoriKategoriLinkService.PutCmsDirektoriKategoriLinksIdParams): __Observable<{success?: boolean, data?: Cms_Direktori_kategori_link, message?: string}> {
    return this.putCmsDirektoriKategoriLinksIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Cms_Direktori_kategori_link, message?: string})
    );
  }

  /**
   * Remove the specified Direktori_kategori_link from storage
   *
   * Delete Direktori_kategori_link
   * @param id id of Direktori_kategori_link
   * @return successful operation
   */
  deleteCmsDirektoriKategoriLinksIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/cms/direktori_kategori_links/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Direktori_kategori_link from storage
   *
   * Delete Direktori_kategori_link
   * @param id id of Direktori_kategori_link
   * @return successful operation
   */
  deleteCmsDirektoriKategoriLinksId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteCmsDirektoriKategoriLinksIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module CmsDirektoriKategoriLinkService {

  /**
   * Parameters for putCmsDirektoriKategoriLinksId
   */
  export interface PutCmsDirektoriKategoriLinksIdParams {

    /**
     * id of Direktori_kategori_link
     */
    id: string;

    /**
     * Direktori_kategori_link that should be updated
     */
    body?: Cms_Direktori_kategori_link;
  }
}

export { CmsDirektoriKategoriLinkService }
