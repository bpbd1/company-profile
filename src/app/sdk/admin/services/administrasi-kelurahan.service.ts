/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Administrasi_Kelurahan } from '../models/administrasi-_kelurahan';
@Injectable({
  providedIn: 'root',
})
class AdministrasiKelurahanService extends __BaseService {
  static readonly getAdministrasiKelurahansPath = '/administrasi/kelurahans';
  static readonly postAdministrasiKelurahansPath = '/administrasi/kelurahans';
  static readonly getAdministrasiKelurahansIdPath = '/administrasi/kelurahans/{id}';
  static readonly putAdministrasiKelurahansIdPath = '/administrasi/kelurahans/{id}';
  static readonly deleteAdministrasiKelurahansIdPath = '/administrasi/kelurahans/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get a listing of the Kelurahans.
   *
   * Get all Kelurahans
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getAdministrasiKelurahansResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Administrasi_Kelurahan>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/administrasi/kelurahans`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Administrasi_Kelurahan>, message?: string}>;
      })
    );
  }
  /**
   * Get a listing of the Kelurahans.
   *
   * Get all Kelurahans
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getAdministrasiKelurahans(filter?: string): __Observable<{success?: boolean, data?: Array<Administrasi_Kelurahan>, message?: string}> {
    return this.getAdministrasiKelurahansResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Administrasi_Kelurahan>, message?: string})
    );
  }

  /**
   * Store a newly created Kelurahan in storage
   *
   * Store Kelurahan
   * @param body Kelurahan that should be stored
   * @return successful operation
   */
  postAdministrasiKelurahansResponse(body?: Administrasi_Kelurahan): __Observable<__StrictHttpResponse<{success?: boolean, data?: Administrasi_Kelurahan, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/administrasi/kelurahans`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Administrasi_Kelurahan, message?: string}>;
      })
    );
  }
  /**
   * Store a newly created Kelurahan in storage
   *
   * Store Kelurahan
   * @param body Kelurahan that should be stored
   * @return successful operation
   */
  postAdministrasiKelurahans(body?: Administrasi_Kelurahan): __Observable<{success?: boolean, data?: Administrasi_Kelurahan, message?: string}> {
    return this.postAdministrasiKelurahansResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Administrasi_Kelurahan, message?: string})
    );
  }

  /**
   * Display the specified Kelurahan
   *
   * Get Kelurahan
   * @param id id of Kelurahan
   * @return successful operation
   */
  getAdministrasiKelurahansIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Administrasi_Kelurahan, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/administrasi/kelurahans/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Administrasi_Kelurahan, message?: string}>;
      })
    );
  }
  /**
   * Display the specified Kelurahan
   *
   * Get Kelurahan
   * @param id id of Kelurahan
   * @return successful operation
   */
  getAdministrasiKelurahansId(id: string): __Observable<{success?: boolean, data?: Administrasi_Kelurahan, message?: string}> {
    return this.getAdministrasiKelurahansIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Administrasi_Kelurahan, message?: string})
    );
  }

  /**
   * Update the specified Kelurahan in storage
   *
   * Update Kelurahan
   * @param params The `AdministrasiKelurahanService.PutAdministrasiKelurahansIdParams` containing the following parameters:
   *
   * - `id`: id of Kelurahan
   *
   * - `body`: Kelurahan that should be updated
   *
   * @return successful operation
   */
  putAdministrasiKelurahansIdResponse(params: AdministrasiKelurahanService.PutAdministrasiKelurahansIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Administrasi_Kelurahan, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/administrasi/kelurahans/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Administrasi_Kelurahan, message?: string}>;
      })
    );
  }
  /**
   * Update the specified Kelurahan in storage
   *
   * Update Kelurahan
   * @param params The `AdministrasiKelurahanService.PutAdministrasiKelurahansIdParams` containing the following parameters:
   *
   * - `id`: id of Kelurahan
   *
   * - `body`: Kelurahan that should be updated
   *
   * @return successful operation
   */
  putAdministrasiKelurahansId(params: AdministrasiKelurahanService.PutAdministrasiKelurahansIdParams): __Observable<{success?: boolean, data?: Administrasi_Kelurahan, message?: string}> {
    return this.putAdministrasiKelurahansIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Administrasi_Kelurahan, message?: string})
    );
  }

  /**
   * Remove the specified Kelurahan from storage
   *
   * Delete Kelurahan
   * @param id id of Kelurahan
   * @return successful operation
   */
  deleteAdministrasiKelurahansIdResponse(id: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/administrasi/kelurahans/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Remove the specified Kelurahan from storage
   *
   * Delete Kelurahan
   * @param id id of Kelurahan
   * @return successful operation
   */
  deleteAdministrasiKelurahansId(id: string): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteAdministrasiKelurahansIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module AdministrasiKelurahanService {

  /**
   * Parameters for putAdministrasiKelurahansId
   */
  export interface PutAdministrasiKelurahansIdParams {

    /**
     * id of Kelurahan
     */
    id: string;

    /**
     * Kelurahan that should be updated
     */
    body?: Administrasi_Kelurahan;
  }
}

export { AdministrasiKelurahanService }
