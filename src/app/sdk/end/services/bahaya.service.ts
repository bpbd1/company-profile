/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Bahaya } from '../models/bahaya';
@Injectable({
  providedIn: 'root',
})
class BahayaService extends __BaseService {
  static readonly getEndBahayasPath = '/end/bahayas';
  static readonly postEndBahayasPath = '/end/bahayas';
  static readonly getEndBahayasIdPath = '/end/bahayas/{id}';
  static readonly putEndBahayasIdPath = '/end/bahayas/{id}';
  static readonly deleteEndBahayasIdPath = '/end/bahayas/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get all Bahayas
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getEndBahayasResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Bahaya>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/end/bahayas`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Bahaya>, message?: string}>;
      })
    );
  }
  /**
   * Get all Bahayas
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getEndBahayas(filter?: string): __Observable<{success?: boolean, data?: Array<Bahaya>, message?: string}> {
    return this.getEndBahayasResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Bahaya>, message?: string})
    );
  }

  /**
   * Store Bahaya
   * @param body Bahaya that should be stored
   * @return successful operation
   */
  postEndBahayasResponse(body?: Bahaya): __Observable<__StrictHttpResponse<{success?: boolean, data?: Bahaya, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/end/bahayas`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Bahaya, message?: string}>;
      })
    );
  }
  /**
   * Store Bahaya
   * @param body Bahaya that should be stored
   * @return successful operation
   */
  postEndBahayas(body?: Bahaya): __Observable<{success?: boolean, data?: Bahaya, message?: string}> {
    return this.postEndBahayasResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Bahaya, message?: string})
    );
  }

  /**
   * Get Bahaya
   * @param id id of Bahaya
   * @return successful operation
   */
  getEndBahayasIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: Bahaya, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/end/bahayas/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Bahaya, message?: string}>;
      })
    );
  }
  /**
   * Get Bahaya
   * @param id id of Bahaya
   * @return successful operation
   */
  getEndBahayasId(id: number): __Observable<{success?: boolean, data?: Bahaya, message?: string}> {
    return this.getEndBahayasIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Bahaya, message?: string})
    );
  }

  /**
   * Update Bahaya
   * @param params The `BahayaService.PutEndBahayasIdParams` containing the following parameters:
   *
   * - `id`: id of Bahaya
   *
   * - `body`: Bahaya that should be updated
   *
   * @return successful operation
   */
  putEndBahayasIdResponse(params: BahayaService.PutEndBahayasIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Bahaya, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/end/bahayas/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Bahaya, message?: string}>;
      })
    );
  }
  /**
   * Update Bahaya
   * @param params The `BahayaService.PutEndBahayasIdParams` containing the following parameters:
   *
   * - `id`: id of Bahaya
   *
   * - `body`: Bahaya that should be updated
   *
   * @return successful operation
   */
  putEndBahayasId(params: BahayaService.PutEndBahayasIdParams): __Observable<{success?: boolean, data?: Bahaya, message?: string}> {
    return this.putEndBahayasIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Bahaya, message?: string})
    );
  }

  /**
   * Delete Bahaya
   * @param id id of Bahaya
   * @return successful operation
   */
  deleteEndBahayasIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/end/bahayas/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Delete Bahaya
   * @param id id of Bahaya
   * @return successful operation
   */
  deleteEndBahayasId(id: number): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteEndBahayasIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module BahayaService {

  /**
   * Parameters for putEndBahayasId
   */
  export interface PutEndBahayasIdParams {

    /**
     * id of Bahaya
     */
    id: number;

    /**
     * Bahaya that should be updated
     */
    body?: Bahaya;
  }
}

export { BahayaService }
