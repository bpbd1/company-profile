/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Content } from '../models/content';
@Injectable({
  providedIn: 'root',
})
class ContentService extends __BaseService {
  static readonly getEndContentsPath = '/end/contents';
  static readonly postEndContentsPath = '/end/contents';
  static readonly getEndContentsIdPath = '/end/contents/{id}';
  static readonly putEndContentsIdPath = '/end/contents/{id}';
  static readonly deleteEndContentsIdPath = '/end/contents/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get all Contents
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getEndContentsResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Content>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/end/contents`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Content>, message?: string}>;
      })
    );
  }
  /**
   * Get all Contents
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getEndContents(filter?: string): __Observable<{success?: boolean, data?: Array<Content>, message?: string}> {
    return this.getEndContentsResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Content>, message?: string})
    );
  }

  /**
   * Store Content
   * @param body Content that should be stored
   * @return successful operation
   */
  postEndContentsResponse(body?: Content): __Observable<__StrictHttpResponse<{success?: boolean, data?: Content, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/end/contents`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Content, message?: string}>;
      })
    );
  }
  /**
   * Store Content
   * @param body Content that should be stored
   * @return successful operation
   */
  postEndContents(body?: Content): __Observable<{success?: boolean, data?: Content, message?: string}> {
    return this.postEndContentsResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Content, message?: string})
    );
  }

  /**
   * Get Content
   * @param id id of Content
   * @return successful operation
   */
  getEndContentsIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: Content, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/end/contents/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Content, message?: string}>;
      })
    );
  }
  /**
   * Get Content
   * @param id id of Content
   * @return successful operation
   */
  getEndContentsId(id: number): __Observable<{success?: boolean, data?: Content, message?: string}> {
    return this.getEndContentsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Content, message?: string})
    );
  }

  /**
   * Update Content
   * @param params The `ContentService.PutEndContentsIdParams` containing the following parameters:
   *
   * - `id`: id of Content
   *
   * - `body`: Content that should be updated
   *
   * @return successful operation
   */
  putEndContentsIdResponse(params: ContentService.PutEndContentsIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Content, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/end/contents/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Content, message?: string}>;
      })
    );
  }
  /**
   * Update Content
   * @param params The `ContentService.PutEndContentsIdParams` containing the following parameters:
   *
   * - `id`: id of Content
   *
   * - `body`: Content that should be updated
   *
   * @return successful operation
   */
  putEndContentsId(params: ContentService.PutEndContentsIdParams): __Observable<{success?: boolean, data?: Content, message?: string}> {
    return this.putEndContentsIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Content, message?: string})
    );
  }

  /**
   * Delete Content
   * @param id id of Content
   * @return successful operation
   */
  deleteEndContentsIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/end/contents/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Delete Content
   * @param id id of Content
   * @return successful operation
   */
  deleteEndContentsId(id: number): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteEndContentsIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module ContentService {

  /**
   * Parameters for putEndContentsId
   */
  export interface PutEndContentsIdParams {

    /**
     * id of Content
     */
    id: number;

    /**
     * Content that should be updated
     */
    body?: Content;
  }
}

export { ContentService }
