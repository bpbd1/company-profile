/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Direktori } from '../models/direktori';
@Injectable({
  providedIn: 'root',
})
class DirektoriService extends __BaseService {
  static readonly getEndDirektorisPath = '/end/direktoris';
  static readonly postEndDirektorisPath = '/end/direktoris';
  static readonly getEndDirektorisIdPath = '/end/direktoris/{id}';
  static readonly putEndDirektorisIdPath = '/end/direktoris/{id}';
  static readonly deleteEndDirektorisIdPath = '/end/direktoris/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get all Direktoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getEndDirektorisResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Direktori>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/end/direktoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Direktori>, message?: string}>;
      })
    );
  }
  /**
   * Get all Direktoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getEndDirektoris(filter?: string): __Observable<{success?: boolean, data?: Array<Direktori>, message?: string}> {
    return this.getEndDirektorisResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Direktori>, message?: string})
    );
  }

  /**
   * Store Direktori
   * @param body Direktori that should be stored
   * @return successful operation
   */
  postEndDirektorisResponse(body?: Direktori): __Observable<__StrictHttpResponse<{success?: boolean, data?: Direktori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/end/direktoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Direktori, message?: string}>;
      })
    );
  }
  /**
   * Store Direktori
   * @param body Direktori that should be stored
   * @return successful operation
   */
  postEndDirektoris(body?: Direktori): __Observable<{success?: boolean, data?: Direktori, message?: string}> {
    return this.postEndDirektorisResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Direktori, message?: string})
    );
  }

  /**
   * Get Direktori
   * @param id id of Direktori
   * @return successful operation
   */
  getEndDirektorisIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: Direktori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/end/direktoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Direktori, message?: string}>;
      })
    );
  }
  /**
   * Get Direktori
   * @param id id of Direktori
   * @return successful operation
   */
  getEndDirektorisId(id: number): __Observable<{success?: boolean, data?: Direktori, message?: string}> {
    return this.getEndDirektorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Direktori, message?: string})
    );
  }

  /**
   * Update Direktori
   * @param params The `DirektoriService.PutEndDirektorisIdParams` containing the following parameters:
   *
   * - `id`: id of Direktori
   *
   * - `body`: Direktori that should be updated
   *
   * @return successful operation
   */
  putEndDirektorisIdResponse(params: DirektoriService.PutEndDirektorisIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Direktori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/end/direktoris/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Direktori, message?: string}>;
      })
    );
  }
  /**
   * Update Direktori
   * @param params The `DirektoriService.PutEndDirektorisIdParams` containing the following parameters:
   *
   * - `id`: id of Direktori
   *
   * - `body`: Direktori that should be updated
   *
   * @return successful operation
   */
  putEndDirektorisId(params: DirektoriService.PutEndDirektorisIdParams): __Observable<{success?: boolean, data?: Direktori, message?: string}> {
    return this.putEndDirektorisIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Direktori, message?: string})
    );
  }

  /**
   * Delete Direktori
   * @param id id of Direktori
   * @return successful operation
   */
  deleteEndDirektorisIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/end/direktoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Delete Direktori
   * @param id id of Direktori
   * @return successful operation
   */
  deleteEndDirektorisId(id: number): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteEndDirektorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module DirektoriService {

  /**
   * Parameters for putEndDirektorisId
   */
  export interface PutEndDirektorisIdParams {

    /**
     * id of Direktori
     */
    id: number;

    /**
     * Direktori that should be updated
     */
    body?: Direktori;
  }
}

export { DirektoriService }
