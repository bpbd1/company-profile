/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Kategori } from '../models/kategori';
@Injectable({
  providedIn: 'root',
})
class KategoriService extends __BaseService {
  static readonly getEndKategorisPath = '/end/kategoris';
  static readonly postEndKategorisPath = '/end/kategoris';
  static readonly getEndKategorisIdPath = '/end/kategoris/{id}';
  static readonly putEndKategorisIdPath = '/end/kategoris/{id}';
  static readonly deleteEndKategorisIdPath = '/end/kategoris/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get all Kategoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getEndKategorisResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Kategori>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/end/kategoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Kategori>, message?: string}>;
      })
    );
  }
  /**
   * Get all Kategoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getEndKategoris(filter?: string): __Observable<{success?: boolean, data?: Array<Kategori>, message?: string}> {
    return this.getEndKategorisResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Kategori>, message?: string})
    );
  }

  /**
   * Store Kategori
   * @param body Kategori that should be stored
   * @return successful operation
   */
  postEndKategorisResponse(body?: Kategori): __Observable<__StrictHttpResponse<{success?: boolean, data?: Kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/end/kategoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Kategori, message?: string}>;
      })
    );
  }
  /**
   * Store Kategori
   * @param body Kategori that should be stored
   * @return successful operation
   */
  postEndKategoris(body?: Kategori): __Observable<{success?: boolean, data?: Kategori, message?: string}> {
    return this.postEndKategorisResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Kategori, message?: string})
    );
  }

  /**
   * Get Kategori
   * @param id id of Kategori
   * @return successful operation
   */
  getEndKategorisIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: Kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/end/kategoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Kategori, message?: string}>;
      })
    );
  }
  /**
   * Get Kategori
   * @param id id of Kategori
   * @return successful operation
   */
  getEndKategorisId(id: number): __Observable<{success?: boolean, data?: Kategori, message?: string}> {
    return this.getEndKategorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Kategori, message?: string})
    );
  }

  /**
   * Update Kategori
   * @param params The `KategoriService.PutEndKategorisIdParams` containing the following parameters:
   *
   * - `id`: id of Kategori
   *
   * - `body`: Kategori that should be updated
   *
   * @return successful operation
   */
  putEndKategorisIdResponse(params: KategoriService.PutEndKategorisIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/end/kategoris/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Kategori, message?: string}>;
      })
    );
  }
  /**
   * Update Kategori
   * @param params The `KategoriService.PutEndKategorisIdParams` containing the following parameters:
   *
   * - `id`: id of Kategori
   *
   * - `body`: Kategori that should be updated
   *
   * @return successful operation
   */
  putEndKategorisId(params: KategoriService.PutEndKategorisIdParams): __Observable<{success?: boolean, data?: Kategori, message?: string}> {
    return this.putEndKategorisIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Kategori, message?: string})
    );
  }

  /**
   * Delete Kategori
   * @param id id of Kategori
   * @return successful operation
   */
  deleteEndKategorisIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/end/kategoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Delete Kategori
   * @param id id of Kategori
   * @return successful operation
   */
  deleteEndKategorisId(id: number): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteEndKategorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module KategoriService {

  /**
   * Parameters for putEndKategorisId
   */
  export interface PutEndKategorisIdParams {

    /**
     * id of Kategori
     */
    id: number;

    /**
     * Kategori that should be updated
     */
    body?: Kategori;
  }
}

export { KategoriService }
