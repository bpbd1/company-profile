/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Content_kategori } from '../models/content-_kategori';
@Injectable({
  providedIn: 'root',
})
class ContentKategoriService extends __BaseService {
  static readonly getEndContentKategorisPath = '/end/contentKategoris';
  static readonly postEndContentKategorisPath = '/end/contentKategoris';
  static readonly getEndContentKategorisIdPath = '/end/contentKategoris/{id}';
  static readonly putEndContentKategorisIdPath = '/end/contentKategoris/{id}';
  static readonly deleteEndContentKategorisIdPath = '/end/contentKategoris/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get all Content_kategoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getEndContentKategorisResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Content_kategori>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/end/contentKategoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Content_kategori>, message?: string}>;
      })
    );
  }
  /**
   * Get all Content_kategoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getEndContentKategoris(filter?: string): __Observable<{success?: boolean, data?: Array<Content_kategori>, message?: string}> {
    return this.getEndContentKategorisResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Content_kategori>, message?: string})
    );
  }

  /**
   * Store Content_kategori
   * @param body Content_kategori that should be stored
   * @return successful operation
   */
  postEndContentKategorisResponse(body?: Content_kategori): __Observable<__StrictHttpResponse<{success?: boolean, data?: Content_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/end/contentKategoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Content_kategori, message?: string}>;
      })
    );
  }
  /**
   * Store Content_kategori
   * @param body Content_kategori that should be stored
   * @return successful operation
   */
  postEndContentKategoris(body?: Content_kategori): __Observable<{success?: boolean, data?: Content_kategori, message?: string}> {
    return this.postEndContentKategorisResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Content_kategori, message?: string})
    );
  }

  /**
   * Get Content_kategori
   * @param id id of Content_kategori
   * @return successful operation
   */
  getEndContentKategorisIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: Content_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/end/contentKategoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Content_kategori, message?: string}>;
      })
    );
  }
  /**
   * Get Content_kategori
   * @param id id of Content_kategori
   * @return successful operation
   */
  getEndContentKategorisId(id: number): __Observable<{success?: boolean, data?: Content_kategori, message?: string}> {
    return this.getEndContentKategorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Content_kategori, message?: string})
    );
  }

  /**
   * Update Content_kategori
   * @param params The `ContentKategoriService.PutEndContentKategorisIdParams` containing the following parameters:
   *
   * - `id`: id of Content_kategori
   *
   * - `body`: Content_kategori that should be updated
   *
   * @return successful operation
   */
  putEndContentKategorisIdResponse(params: ContentKategoriService.PutEndContentKategorisIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Content_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/end/contentKategoris/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Content_kategori, message?: string}>;
      })
    );
  }
  /**
   * Update Content_kategori
   * @param params The `ContentKategoriService.PutEndContentKategorisIdParams` containing the following parameters:
   *
   * - `id`: id of Content_kategori
   *
   * - `body`: Content_kategori that should be updated
   *
   * @return successful operation
   */
  putEndContentKategorisId(params: ContentKategoriService.PutEndContentKategorisIdParams): __Observable<{success?: boolean, data?: Content_kategori, message?: string}> {
    return this.putEndContentKategorisIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Content_kategori, message?: string})
    );
  }

  /**
   * Delete Content_kategori
   * @param id id of Content_kategori
   * @return successful operation
   */
  deleteEndContentKategorisIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/end/contentKategoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Delete Content_kategori
   * @param id id of Content_kategori
   * @return successful operation
   */
  deleteEndContentKategorisId(id: number): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteEndContentKategorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module ContentKategoriService {

  /**
   * Parameters for putEndContentKategorisId
   */
  export interface PutEndContentKategorisIdParams {

    /**
     * id of Content_kategori
     */
    id: number;

    /**
     * Content_kategori that should be updated
     */
    body?: Content_kategori;
  }
}

export { ContentKategoriService }
