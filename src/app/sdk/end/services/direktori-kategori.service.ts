/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { Direktori_kategori } from '../models/direktori-_kategori';
@Injectable({
  providedIn: 'root',
})
class DirektoriKategoriService extends __BaseService {
  static readonly getEndDirektoriKategorisPath = '/end/direktoriKategoris';
  static readonly postEndDirektoriKategorisPath = '/end/direktoriKategoris';
  static readonly getEndDirektoriKategorisIdPath = '/end/direktoriKategoris/{id}';
  static readonly putEndDirektoriKategorisIdPath = '/end/direktoriKategoris/{id}';
  static readonly deleteEndDirektoriKategorisIdPath = '/end/direktoriKategoris/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Get all Direktori_kategoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getEndDirektoriKategorisResponse(filter?: string): __Observable<__StrictHttpResponse<{success?: boolean, data?: Array<Direktori_kategori>, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (filter != null) __params = __params.set('filter', filter.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/end/direktoriKategoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Array<Direktori_kategori>, message?: string}>;
      })
    );
  }
  /**
   * Get all Direktori_kategoris
   * @param filter PurchaseRequest that should be stored
   * @return successful operation
   */
  getEndDirektoriKategoris(filter?: string): __Observable<{success?: boolean, data?: Array<Direktori_kategori>, message?: string}> {
    return this.getEndDirektoriKategorisResponse(filter).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Array<Direktori_kategori>, message?: string})
    );
  }

  /**
   * Store Direktori_kategori
   * @param body Direktori_kategori that should be stored
   * @return successful operation
   */
  postEndDirektoriKategorisResponse(body?: Direktori_kategori): __Observable<__StrictHttpResponse<{success?: boolean, data?: Direktori_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/end/direktoriKategoris`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Direktori_kategori, message?: string}>;
      })
    );
  }
  /**
   * Store Direktori_kategori
   * @param body Direktori_kategori that should be stored
   * @return successful operation
   */
  postEndDirektoriKategoris(body?: Direktori_kategori): __Observable<{success?: boolean, data?: Direktori_kategori, message?: string}> {
    return this.postEndDirektoriKategorisResponse(body).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Direktori_kategori, message?: string})
    );
  }

  /**
   * Get Direktori_kategori
   * @param id id of Direktori_kategori
   * @return successful operation
   */
  getEndDirektoriKategorisIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: Direktori_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/end/direktoriKategoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Direktori_kategori, message?: string}>;
      })
    );
  }
  /**
   * Get Direktori_kategori
   * @param id id of Direktori_kategori
   * @return successful operation
   */
  getEndDirektoriKategorisId(id: number): __Observable<{success?: boolean, data?: Direktori_kategori, message?: string}> {
    return this.getEndDirektoriKategorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Direktori_kategori, message?: string})
    );
  }

  /**
   * Update Direktori_kategori
   * @param params The `DirektoriKategoriService.PutEndDirektoriKategorisIdParams` containing the following parameters:
   *
   * - `id`: id of Direktori_kategori
   *
   * - `body`: Direktori_kategori that should be updated
   *
   * @return successful operation
   */
  putEndDirektoriKategorisIdResponse(params: DirektoriKategoriService.PutEndDirektoriKategorisIdParams): __Observable<__StrictHttpResponse<{success?: boolean, data?: Direktori_kategori, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.body;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/end/direktoriKategoris/${encodeURIComponent(params.id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: Direktori_kategori, message?: string}>;
      })
    );
  }
  /**
   * Update Direktori_kategori
   * @param params The `DirektoriKategoriService.PutEndDirektoriKategorisIdParams` containing the following parameters:
   *
   * - `id`: id of Direktori_kategori
   *
   * - `body`: Direktori_kategori that should be updated
   *
   * @return successful operation
   */
  putEndDirektoriKategorisId(params: DirektoriKategoriService.PutEndDirektoriKategorisIdParams): __Observable<{success?: boolean, data?: Direktori_kategori, message?: string}> {
    return this.putEndDirektoriKategorisIdResponse(params).pipe(
      __map(_r => _r.body as {success?: boolean, data?: Direktori_kategori, message?: string})
    );
  }

  /**
   * Delete Direktori_kategori
   * @param id id of Direktori_kategori
   * @return successful operation
   */
  deleteEndDirektoriKategorisIdResponse(id: number): __Observable<__StrictHttpResponse<{success?: boolean, data?: string, message?: string}>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/end/direktoriKategoris/${encodeURIComponent(id)}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<{success?: boolean, data?: string, message?: string}>;
      })
    );
  }
  /**
   * Delete Direktori_kategori
   * @param id id of Direktori_kategori
   * @return successful operation
   */
  deleteEndDirektoriKategorisId(id: number): __Observable<{success?: boolean, data?: string, message?: string}> {
    return this.deleteEndDirektoriKategorisIdResponse(id).pipe(
      __map(_r => _r.body as {success?: boolean, data?: string, message?: string})
    );
  }
}

module DirektoriKategoriService {

  /**
   * Parameters for putEndDirektoriKategorisId
   */
  export interface PutEndDirektoriKategorisIdParams {

    /**
     * id of Direktori_kategori
     */
    id: number;

    /**
     * Direktori_kategori that should be updated
     */
    body?: Direktori_kategori;
  }
}

export { DirektoriKategoriService }
