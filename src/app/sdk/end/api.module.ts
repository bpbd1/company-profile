/* tslint:disable */
import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ApiConfiguration, ApiConfigurationInterface } from './api-configuration';

import { BahayaService } from './services/bahaya.service';
import { ContentService } from './services/content.service';
import { ContentKategoriService } from './services/content-kategori.service';
import { DirektoriService } from './services/direktori.service';
import { DirektoriKategoriService } from './services/direktori-kategori.service';
import { KategoriService } from './services/kategori.service';

/**
 * Provider for all Api services, plus ApiConfiguration
 */
@NgModule({
  imports: [
    HttpClientModule
  ],
  exports: [
    HttpClientModule
  ],
  declarations: [],
  providers: [
    ApiConfiguration,
    BahayaService,
    ContentService,
    ContentKategoriService,
    DirektoriService,
    DirektoriKategoriService,
    KategoriService
  ],
})
export class ApiModule {
  static forRoot(customParams: ApiConfigurationInterface): ModuleWithProviders<ApiModule> {
    return {
      ngModule: ApiModule,
      providers: [
        {
          provide: ApiConfiguration,
          useValue: {rootUrl: customParams.rootUrl}
        }
      ]
    }
  }
}
