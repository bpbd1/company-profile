/* tslint:disable */
export interface Content  {

  /**
   * content
   */
  content?: string;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * expert
   */
  expert?: string;

  /**
   * file
   */
  file?: string;

  /**
   * id_content
   */
  id_content?: string;

  /**
   * id_user
   */
  id_user?: number;

  /**
   * judul
   */
  judul?: string;

  /**
   * keyword
   */
  keyword?: string;

  /**
   * status
   */
  status?: string;

  /**
   * tag
   */
  tag?: string;

  /**
   * thum
   */
  thum?: string;

  /**
   * type
   */
  type?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
