/* tslint:disable */
export interface Kategori  {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * for_module
   */
  for_module?: string;

  /**
   * icon
   */
  icon?: string;

  /**
   * id_kategori
   */
  id_kategori?: string;

  /**
   * kategori
   */
  kategori?: string;

  /**
   * slug
   */
  slug?: string;

  /**
   * thum
   */
  thum?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
