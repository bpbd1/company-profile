/* tslint:disable */
export interface Direktori_kategori  {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * id_direktori
   */
  id_direktori?: string;

  /**
   * id_direktory_kategori
   */
  id_direktory_kategori?: string;

  /**
   * id_kategori
   */
  id_kategori?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
