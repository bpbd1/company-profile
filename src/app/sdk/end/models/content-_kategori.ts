/* tslint:disable */
export interface Content_kategori  {

  /**
   * created_at
   */
  created_at?: string;

  /**
   * id_content
   */
  id_content?: string;

  /**
   * id_content_kategori
   */
  id_content_kategori?: string;

  /**
   * id_kategori
   */
  id_kategori?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
