/* tslint:disable */
export interface Direktori  {

  /**
   * file
   */
   file?: string;

  /**
   * Administrator
   */
  Administrator?: string;

  /**
   * abstark
   */
  abstark?: string;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * feature
   */
  feature?: string;

  /**
   * id_direktori
   */
  id_direktori?: string;

  /**
   * judul
   */
  judul?: string;

  /**
   * kategori
   */
  kategori?: string;

  /**
   * keyword
   */
  keyword?: string;

  /**
   * kode
   */
  kode?: string;

  /**
   * kugi
   */
  kugi?: string;

  /**
   * nama_peta_wilayah
   */
  nama_peta_wilayah?: string;

  /**
   * name_file
   */
  name_file?: string;

  /**
   * namna_peta
   */
  namna_peta?: string;

  /**
   * organisasi_pengelola
   */
  organisasi_pengelola?: string;

  /**
   * shp
   */
  shp?: string;

  /**
   * size
   */
  size?: string;

  /**
   * slug
   */
  slug?: string;

  /**
   * tags
   */
  tags?: string;

  /**
   * tahun
   */
  tahun?: string;

  /**
   * tgl_publikasi
   */
  tgl_publikasi?: string;

  /**
   * tipe
   */
  tipe?: string;

  /**
   * updated_at
   */
  updated_at?: string;

  /**
   * walidata
   */
  walidata?: string;

  /**
   * wilayah
   */
  wilayah?: string;
}
