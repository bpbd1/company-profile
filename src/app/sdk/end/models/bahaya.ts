/* tslint:disable */
export interface Bahaya  {

  /**
   * class
   */
  class?: string;

  /**
   * created_at
   */
  created_at?: string;

  /**
   * id_bahaya
   */
  id_bahaya?: string;

  /**
   * jenis
   */
  jenis?: string;

  /**
   * luas
   */
  luas?: any;

  /**
   * nama
   */
  nama?: string;

  /**
   * updated_at
   */
  updated_at?: string;
}
