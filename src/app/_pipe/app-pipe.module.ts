import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SafeHtmlPipe } from './safe-html.pipe';
import { SafeUrlPipe } from './safe-url.pipe';

const COM = [SafeHtmlPipe, SafeUrlPipe]


@NgModule({
    declarations: [...COM, SafeUrlPipe, SafeUrlPipe],
    imports: [
        CommonModule
    ],
    exports: [...COM]
})
export class AppPipeModule { }
